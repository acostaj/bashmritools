package DMDX::Utils;
# Utilities for DMDX files

use RTF::Config;
use File::Basename;
use Carp;

require Exporter;
@ISA = qw( Exporter );
@EXPORT = qw( );
@EXPORT_OK = qw(zil_est_tr zil_sub_itimes zil_sub_rtimes 
		rtf_cot1 rtf_pred_cots rtf_parse zil_parse est_int do_est);
$VERSION = 0.02;

use strict;

my $pi = atan2(1,1) * 4;

sub zil_est_tr {
# gets estimated TR from zil file
    my($subjects, $resps, $str, $resp, @times, $s, $tr, $lim, $tol);
    croak('Need zil data') unless (@_);
    my $zil = shift; # reference to hash
    if (@_) {$str = shift;} else {$str = 3000};
    if (@_) {$subjects = shift;} else {$subjects = [0]};
    if (@_) {$resps = shift;} else {$resps = ['+Bit0', '+Bit0']};
    if (@_) {$lim = shift;} else {$lim = [200]};
    if (@_) {$tol = shift;} else {$tol = [0.05]};
    my @pulses = ();
    foreach $s(@$subjects) {
	foreach $resp(@$resps) {
	    @times = zil_sub_rtimes($zil->{subjects}[$s], $resp);
	    push(@pulses, \@times);
	}
    }
    $tr = est_int(\@pulses, $str, $lim, $tol);
    return $tr;
}

sub zil_sub_itimes {
# returns 2D item array of row format [item no, cot, respno, time]
    my ($i, $rno, $rtime, $r, $rt, $null, $omit, $o);
    croak('Need zil subject data') unless (@_);
    my $subj = shift; # reference to hash
    croak('Need response strings to get') unless (@_);
    my $resps = shift; # responses to return
    if (@_) {$null = shift;} else {$null = undef}; # not defined
    if (@_) {$omit = shift;} else {$omit = []}; # resps to omit

    my @itimes = ();
    IL: foreach $i(@{$subj->{items}}) {
	foreach $o(@{$omit}) {
	    next IL if ($i->{ino} == $o);
	}
	$rno = $null;
	$rtime = $null; 
# find first matching response
      RL: foreach $r(@{$i->{resps}}) {
	  foreach $rt(0..$#$resps) {
	      if ($r->[0] eq $resps->[$rt]) {
		  $rno = $rt;
		  $rtime = $r->[1] + $i->{cot};
		  last RL;
	      }
	  }
      }
	push(@itimes, [$i->{ino}, $i->{cot}, $rno, $rtime]);
    }
    return @itimes;
}

sub zil_sub_rtimes {
# gets times for named response for one subject
    croak('Need zil subject data') unless (@_);
    my $subj = shift; # reference to hash
    croak('Need response string to get') unless (@_);
    my $resp = shift; # response to return

    my($i, $r, @times);
    foreach $i(@{$subj->{items}}) {
	foreach $r(@{$i->{resps}}) {
	    push(@times, $r->[1] + $i->{cot}) if ($r->[0] eq $resp);
	}
    }
    return @times;
}

sub rtf_cot1 {
# gets time of first clock on
    croak('Need rtf data') unless (@_);
    my $rtf = shift; # reference to hash
    croak('Need TR') unless (@_);
    my $tr = shift; 
    croak('Need tic length') unless (@_);
    my $tic = shift; # response to return
    my $pdelay  = (@_) ? shift : 0;
    my $misstrs = (@_) ? shift : 0;
    my ($item, $ino, $frame);
    my $cot1 = 0;
    my $rflag = 0;
    my $delay = $rtf->{delay} * $tic;
    IL: foreach $ino($misstrs..$#{$rtf->{items}}) {
	$item = $rtf->{items}[$ino];
	foreach $frame(@{$item->{frames}}) {
	    last IL if $frame->{cot};
	    SW: {
		if ($frame->{time}[0] eq "response") {
		    if ($rflag) {
			$cot1+= $tr-$delay;
		    } else {
			$cot1+= $pdelay;
			$rflag = 1;
		    }
		    last SW;
		}
		if ($frame->{time}[0] eq "ms") {
		    $cot1 += $frame->{time}[1];
		last SW;
		}
		# must be tic
		$cot1 += $frame->{time}[1] * $tic;
	    }
	}
	$cot1+= $delay; # delay
    }
    return $cot1;
}

sub rtf_pred_cots {
# gets predicted clock on times from parsed rtf file
# 
# format $cot1 = rtf_pred_cots($rtf, $ticlen, $responselen);

    croak('Need rtf data') unless (@_);
    my $rtf = shift; # reference to hash
    croak('Need tic length') unless (@_);
    my $tic = shift; # response to return
    my $response = (@_) ? shift : 0;
    my ($item, $ino, $frame, @cots);
    my $cot = 0;
    my $cotf = 0;
    my $delay = $rtf->{delay} * $tic;
    foreach $ino(0..$#{$rtf->{items}}) {
	$item = $rtf->{items}[$ino];
	IF: foreach $frame(@{$item->{frames}}) {
	    if (!$cotf) {
		if ($frame->{cot}) {
		    $cotf = 1;
		} else {
		    next IF;
		}
	    }
	    push(@cots, $cot) if ($frame->{cot});
	    SW: {
		if ($frame->{time}[0] eq "response") {
		    $cot+= $response;
		    last SW;
		}
		if ($frame->{time}[0] eq "ms") {
		    $cot += $frame->{time}[1];
		last SW;
		}
		# must be tic
		$cot += $frame->{time}[1] * $tic;
	    }
	}
	$cot+= $delay if ($cotf);
    }
    return @cots;
}

sub rtf_parse {
# parses simple DMDX rtf item file into hash
# does not handle scrambling, branching, DMDX counters
# "," frame delimters (maybe this adds one to containing frame?)
# time outs longer than specified length of item 
# change in default frame length in item
# and probably lots of other stuff
#
# format $rtf = rtf_parse($rtffile);

    croak('Need rtf file name') unless (@_);
    my $rtffile = shift;
    
    my $line1;

# get file name info
    my ($fname,$fpath,$fsuffix) = fileparse($rtffile,'\.rtf');
    if (!($fsuffix =~ /\.rtf/)) {
	croak "File $rtffile does not have .rtf extension\n";
	next;
    }

# get data
    my $rtfdata = rtf2txt($rtffile);

# array for return
    my $rtf = {'filename', $rtffile};

# get parameter (first) line
    ($line1, $rtfdata) = split("\n", $rtfdata, 2);

# parse parameter line
    ($rtf->{delay}) = ($line1 =~ /<(?:Delay|d) (\d+)/);
    ($rtf->{dfd}) = 
	($line1 =~ /(?:^|\s)f(\d+)/);
    $rtf->{dfd} = $1 if
	($line1 =~ /<(?:dfd|DefaultFrameDuration|fd|FrameDuration) (\d+)/);

# parse items
    $rtfdata =~ s/\".*?\"/\"\"/g; # strip quoted stuff
    $rtfdata =~ s/(.*);.*?$/$1/s; # strip end stuff
    my @items = split(';', $rtfdata); # to items

# over items
    my ($item, $frame, $ictr, @frames, $fno, $fend);
    $ictr = -1;
    IL: foreach $item(@items) {
	$ictr ++;
	# get item no and correct response indicator
	($rtf->{items}[$ictr]{fb},
	 $rtf->{items}[$ictr]{ino},
	 $item)
	    = ($item =~ /^\s*(\D?)(\d+)\s*(.*)/s);

# process 0 item types (assume 1 frame only)
	if ($rtf->{items}[$ictr]{ino} == 0) {
	    $rtf->{items}[$ictr]{frames}[0]{time} = 
		['response', 1];
	    $rtf->{items}[$ictr]{frames}[0]{cot} =
		($item =~ /\*/);
	    next IL;
	}

# parse into frames
	@frames = split('/', $item);
	$fend = $#frames;
	FL: foreach $fno(0..$fend) {
# identify clock on time
	    if ($frames[$fno] =~ /\*/) {
		$rtf->{items}[$ictr]{frames}[$fno]{cot} = 1;
	    } else {
		$rtf->{items}[$ictr]{frames}[$fno]{cot} = 0;
	    }
# last empty frame has length 1		    
	    if (($frames[$fno] =~ /^[\s\!]*$/) && ($fno == $fend)) {
		$rtf->{items}[$ictr]{frames}[$fno]{time} =
		    ['tic', 1];
		last FL;
	    }
# search for timing, or default frame length if absent
	    if ($frames[$fno] =~ /<(?:FrameDuration|fd|%) (\d+)>/) {
		$rtf->{items}[$ictr]{frames}[$fno]{time} =
		    ['tic', $1];
	    } else {
		if ($frames[$fno] =~ 
		    /<(?:MSFrameDuration|msfd|ms%|ms) (\d+)>/) {
		    $rtf->{items}[$ictr]{frames}[$fno]{time} =
			['ms', $1];
		} else { # default frame length
		    $rtf->{items}[$ictr]{frames}[$fno]{time} =
			['tic', $rtf->{dfd}];
		}		    
	    }
	}
    }
    return $rtf;
}

sub zil_parse {
# takes zil file name, reads into hash
    croak('Need zil file name') unless (@_);
    my $zilfile = shift;

# get file name info
    my ($fname,$fpath,$fsuffix) = fileparse($zilfile,'\.zil');
    if (!($fsuffix =~ /\.zil/)) {
	croak "File $zilfile does not have .zil extension\n";
	next;
    }

# Read input file as one long record.
    my $zildata=read_text_file($zilfile);

# array for return
    my $zil = {'filename', $zilfile};

# slice off preamble
    $zildata = ($zildata =~ /\*+/) ? $' : $zildata;

# to subjects
    my(@subjects)= split(/\*+/, $zildata);
    my $sno = 0;

    # variables for SLOOP
    my ($sline, %subj, $subject, $item, @items, $ino);

    # variables for ILOOP
    my (%idata, $rest, $warning, $wstore, $resps, @rdata, $rno, 
	$resp, $time, $type);

  SLOOP: foreach $subject (@subjects) {
      next SLOOP if (!($subject =~ /Item/)); # no valid data	
      %subj = ();

      # get subject data if present;
      if ($subject =~ /Subject .*?(?=Item)/s) {
	  $sline = $&;
	  $subject = $';
	  ($zil->{subjects}[$sno]{sno},
	   $zil->{subjects}[$sno]{time},
	   $zil->{subjects}[$sno]{pc},
	   $zil->{subjects}[$sno]{refresh}) = 
	       ($sline=~ /Subject (\d*), (.*?) on (\w*), refresh ([\d.]*)ms/);
	  if ($sline=~ /ID ([\w ]+?)\s/) {
	      $zil->{subjects}[$sno]{id} = $1;
	  } else {
	      $zil->{subjects}[$sno]{id} = "";
	  }
      }

      # get item data
      @items = split(/Item /, $subject);
      $ino = 0;
      undef $warning;
    ILOOP: foreach $item (@items){
	next ILOOP unless ($item =~ /\d+/);
	($zil->{subjects}[$sno]{items}[$ino]{ino},
	 $zil->{subjects}[$sno]{items}[$ino]{cot},
	 $rest) = 
	    ($item =~ /(\d+), COT (\d+\.?\d*)(.*)/s);
	# get warning (from last trial)
	if (defined($warning)) {
	    $wstore = $warning;
	} else { 
	    $wstore = ""; 
	}
	$zil->{subjects}[$sno]{items}[$ino]{warning} = $wstore;

	# get responses, warning from this trial
	if ($rest =~ /!/) {
	    ($rest, $warning) = 
		($rest =~ /(.*?)\!(.*)$/);
	} else { 
	    undef $warning; 
	}
	if ($rest =~ /, No responses./) {
	    $zil->{subjects}[$sno]{items}[$ino]{resps} = ();
	} else {
	    @rdata = ($rest =~ /(\d+\.?\d*,[-+\w]*)/g);
	    $rno = 0;
	    foreach $resp(@rdata) {
		($time, $type) = 
		    ($resp =~ /(\d+\.?\d*),([-+\w]*)/);
		$zil->{subjects}[$sno]{items}[$ino]{resps}[$rno] = [$type, $time];
		$rno ++;
	    }
	}
	$ino ++;
    }
      $sno ++;      
  }
    return $zil;
}


sub est_int {
# FORMAT etr = est_int(pulses,etr,lim,tol)
# Best guess at interval from pulse onset/offset times / lengths
# Uses the megolithic yard algorithm, thanks to Ian NS
#
# pulses   - array of vectors of pulse onset / offset times
# etr      - starting interval estimate (default 3000 ms)
# lim      - limits tolerable +/- in ms
#            array ref, containing vector or scalar
#            if vector, tries estimate with each limit in turn
#            (default - vector dropping from [(etr*3)^1 to 1]
# tol      - accuracy required in ms.  Array ref.
#            If vector, accuracy for each limit specified 
#            (default - lim / 100)
#
# It seems pretty robust to wrong starting estimates of TR
# - but can make errors for correct starting estimates!
# Matthew Brett 6/7/00, 7/3/02

    croak "Need pulses" unless (@_);
    my $pulses = shift;
    my($str, $lims, $tols, @tmp, $i, $l);
    if (@_) {$str = shift;} else {$str = 3000};
    if (@_) {$lims = shift;} else {
	@tmp = reverse(0 .. 10);
	foreach $i(0..$#tmp) {
	    $lims->[$i] = $str ** ($tmp[$i] / $#tmp);
	}
    }
    if (@_) {$tols = shift;} else {
	foreach $l(0 .. $#$lims) {
	    $tols->[$l] = $lims->[$l] / 100;
	}
    }

    my ($tol, $lim, $wt);
    my $etr = $str;
    my $ti = 0;
    foreach $ti(0..$#{$tols}) {
	$lim = $lims->[$ti];
	$tol = $tols->[$ti];
	($etr, $wt) = do_est($pulses, $etr, $lim, $tol);
    }
    if ($#{$tols} && $wt > 0) {
	# maybe hit a multiple / divisor of the interval
	# check between start -+ first limit
	my $m = 1; 
	my $wtn = $wt;
	my $etrn;
	my $end = $str + $lims->[0];
	my $trytr = $etr;
	my $int = $etr;
	while ($trytr < $end) {
	    ($etrn, $wtn) = do_est($pulses, $trytr , $lim, $tol);
	    if ($wtn > $wt) {
		$etr = $etrn;
	    }
	    $trytr += $int;
	}
    } 
    return $etr;
}

sub do_est {
    croak "Need pulses" unless (@_);
    my $pulses = shift;
    croak "Need estimated interval" unless (@_);
    my $etr = shift;
    my($tol, $lim, $min, $max, $ssin, $scos, @megy, $mmegy, $idx,
       @trs, $ppack, $t, $p, $bigm);
    if (@_) {$lim = shift;} else {$lim = 1};
    if (@_) {$tol = shift;} else {$tol = $lim / 100};
    if (@_) {$min = shift;} else {$min = 0.1};
    if (@_) {$max = shift;} else {$max = 25000};
    
    # defining intervals to try
    my $lb = $etr-$lim;
    $lb = $min if $lb < $min;
    my $hb = $etr+$lim;
    $hb = $max if $hb > $max;

    # intervals 
    $t = $lb;
    while ($t < $hb) {
	push(@trs, $t);
	$t += $tol;
    }

    # cycle over pulse lists
    @megy = (0) x @trs;
 
    foreach $ppack(@$pulses) {
	# intervals
	foreach $t(0..$#trs) {
	    # and pulses
	    $ssin = 0;
	    $scos = 0;
	    foreach $p(@$ppack) {
		$bigm = $p * 2 * $pi / $trs[$t];
		$ssin += sin($bigm);
		$scos += cos($bigm);
	    }
	    # value for this interval, all pulses in pack
	    $megy[$t] += ($ssin ** 2)  + ($scos ** 2);
	}
    }
    ($mmegy, $idx) = max(@megy);
#    print "lb $lb, hb $hb, lim $lim, tol $tol, tr $trs[$idx], wt $mmegy, idx $idx\n";
    return wantarray ? ($trs[$idx], $mmegy) : $trs[$idx];
}

sub max {
    # zero based max function, returns max and index
    my $max = 0;
    my ($idx, $foo);
    my $i = 0;
    foreach $foo(@_) {
	if ($max < $foo) {
	    $max = $foo;
	    $idx = $i;
	}
	$i += 1;
    }
    return wantarray ? ($max, $idx): $max;
}

sub read_text_file {
    my $fname = @_ ? shift : croak "Need filename";
    my $data;

# open file
    if (! open(FILE,"<$fname") ) {
	croak "Can't open input file $fname\n";
    }

# Read input file as one long record.
    {
	local $/;  
	$data=<FILE>;
	close FILE;
    }

# translate to unix format
    $data =~ s/\r\n/\n/g;

    return $data;
}

sub rtf2txt {
# converts rtf file to plain text
    my $fname = shift;
    my $text;

    {
	local $^W=0;
	require RTF::TEXT::Converter;
	my $self = new RTF::TEXT::Converter(Output => \$text);	
	$self->parse_stream($fname);
    }
    return wantarray ? split("\n", $text) : $text;
}
1;
