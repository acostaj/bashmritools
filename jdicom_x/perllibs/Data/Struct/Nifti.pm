# Data::Struct::Nifti - implements structure for Nifti header.
#
# $Id: Analyze.pm,v 1.1.1.1 2004/04/22 18:26:03 matthewbrett Exp $

package Data::Struct::Nifti;

use Data::Struct::Base;

@ISA = ('Data::Struct::Base');

use strict;

my @hdrdef  = (
		  ["sizeof_hdr",    'l', 348],
		  ["data_type",     'a10', ""],
		  ["db_name",       'a18', ''],
		  ["extents",       'l', 16384],
		  ["session_error", 's', 0],
		  ["regular",       'a', 'r'],
		  ["dim_info",      'c', 0],
		  
		  ["dim",           's', [4,1,1,1,1,0,0,0]],
		  ["intent_p1",     'f', 0],   
		  ["intent_p2",     'f', 0],   
		  ["intent_p3",     'f', 0],   
		  ["intent_code",   's', 0],
		  ["datatype",      's', 4],
		  ["bitpix",        's', 16],
		  ["slice_start",   's', 0],
		  ["pixdim",        'f', [0,1,1,1,0,0,0,0]],
		  ["vox_offset",    'f', 348],	# Should be 352 if nift_ext used 
		  ["scl_slope",     'f', 1],
		  ["scl_inter",     'f', 0], 
		  ["slice_end",     's', 0],
		  ["slice_code",    'c', 0],
		  ["xyzt_units",    'c', 0],
		  ["cal_max",       'f', 0],   
		  ["cal_min",       'f', 0],
		  ["slice_duration",'f', 0],
		  ["toffset",       'f', 0],
		  ["glmax",         'l', 0],
		  ["glmin",         'l', 0],
		  
		  ["descrip",       'a80', ''],
		  ["aux_file",      'a24', ''],
		  
		  ["qform_code",     's', 0],
		  ["sform_code",     's', 0],
		  
		  ["quatern_b",     'f', 0],
		  ["quatern_c",     'f', 0],
		  ["quatern_d",     'f', 0],
		  ["qoffset_x",     'f', 0],
		  ["qoffset_y",     'f', 0],
		  ["qoffset_z",     'f', 0],
		  
		  ["srow_x",        'f', [1,0,0,0]],
		  ["srow_y",        'f', [0,1,0,0]],
		  ["srow_z",        'f', [0,0,1,0]],
		  
		  ["intent_name",   'a16', ''],
		  ["magic",         'a4', "n+1\0" ],   
		  
#		  ["nifti_ext",     'l', 0 ],  # Unused and left blank so files
#		  				 can be imported into Analyze
		  );

my $swaptest = sub {
    my $self = shift;
    $self->{was_swapped} = ($self->sizeof_hdr()==1543569408 ||
    	($self->dim(0) < 0 || $self->dim(0) > 15));
    return $self->{was_swapped};
};

sub new {
    my $that = shift;
    my $class = ref($that) || $that;
    my $self = $class->SUPER::new(\@hdrdef, 
			  'verbose'=> 0, 
			  'extension' => 'nii',
			  'enforce_extension' => 1, 
			  'endian', {'default_in', 'native',
				     'try_unswap', 1,
				     'swaptest',$swaptest,},
			  @_);
    bless $self, $class; 
    return $self;
}
1;
