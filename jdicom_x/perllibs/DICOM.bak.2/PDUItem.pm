
package DICOM::PDUItem; 
use strict; 

my $uid = ""; 
my $itemtype = 0; 

sub new { 
 my $class = shift;  
 my $pdu = {}; 
 
 $pdu->{itemtype} = 0; 

 bless $pdu, $class; 
 return $pdu; 
}

sub getlength { 
  my $self = shift;  
  return length($self->{uid}); 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 
  
  print $dest pack("c", $self->{itemtype});
  print $dest pack("c", 0);
  print $dest pack("n", $self->getlength()); 
  print $dest $self->{uid}; 

}

sub read { 
  my $self = shift; 
  my $source = shift; 

  # Have read the first byte (pdu type) already... 
  
  my $input;
  read $source, $input, 3; 
  my $length = unpack("xn", $input); 
  read $source, $uid, $length; 
  $self->{uid} = $uid; 
}

sub printcontents { 
  my $self = shift; 

  print " Type: " . $self->{itemtype} . "\t"; 
  if ((length $self->{uid}) == 4) { print unpack("N", $self->{uid}) . "\n"; } 
    else { print $self->{uid} . " (" . (length $self->{uid}) . ")\n"; } 
}

1;

