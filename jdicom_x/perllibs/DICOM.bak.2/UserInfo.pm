
package DICOM::UserInfo; 
use strict;

require DICOM::PDUItem; 

sub new { 
 my $class = shift;  
 my $pdu = {}; 
 $pdu->{implver} = "dicomperl";
 #$pdu->{implclass} = "1.2.804.114118.3";
 $pdu->{implclass} = "1.0";
 
 bless $pdu, $class; 
 $pdu->{contents} = [ ]; 
 
 my $subpdu = new DICOM::PDUItem; 
 $subpdu->{itemtype} = 81;	# Max PDU Length 
 #$subpdu->{uid} = pack("N", 16384); 
 $subpdu->{uid} = pack("N", 32000); 
 push @{$pdu->{contents}}, $subpdu; 

 
 my $cpdu = new DICOM::PDUItem; 
 $cpdu->{itemtype} = 82;
 $cpdu->{uid} = $pdu->{implclass}; 
 push @{$pdu->{contents}}, $cpdu; 
 my $ipdu = new DICOM::PDUItem; 
 $ipdu->{itemtype} = 85;
 $ipdu->{uid} = $pdu->{implver}; 
 push @{$pdu->{contents}}, $ipdu; 
 
 return $pdu; 
}

sub getlength { 
  my $self = shift; 
  my $length = 0;
  foreach my $subpdu (@{$self->{contents}}) {
    $length += $subpdu->getlength() + 4; 
  }
  return $length; 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 
  
  print $dest pack("c", 80);
  print $dest pack("c", 0);
  print $dest pack("n", $self->getlength() ); 

  foreach my $subpdu (@{$self->{contents}}) { 
	$subpdu->write( $dest ); 
  }

}

sub read { 
  my $self = shift; 
  my $source = shift; 

  $self->{itemtype} = 80; 
  $self->{contents} = [ ]; 

  my $input; 
  read $source, $input, 3; 
  my $length = unpack("xn", $input);

  while ($length > 0) {
    read $source, $input, 1;
    my $pdutype = unpack ("c", $input);
    my $pdu = new DICOM::PDUItem; 
    $pdu->{itemtype} = $pdutype; 
    $pdu->read( $source );
    $length -= $pdu->getlength() + 4;

    push @{$self->{contents}}, $pdu; 
  }

}

sub printcontents { 
  my $self = shift; 
  
  print " UserInfo : \n"; 
  foreach my $subpdu (@{$self->{contents}}) {
    print "              " . $subpdu->{itemtype} . " "; 
    if (length($subpdu->{uid}) != 4) { print $subpdu->{uid} . " (" . length($subpdu->{uid}) . ")\n"; } 
      else { print unpack("N", $subpdu->{uid}) . "\n"; } 
  } 

}

1;

