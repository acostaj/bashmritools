
package DICOM::PDU; 
use strict; 

require DICOM::AssociateRQ;
require DICOM::AssociateAC;
require DICOM::AssociateRJ;
require DICOM::PDataTF;
require DICOM::ReleaseRQ;
require DICOM::ReleaseRP;
require DICOM::Abort; 

sub new {
 my $class = shift;
 
 if (scalar @_ == 0) { return undef; }
 my $source = shift; 

 my $input; 
 read $source, $input, 1; 
 my ($pdutype) = unpack ("c", $input); 
 
 my $pdu_object; 
 
 if (length($input)==0) { die "Connection aborted by server!"; } 
 
 if ($pdutype==1) { $pdu_object = new DICOM::AssociateRQ; } 
 elsif ($pdutype==2) { $pdu_object = new DICOM::AssociateAC; }
 elsif ($pdutype==3) { $pdu_object = new DICOM::AssociateRJ; }
 elsif ($pdutype==4) { $pdu_object = new DICOM::PDataTF; }
 elsif ($pdutype==5) { $pdu_object = new DICOM::ReleaseRQ; }
 elsif ($pdutype==6) { $pdu_object = new DICOM::ReleaseRP; }
 elsif ($pdutype==7) { $pdu_object = new DICOM::Abort; }
 else { die ("Bad PDU Type: ", $pdutype); }
 
 $pdu_object->read( $source ); 

 return $pdu_object; 

}

1;
