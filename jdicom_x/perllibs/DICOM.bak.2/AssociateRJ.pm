
package DICOM::AssociateRJ; 

use DICOM::Release; 
use strict;
use vars qw( @ISA );

@ISA =  qw/DICOM::Release/;

sub new {
 my $class = shift;
 
 my $elements = $class->SUPER::new( @_ );
 $elements->{pdutype} = 3; 

 bless $elements, $class;
 
 return $elements;
}

1;
