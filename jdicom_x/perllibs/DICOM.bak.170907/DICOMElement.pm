
package DICOM::DICOMElement; 

use Config;
use DICOM::Dictionary; 
use strict; 

sub new { 
  my $class = shift;
  my $elements = {};
  bless $elements, $class;

  return $elements; 
}

sub getlength { 
  my $self = shift; 
  my $length = 0; 
  
  if (! defined $self->{type}) { 
    return length($self->{data}); 
  } elsif ( $self->{type} == 1 ) { # String 
    $length = length($self->{data}); 
  } elsif ( $self->{type} == 2) { # Short int 
    $length = 2; 
  } elsif ( $self->{type} == 3) { # Long int 
    $length = 4; 
  } elsif ( $self->{type} == 5) { # float 
    $length = 4; 
  } elsif ( $self->{type} == 6) { # double 
    $length = 8; 
  } elsif ( $self->{type} == 4) { # Sequence 
    foreach (@{$self->{data}}) { 
	$length += $_->getlength() + 8; 
    } 
  }
  
  return $length; 
}

sub setValue { 
  my $self = shift; 
  my $group = shift; 
  my $element = shift; 
  my $contents = shift; 
  
  my $type; 
  if ( DICOM::Dictionary::is_short( $group, $element ) ) {
    $type = 2; 
  } elsif ( DICOM::Dictionary::is_int( $group, $element ) ) { 
    $type = 3;
  } elsif ( DICOM::Dictionary::is_float( $group, $element ) ) { 
    $type = 5;
  } elsif ( DICOM::Dictionary::is_double( $group, $element ) ) { 
    $type = 6;
  } else { 
    $type = 1; 
  } 
  if ( $element eq '0000' ) {	# Group lengths are always UL 
    $type = 3;
  }

  if (($type == 1) && ((length($contents) & 1) == 1)) { 
    if (DICOM::Dictionary::is_string( $group, $element ) ) { 
      $contents .= " "; 
    } else { 
      $contents .= "\0"; 
    }
  }
  
  $self->{group} = $group; 
  $self->{elem} = $element; 
  $self->{type} = $type; 
  $self->{data} = $contents; 

}

sub getbinary { 
  my $self = shift; 
  
  my $group_num = hex( $self->{group} );
  my $elem_num = hex( $self->{elem} );
  my $tags = pack("v", $group_num) . pack("v", $elem_num) . pack("V", $self->getlength() ); 
  
  my $data = $tags; 
  my $type = $self->{type} || 1; 
  my $contents = $self->{data}; 

  if ($type == 1) { 
    if (defined $contents) { $data .= $contents } ; 
  } elsif ($type == 2) {
    $data .= pack("v", $contents);
  } elsif ($type == 3) {
    $data .= pack("V", $contents);
  } elsif ($type == 5) {
    my $is_littleendian = ((substr $Config{byteorder},  0, 1) == "1");
    my $raw = $contents;
    if (! $is_littleendian) { # Need to flip data if we are big endian 
   	 $raw = pack("C*", reverse unpack("C*", $contents));
    }
    $data .= pack("f", $raw);
  } elsif ($type == 6) {
    my $is_littleendian = ((substr $Config{byteorder},  0, 1) == "1");
    my $raw = $contents;
    if (! $is_littleendian) { # Need to flip data if we are big endian 
   	 $raw = pack("C*", reverse unpack("C*", $contents));
    }
    $data .= pack("d", $raw);
  } elsif ($type == 4) {
    foreach (@{$self->{data}}) {
	$data .= pack("v", hex('FFFE') );
	$data .= pack("v", hex('E000') );
	$data .= pack("V", $_->getlength() ); 
 	$data .= $_->getbinary();
    }
  } else { 
    $data .= $contents;
  } 

  return $data; 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 
  
  my $group_num = hex( $self->{group} ); 
  my $elem_num = hex( $self->{elem} ); 

  print $dest pack("v", $group_num); 
  print $dest pack("v", $elem_num); 
  print $dest pack("V", $self->getlength() ); 

  if ($self->{type} == 1) { 
    print $dest $self->{data}; 
  } elsif ($self->{type} == 2) { 
    print $dest pack("v", $self->{data}); 
  } elsif ($self->{type} == 3) { 
    print $dest pack("V", $self->{data}); 
  } elsif ($self->{type} == 4) { 
    foreach (@{$self->{data}}) { 
	print $dest pack("v", hex('FFFE') ); 
	print $dest pack("v", hex('E000') ); 
	print $dest pack("V", $_->getlength());
 	$_->write( $dest ); 
    }
  } elsif ($self->{type} == 5) { 
    my $is_littleendian = ((substr $Config{byteorder},  0, 1) == "1");
    my $raw = $self->{data};
    if (! $is_littleendian) { # Need to flip data if we are big endian 
   	 $raw = pack("C*", reverse unpack("C*", $self->{data}));
    }
    print $dest pack("f", $raw);
  } elsif ($self->{type} == 6) { 
    my $is_littleendian = ((substr $Config{byteorder},  0, 1) == "1");
    my $raw = $self->{data};
    if (! $is_littleendian) { # Need to flip data if we are big endian 
   	 $raw = pack("C*", reverse unpack("C*", $self->{data}));
    }
    print $dest pack("d", $raw);
  } else {
    print $dest $self->{data};
  }

}

sub parse { 
  my $self = shift; 
  my $data = shift; 
  my $offset = shift; 
 
  my $isexplicit = shift || 0;

  my $sub_offset = 8; 

  my $tags = substr( $data, $offset, 12); 
  my ($group, $elem, $length);
  if ($isexplicit) { 
    ($group, $elem) =  unpack("vv", $tags);
    my ($vr) = unpack("x4A2", $tags);
    if ( ($vr eq "OB") || ($vr eq "OW") || ($vr eq "OF") ||
    	($vr eq "SQ") || ($vr eq "UT") || ($vr eq "UN") ) { 
		($length) = unpack("x8V", $tags); 
    		$sub_offset = 12;
    } else { 
   		($length) = unpack("x6v", $tags); 
    		$sub_offset = 8;
    }
    $self->{vr} = $vr;
  } else { 
    ($group, $elem, $length) = unpack("vvV", $tags); 
    $sub_offset = 8;
  }

  $self->{group} = sprintf("%04x", $group); 
  $self->{elem} = sprintf("%04x", $elem); 

  if (( ($isexplicit) && DICOM::Dictionary::is_vr_short( $self->{vr} ) ) || 
  	( (!$isexplicit) && DICOM::Dictionary::is_short( $self->{group},  $self->{elem} ) && ($length==2) ) ) {
    $self->{data} = unpack("v", substr( $data, ($offset+$sub_offset), $length) ); 
    $self->{type} = 2; 
  } elsif (( ($isexplicit) && DICOM::Dictionary::is_vr_int( $self->{vr} ) ) || 
  	( (!$isexplicit) && DICOM::Dictionary::is_int( $self->{group},  $self->{elem} ) && ($length==4) ) ) {
    $self->{data} = unpack("V", substr( $data, ($offset+$sub_offset), $length) );
    $self->{type} = 3; 
  } elsif (( ($isexplicit) && DICOM::Dictionary::is_vr_float( $self->{vr} ) ) || 
  	( (!$isexplicit) && DICOM::Dictionary::is_float( $self->{group},  $self->{elem} ) && ($length==4) ) ) {
    my $raw = substr( $data, ($offset+$sub_offset), $length);
    my $is_littleendian = ((substr $Config{byteorder},  0, 1) == "1");
    if (! $is_littleendian) { # Need to flip data if we are big endian 
   	 $raw = pack("C*", reverse unpack("C*", $raw));
    }
    $self->{data} = unpack("f", $raw );
    $self->{type} = 5; 
  } elsif (( ($isexplicit) && DICOM::Dictionary::is_vr_double( $self->{vr} ) ) || 
  	( (!$isexplicit) && DICOM::Dictionary::is_double( $self->{group},  $self->{elem} ) && ($length==8) ) ) {
    my $raw = substr( $data, ($offset+$sub_offset), $length);
    my $is_littleendian = ((substr $Config{byteorder},  0, 1) == "1");
    if (! $is_littleendian) { # Need to flip data if we are big endian 
   	 $raw = pack("C*", reverse unpack("C*", $raw));
    }
    $self->{data} = unpack("d", $raw );
    $self->{type} = 6; 
  } elsif (( ($isexplicit) && DICOM::Dictionary::is_vr_sq( $self->{vr} ) ) || 
  	( (!$isexplicit) && DICOM::Dictionary::is_sq( $self->{group},  $self->{elem} ) ) ) {
    $self->{type} = 4; 
    $self->{data} = []; 
    my $sq_offset = $sub_offset; 
    while (($length > 0) || ($length ==  hex('FFFFFFFF'))) { 
	my $seq_item = substr($data, $offset+$sq_offset, 8); 
	my ($gp, $el, $sq_len) = unpack("vvV", $seq_item);
	if ($sq_len>$length) { $sq_len = $length-8; } # Should never happen. TODO: produce proper error
	$sq_offset += 8; 
	if (($length ==  hex('FFFFFFFF')) && ($gp == hex('FFFE')) && ($el == hex('E0DD')) ) { last; }
	my $sq_data = new DICOM::DICOMObject;
	if ($sq_len != hex('FFFFFFFF')) { 
		$sq_data->parse($data, $offset+$sq_offset, $offset+$sq_offset+$sq_len, $isexplicit); 
	} else {
		$sq_len = $sq_data->parse($data, $offset+$sq_offset, -1, $isexplicit);
	}
	push @{$self->{data}}, $sq_data; 
   	if ($length !=  hex('FFFFFFFF')) { $length -= ($sq_len+8); } 
	$sq_offset += $sq_len;
    }
    $length = $sq_offset-$sub_offset; 

  } else { 
    $self->{data} = substr( $data, ($offset+$sub_offset), $length); 
    $self->{type} = 1; 
  }

  return ($length+$sub_offset); 

}

sub printcontents {
  my $self = shift;
  my $indent = shift | 0; 

  my $gref = $self->{group}; 
  my $eref = $self->{elem}; 
  
  print pack("A$indent"); 
  print "$gref $eref ";
  if (defined $self->{vr}) { print $self->{vr} . " "; } 
  print pack("A35", DICOM::Dictionary::getname( $gref, $eref ));
  
  if ($self->{type} == 4) { 
	print "Sequence\n"; 
	for (my $i=0; $i<(scalar @{$self->{data}}); $i++) { 
		print pack("A$indent");
		print " Item: $i\n"; 
		$self->{data}->[$i]->printcontents($indent + 1); 
		print "\n";	
	}
  } elsif (($self->{type} == 1) 
  	&& (DICOM::Dictionary::is_unprintable( $gref, $eref ))) {
	print "<Data>\n"; 
  } else { 
	my $out =  $self->{data};
	$out =~ s/\000//g;
	print $out, "\n"; 
  }

}

1;
