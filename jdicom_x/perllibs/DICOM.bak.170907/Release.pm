
package DICOM::Release;

use DICOM::PDU; 
use strict; 

sub new {
 my $class = shift;
 my $elements = {};

 bless $elements, $class;
 my $i;
 for ($i=0; $i < scalar @_; $i++) {
   if ($_[$i] eq "Result") { $elements->{result} = $_[$i+1]; $i++; }
   elsif ($_[$i] eq "Reason") { $elements->{reason} = $_[$i+1]; $i++; }
   elsif ($_[$i] eq "Source") { $elements->{source} = $_[$i+1]; $i++; }
 }

 return $elements;
}

sub getlength() { 
  my $self = shift; 
  return 4; 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 

  print $dest pack("c", $self->{pdutype} ); 
  print $dest pack("c", 0);

  print $dest pack("N", $self->getlength() ); 
  
  print $dest pack("xccc", $self->{result}, $self->{reason}, $self->{diag}); 

}

sub read { 
  my $self = shift; 
  my $source = shift; 
  
  my $input; 
  read $source, $input, 5; 
 
  my ( $length ) = unpack ("xN", $input); 

  read $source, $input, 4; 
  my ( $result, $source, $diag ) = unpack ("xccc", $input ); 
  #print "Result: $result Source: $source Diag: $diag\n"; 
  $self->{result} = $result; 
  $self->{reason} = $source; 
  $self->{diag} = $diag; 
  
  return 1;
}

sub printcontents { 
  my $self = shift;
  if ($self->{pdutype} == 5) { 
	print "ReleaseRQ\n"; 
  } else { 
	print "ReleaseRP\n"; 
  }
}

1;
