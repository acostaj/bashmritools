
package DICOM::AssociateRJ; 

use DICOM::Release; 
use strict;
use vars qw( @ISA );

@ISA =  qw/DICOM::Release/;

sub new {
 my $class = shift;
 
 my $elements = $class->SUPER::new( @_ );
 $elements->{pdutype} = 3; 

 bless $elements, $class;
 
 return $elements;
}

sub write {
 my $self = shift;
 my $dest = shift;

 print $dest pack("c", 3 );
 print $dest pack("c", 0 );
 print $dest pack("N", 4 );
 print $dest pack("c", 0 );

 print $dest pack("c", $self->{result} );
 print $dest pack("c", $self->{source} );
 print $dest pack("c", $self->{reason} );

}

1;
