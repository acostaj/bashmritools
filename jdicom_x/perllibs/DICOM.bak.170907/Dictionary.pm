
package DICOM::Dictionary; 
use strict; 

use vars qw($types $names $vm $dictionary_location); 

sub new { 
  my $class = shift;
  my $elements = {};
  bless $elements, $class;

  return $elements; 
}

#%types = ();
#%names = (); 
#%vm = (); 

sub gettype { 
  my $group = lc shift; 
  my $elem = lc shift; 

  findentry( $group, $elem );
  return $types->{$group}{$elem};

}

sub is_string { 
  my $group = lc shift; 
  my $elem = lc shift; 
  
  findentry( $group, $elem );
  
  # String data types
  if ( ( $types->{$group}{$elem} eq "AE" ) ||
       ( $types->{$group}{$elem} eq "AS" ) ||
       ( $types->{$group}{$elem} eq "CS" ) ||
       ( $types->{$group}{$elem} eq "DA" ) ||
       ( $types->{$group}{$elem} eq "DS" ) ||
       ( $types->{$group}{$elem} eq "DT" ) ||
       ( $types->{$group}{$elem} eq "IS" ) ||
       ( $types->{$group}{$elem} eq "LO" ) ||
       ( $types->{$group}{$elem} eq "LT" ) ||
       ( $types->{$group}{$elem} eq "PN" ) ||
       ( $types->{$group}{$elem} eq "ST" ) ||
       ( $types->{$group}{$elem} eq "SH" ) ||
       ( $types->{$group}{$elem} eq "TM" ) ||
       ( $types->{$group}{$elem} eq "UI" ) ||
       ( $types->{$group}{$elem} eq "UT" ) ||
   # Binary data types - treat as the same 
       ( $types->{$group}{$elem} eq "OB" ) ||
       ( $types->{$group}{$elem} eq "OW" ) ) { 

       return 1; 
   } else { 
       return 0; 
   }
}


sub is_unprintable { 
  my $group = lc shift; 
  my $elem = lc shift; 
  
  findentry( $group, $elem );
 
 if ( ( $types->{$group}{$elem} eq "OB" ) ||
      ( $types->{$group}{$elem} eq "OW" ) || 
      ( $types->{$group}{$elem} eq "OF" ) || 
      ( $types->{$group}{$elem} eq "FD" ) || 
      ( $types->{$group}{$elem} eq "FL" ) || 
      ( $types->{$group}{$elem} eq "OW/OB" ) || 
      ( $types->{$group}{$elem} eq "UN" ) ) {

      return 1; 
  } else { 
      return 0; 
  }
}


sub is_short { 
  my $group = lc shift; 
  my $elem = lc shift; 

  findentry( $group, $elem );
  return is_vr_short( $types->{$group}{$elem} );
}

sub is_vr_short { 
  my $vr = shift; 

  # Short data types
  if ( ( $vr eq "SS" ) ||
       ( $vr eq "US" ) ) { 
       return 1; 
   } else { 
       return 0; 
   }
}


sub is_int { 
  my $group = lc shift; 
  my $elem = lc shift; 
  
  findentry( $group, $elem );
  return is_vr_int( $types->{$group}{$elem} );
}

sub is_vr_int { 
  my $vr = shift; 

  # Integer data types
  if ( ( $vr eq "SL" ) ||
       ( $vr eq "UL" ) ) { 
       return 1; 
   } else { 
       return 0; 
   }
}

       

sub is_float { 
  my $group = lc shift; 
  my $elem = lc shift; 
  
  findentry( $group, $elem );
  return is_vr_float( $types->{$group}{$elem} );
}

sub is_vr_float { 
  my $vr = shift; 

  # Float data types
  if ( $vr eq "FL" ) { 
       return 1; 
   } else { 
       return 0; 
   }
}

       

sub is_double { 
  my $group = lc shift; 
  my $elem = lc shift; 
  
  findentry( $group, $elem );
  return is_vr_double( $types->{$group}{$elem} );
}

sub is_vr_double { 
  my $vr = shift; 

  # Float data types
  if ( $vr eq "FD" ) { 
       return 1; 
   } else { 
       return 0; 
   }
}

       

sub is_sq { 
  my $group = lc shift; 
  my $elem = lc shift; 
  
  findentry( $group, $elem );
  return is_vr_sq( $types->{$group}{$elem} ); 
}

sub is_vr_sq { 
  my $vr = shift; 

  # Sequence data types
  if ( $vr eq "SQ" ) { 
       return 1; 
   } else { 
       return 0; 
   }
}

       

sub getname { 
  my $group = shift; 
  my $elem = shift; 

  findentry( $group, $elem );
  return $names->{$group}{$elem};

}
     
sub findentry { 
  my $group = lc shift;
  my $elem = lc shift;
  
  if ( ! defined $dictionary_location ) { 
	$dictionary_location = "";
	foreach (@INC) {
		if (( -e "$_/DICOM/Dictionary.pm" ) && ( -e "$_/DICOM/elmdict.tpl")) { 
			$dictionary_location = "$_/DICOM"; 
		}
	}
  }

  if (exists $types->{$group}) { 
    if (exists $types->{$group}{$elem}) { 
      return; 
    } 
  }
  
  # If group not parsed, set up ready for it

  if ( ! exists $types->{$group} ) { 
		$types->{$group} = (); 
		$names->{$group} = (); 
		$vm->{$group} = (); 
  } else { 
   	#print "HAVE ALREADY for $group : ", (keys %{$types->{$group}}), "\n";	
	$types->{$group}{$elem} = "UN"; 	# Unknown tag - don't look up again
  	$vm->{$group}{$elem} = "";
  	$names->{$group}{$elem} = "Unknown"; 
 	return; 
  }
 
  $elem = "ANY"; 					# Do all elements in a group at once for speed's sake
 	
  open (DICOM, "<$dictionary_location/elmdict.tpl") or die "Couldn't open dictionary";
  my $tmp = $/;
  $/ = "\n";
  while (<DICOM>) {
    my ($grp, $elm, $vers, $vr, $vms, $name) = 
	m/\(([\d,A-F,a-f]{4}),([\d,A-F,a-f]{4})\)\s+VERS="(.*?)"\s+VR="(.*?)"\s+VM="(.*?)".*?Name="(.*?)"/ ;
    #print $grp , "," , $elm, "\n"; 
    if ( $grp =~ m/$group/i ) { 
	if ( $elm =~ m/$elem/i ) { 
		$types->{$group}{$elem} = $vr; 
		$vm->{$group}{$elem} = $vms;
		$names->{$group}{$elem} = $name;
   		last; 
   	} elsif ( $elem eq "ANY" ) { 
		$types->{$group}{lc $elm} = $vr; 
		$vm->{$group}{lc $elm} = $vms;
		$names->{$group}{lc $elm} = $name;
	}
    }
  }
  $/ = $tmp;
  close (DICOM); 
}

1;
