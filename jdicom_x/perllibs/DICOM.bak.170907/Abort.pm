
package DICOM::Abort; 

use DICOM::Release; 
use strict; 
use vars qw( @ISA );

@ISA =  qw/DICOM::Release/; 

sub new {
 my $class = shift;
 
 my $elements = $class->SUPER::new( @_ );
 bless $elements, $class;
 
 $elements->{pdutype} = 7; 
 $elements->{reason} = 0; 

 return $elements;
}

1;
