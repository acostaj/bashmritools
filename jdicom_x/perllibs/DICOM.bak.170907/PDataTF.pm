
package DICOM::PDataTF; 
use strict; 

use DICOM::PDUItem; 
use DICOM::PDataValue; 

sub new {
 my $class = shift;
 my $pcid = shift; 
 my $messctrl = shift; 
 my $pdv_data = shift; 

 my $elements = {};
 bless $elements, $class;
 $elements->{pdutype} = 4; 

 $elements->{pdata_values} = [ ]; 
 if ((defined $pcid) && (defined $messctrl) && (defined $pdv_data)) { 
	my $pdv = new DICOM::PDataValue( $pcid, $messctrl, $pdv_data); 
  	$elements->{pdata_values} = [ $pdv ]; 
 }

 return $elements;
}

sub getprescontextid { 
  my $self = shift; 
  
  if (scalar @{$self->{pdata_values}} != 0) { 
 	return (${$self->{pdata_values}}[0]->{pcid}); 
  }
}

sub setprescontextid { 
  my $self = shift; 
  my $pcid = shift; 

  foreach my $pdv (@{$self->{pdata_values}} ) { 
    $pdv->{pcid} = $pcid;  
  }
}

sub getlength() { 
  my $self = shift; 
  
  my $length = 0;
  foreach (@{$self->{pdata_values}}) {
    $length += $_->getlength() + 4;
  }
  return $length;
}

sub write { 
  my $self = shift; 
  my $dest = shift; 

  print $dest pack("c", 4 ); 
  print $dest pack("c", 0);

  print $dest pack("N", $self->getlength() ); 
  foreach (@{$self->{pdata_values}}) {
    $_->write( $dest );
  }

}

sub read { 
  my $self = shift; 
  my $source = shift; 
  
  my $input; 
  read $source, $input, 5; 
 
  my ( $length ) = unpack ("xN", $input); 

  $self->{pdata_values} = [ ]; 
  while ($length > 0) {
    my $pdv = new DICOM::PDataValue;
    $pdv->read( $source );
    $self->{pcid} = $pdv->{pcid}; 
    #print " PDV: $pdv->{messcontrol} ${pdv}->getlength()\n";  
    $length -= $pdv->getlength() + 4;
    push @{$self->{pdata_values}}, $pdv;
  } 
  
  return 1;
}

sub parse_pdata {
  my $self = shift;
  my $data = shift;
  my $messcontrol = shift;
  
  $self->{pdata_values} = [ ];
  my $pdv = new DICOM::PDataValue;

  $pdv->parse( $data, $messcontrol);

  push @{$self->{pdata_values}}, $pdv;

}

sub getbinary { 
  my $self = shift; 
  my $message_type = shift; 
  my $message_offset = shift || 0; 
  
  my $mess_num = 0; 
  my $data = ""; 
  for (my $i=0; $i < @{$self->{pdata_values}}; $i++) { 
    my $pdv = @{$self->{pdata_values}}[$i]; 
    #print " PDV contents: ", $pdv->{messcontrol}, " " , $pdv->getlength() , "\n"; 

    if (($pdv->{messcontrol} & 1) == $message_type) {
      if ($mess_num==$message_offset) { $data .= $pdv->{data}; } 
      if ($pdv->{messcontrol} & 2) { $mess_num++; } 
    }
  }

  return $data; 
}

1;
