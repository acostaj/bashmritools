
package DICOM::DICOMObject; 

use DICOM::DICOMElement;
use DICOM::Dictionary;
use strict; 

sub new { 
  my $class = shift;
  my $rawdata = shift; 

  my $elements = {};
  bless $elements, $class;
  
  if (defined $rawdata) { 
	$elements->parse( $rawdata ); 
  }

  return $elements; 
}

sub getlength { 
  my $self = shift; 
  my $length = 0; 
  
  foreach my $gref (sort { hex($a) <=> hex($b) } keys (%{$self}) ) { 
    my %group = %{$self->{$gref}}; 
    foreach my $eref (sort { hex($a) <=> hex($b) } keys (%group) ) { 
      $length += $group{$eref}->getlength() + 8; 
    }
  }
  return $length; 
}

sub addElement { 
  my $self = shift; 
  my $group = shift; 
  my $element = shift; 
  my $contents = shift; 

  if ( ! defined $self->{$group} ) { 
    $self->{$group} = (); 
  }
  my $dcmelement = new DICOM::DICOMElement; 
  $dcmelement->setValue( $group, $element, $contents ); 
  $self->{$group}{$element} = $dcmelement; 

}

sub get_element { 
  my $self = shift; 
  my $group = shift; 
  my $element = shift; 
  
  if ( ! defined $self->{$group} ) {
    return ""; 
  }
  if ( ! defined $self->{$group}{$element} ) { 
    return ""; 
  } 
 
  return $self->{$group}{$element}->{data}; 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 
  
  foreach my $gref (sort { hex($a) <=> hex($b) } keys (%{$self}) ) { 
    my %group = %{$self->{$gref}}; 
    foreach my $eref (sort { hex($a) <=> hex($b) } keys (%group) ) { 
      $group{$eref}->write( $dest ); 
    }
  }

}

sub getbinary { 
  my $self = shift; 
  
  my $data = ""; 
  foreach my $gref (sort { hex($a) <=> hex($b) } keys (%{$self}) ) { 
    my %group = %{$self->{$gref}}; 
    foreach my $eref (sort { hex($a) <=> hex($b) } keys (%group) ) { 
      $data .= $group{$eref}->getbinary( ); 
    }
  }
  return $data;  
}

sub parse { 
  my $self = shift; 
  my $data = shift; 
  my $offset = shift || 0; 
  my $max = shift || length( $data ); 
  
  my $isexplicit = shift || 0; 

  my $done = 0;
  my $ismedia = 0;
  # Look to see if this is a media file if this is the first call 
  if ((length($data) > 132) && ($offset==0)) { 
  	my ($code) = unpack("x128A4", $data);
	if ($code eq "DICM") { 
		$ismedia = 1; 
		$isexplicit = 1;	
		$done = 132;	
	}
  }

  
  while (($offset + $done < $max) || ($max == -1) ) { 
    if (($ismedia) && ($done==$self->{'0002'}{'0000'}->{data}+144)) { 
   	my ($tuid) = ($self->{'0002'}{'0010'}->{data} =~ m/([\d\.]*)/);
	$ismedia = 0; 
	if ($tuid eq "1.2.840.10008.1.2") { 
		$isexplicit = 0;
	} elsif ($tuid eq "1.2.840.10008.1.2.1") {
		$isexplicit = 1; 
	} else { die ("Can't do transfer syntax $tuid\n"); }
    }
    my $dcmelement = new DICOM::DICOMElement;
    my $length = $dcmelement->parse( $data, $offset + $done, $isexplicit ); 
    $done += $length; # + 8; 

    my $group = $dcmelement->{group}; 
    my $elem = $dcmelement->{elem}; 
   
    if (($max == -1) && ($group == "fffe") && ($elem == "e00d") ) { last; }

    $self->{$group}{$elem} = $dcmelement; 
    
    #$offset += $dcmelement->getlength() + 8; 
    #$offset += $length + 8; 
  
  }
  return $done;
}

sub setgrouplength { 
  my $self = shift; 
  my $group = shift; 
  my $length = 0; 

  my %group = %{$self->{$group}}; 
  foreach my $eref (keys (%group)) { 
    if ($eref eq '0000') {next;}
    $length += $self->{$group}{$eref}->getlength() + 8; 
  }
  $self->addElement( $group, '0000', $length ); 
}

sub printcontents { 
  my $self = shift; 
  my $indent = shift | 0;

  foreach my $gref (sort { hex($a) <=> hex($b) } keys (%{$self}) ) { 
    my %group = %{$self->{$gref}}; 
    foreach my $eref (sort { hex($a) <=> hex($b) } keys (%group) ) { 
      my $elem = $group{$eref}; 
      $elem->printcontents($indent); 
#      print "$gref $eref "; 
#      print pack("A35", DICOM::Dictionary::getname( $gref, $eref )); 
#      if (DICOM::Dictionary::is_unprintable( $gref, $eref )) { 
#	print "<Data>\n"; 
#      } else { 
#	print $elem->{data}, "\n"; 
#	print "\n"; 
#      } 
    }
  }
}

1;

__END__

=head1 NAME

DICOM::DICOMObject - Perl extension for handling DICOM objects

=head1 SYNOPSIS

  use DICOM::DICOMObject; 
  
  # Loading up a DICOM Object
  open (INPUT, "<dump.0") or die "Can't open image"; 
  $/ = undef; 
  my $data = <INPUT>; 
  $/="\n"; 
  close INPUT; 
  my $image = new DICOM::DICOMObject( $data );  
  
  # Printing out a DICOM Object
  $image->printcontents(); 

  # Saving a DICOM Object
  open (DEST, ">outdump") or die "Can't open"; 
  $image->write(*DEST);
  close(DEST); 

=head1 DESCRIPTION 

This module contains the DICOM Object. For more info see the source code.

=head1 AUTHOR

Guy Williams, gbw1000@wbic.cam.ac.uk

=cut

  
