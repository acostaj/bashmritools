
package DICOM::AssociateRQ; 

use DICOM::Associate; 
use strict;
use vars qw( @ISA );

@ISA =  qw/DICOM::Associate/; 

sub new {
 my $class = shift;
 my $elements = {};
 
 $elements = $class->SUPER::new( @_ );
 $elements->{pdutype} = 1; 
 bless $elements, $class;
 
 #$elements->{userinfo}->printcontents(); 

 return $elements;
}

1;
