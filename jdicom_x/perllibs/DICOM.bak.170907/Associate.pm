
package DICOM::Associate; 
use strict; 
use vars qw( @ISA );

require DICOM::UserInfo; 
require DICOM::PDUItem; 
require DICOM::PresContext; 

my $appcontext; 
my @prescontext; 
my $userinfo; 

sub new {
 my $class = shift;
 my $elements = {};

 bless $elements, $class;
 $elements->{protocol} = 1;  

 $elements->{remoteae} = "neurol1"; 
 $elements->{localae} = "neurol1_IJ"; 
  
 $elements->{appcontext} = new DICOM::PDUItem;
 $elements->{appcontext}{uid} = "1.2.840.10008.3.1.1.1"; # Application context
 $elements->{appcontext}{itemtype} = 16; 
 
 my $i;
 for ($i=0; $i < scalar @_; $i++) {
   if ($_[$i] eq "LocalAE") { $elements->{localae} = $_[$i+1]; $i++; }
   elsif ($_[$i] eq "RemoteAE") { $elements->{remoteae} = $_[$i+1]; $i++; }
 }

 my $prescont = new DICOM::PresContext; 
 my @presarray = ( $prescont ); 

 $elements->{prescontext} = \@presarray; 

 $elements->{userinfo} = new DICOM::UserInfo; 
  
 return $elements;
}

sub addprescontext { 
  my $self = shift; 
  
  my %tt = ( @_ );

  my $id = $tt{ID} || ( ( 2 * (scalar @{$self->{prescontext}}) ) + 1 ); 
  $tt{ID} = $id; 
  
  my $pc = new DICOM::PresContext (  %tt ); 

  push @{$self->{prescontext}}, $pc; 
}

sub getprescontextid { 
  my $self = shift; 
  my $abs = shift; 

  my ($abs_m) = ($abs =~ m/([\d,.]*)/); 

  my $id = 0; 
  foreach my $pc (@{$self->{prescontext}} ) { 
    my ($test_uid) = ($pc->{abs_syntax}{uid} =~ m/([\d,.]*)/); 
    if ($test_uid eq $abs_m) { $id = $pc->{pcid}; } 
  }

  return $id; 
}

sub checkprescontextid { 
  my $self = shift; 
  my $pcid = shift; 

  my $res = -1; 
  foreach my $pc (@{$self->{prescontext}} ) { 
    if ($pc->{pcid} == $pcid) { $res = $pc->{result}; } 
  }

  return $res; 
}

sub getlength() { 
  my $self = shift; 
  my $length = $self->{appcontext}->getlength() + 4; 

  foreach (@{$self->{prescontext}}) {
    $length += $_->getlength() + 4; 
  }
  $length += $self->{userinfo}->getlength() + 4; 
  $length += 68; 
  return $length; 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 
  
  print $dest pack("c", $self->{pdutype} ); 
  print $dest pack("c", 0);

  print $dest pack("N", $self->getlength() ); 
  print $dest pack("n", $self->{protocol} );

  print $dest pack("n", 0 );
  print $dest pack("A16", $self->{remoteae} );
  print $dest pack("A16", $self->{localae} );
  print $dest pack("x32");

  $self->{appcontext}->write($dest); 

  foreach (@{$self->{prescontext}}) { 
   $_->write( $dest ); 
  }

  $self->{userinfo}->write($dest); 
}

sub read { 
  my $self = shift; 
  my $source = shift; 
  
  $self->{userinfo} = undef; 
  $self->{appcontext} = undef; 
  $self->{prescontext} = [ ]; 

  my $input; 
  read $source, $input, 5; 
 
  my ( $length ) = unpack ("xN", $input); 

  read $source, $input, 68; 

  my ( $protocol, undef, $remoteae, $localae ) = 
    unpack ("nnA16A16x", $input); 
    
  $self->{remoteae} = $remoteae; 
  $self->{localae} = $localae; 
  $self->{protocol} = $protocol; 

  $length -= 68; 

  $self->{prescontext} = [];
   
  while ($length > 0) { 
    read $source, $input, 1;
    my $pdutype = unpack ("c", $input); 
    if (($pdutype==33) || ($pdutype==32)) {  
        my $prescontext = new DICOM::PresContext; 
        if ( ! $prescontext->read( $source ) ) { 
		$DICOM::Associate::errstr = "Problems reading PresContext"; 
		return 0;
	}
        $length -= $prescontext->getlength() + 4;  
	push @{$self->{prescontext}}, $prescontext; 	
      
    } elsif ($pdutype==16) {  # App Context 
	my $pdu = new DICOM::PDUItem; 
	$pdu->{itemtype} = $pdutype; 
	$pdu->read( $source ); 
        $length -= $pdu->getlength() + 4;
	$self->{appcontext} = $pdu; 
      
    } elsif ($pdutype==80) { # User Info 
	my $pdu = new DICOM::UserInfo; 
	$pdu->{itemtype} = $pdutype; 
	$pdu->read( $source ); 
        $length -= $pdu->getlength() + 4;
        $self->{userinfo} = $pdu; 
      
    } else {  
	if ($self->{pdutype} == 1) { 
		$DICOM::Associate::errstr = "Having problems reading Associate-RQ  !"; 
		return 0;
	} 
	else { 
		$DICOM::Associate::errstr = "Having problems reading Associate-AC  !"; 
		return 0;
	} 
    }
  }
  return 1; 
}

sub printcontents { 
  my $self = shift; 
  
  print "AssociationRQ/RP: \n"; 
  print " RemoteAE:\t" . $self->{remoteae} . "\n"; 
  print " LocalAE:\t" . $self->{localae} . "\n"; 
  print " Protocol:\t" . $self->{protocol} . "\n"; 
  if (defined $self->{appcontext}) { $self->{appcontext}->printcontents(); } 
  foreach my $pc (@{$self->{prescontext}}) { 
	$pc->printcontents(); 
  }
  if (defined $self->{userinfo}) { $self->{userinfo}->printcontents(); } 

}
1;
