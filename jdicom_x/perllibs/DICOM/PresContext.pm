# DICOM::PresContext.pm
#
# Copyright (c) 2007 Guy B. Williams <gbw1000@wbic.cam.ac.uk>. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package DICOM::PresContext; 
use strict; 

use DICOM::PDUItem;

my $abs_syntax; 
my @trans_syntax; 

sub new { 
  my $class = shift;
  my $elements = {};
  bless $elements, $class;
  
  $elements->{pcid} = 1; 
  $elements->{itemtype} = 32; 

  my $abs = new DICOM::PDUItem; 
  $abs->{uid} = "1.2.840.10008.5.1.4.1.2.2.1"; # Study Root Query
  $abs->{itemtype} = 48; 

  my $trans = new DICOM::PDUItem; 
  $trans->{uid} = "1.2.840.10008.1.2"; # Little Endian transfer
  $trans->{itemtype} = 64; 
  
  my $i;
  for ($i=0; $i < scalar @_; $i++) {
    if ($_[$i] eq "ID") { $elements->{pcid} = $_[$i+1]; $i++; }
    elsif ($_[$i] eq "AbsSyntax") { $abs->{uid} = $_[$i+1]; $i++; 
	$abs->{uid} =~ s/\s//g;
	$elements->{itemtype} = 32; 
    }
    elsif ($_[$i] eq "Result") { $elements->{result} = $_[$i+1]; $i++; 
	$elements->{itemtype} = 33;  
    }
  }
      
  if ($elements->{itemtype} == 32) { $elements->{abs_syntax} = $abs; } 
  $elements->{trans_syntax} = [ $trans ]; 

  return $elements; 
}

sub get_length { 
  my $self = shift; 
  my $length = 4; # 0; 
  if (defined $self->{abs_syntax}) { $length += $self->{abs_syntax}->get_length() + 4; }  
  foreach (@{$self->{trans_syntax}}) { 
    $length += $_->get_length() + 4; # 8;
  }
  return $length; 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 

  print $dest pack("c", $self->{itemtype} );
  print $dest pack("c", 0);
  print $dest pack( "n", $self->get_length() ); 
  
  print $dest pack("c", $self->{pcid});
  print $dest pack("c", 0);
  print $dest pack("c", ($self->{result} || 0));
  print $dest pack("c", 0); 
 
  if ($self->{itemtype}==32) { $self->{abs_syntax}->write($dest); } 
  foreach (@{$self->{trans_syntax}}) {
    $_->write($dest);
  }
}

sub read { 
  my $self = shift; 
  my $source = shift; 
  
  # First byte (33) already read

  my $input; 
  read $source, $input, 7; 
  my ( $length, $id, $result ) = unpack("xncxcx", $input); 	   
  $length -= 4; 
  
  $self->{pcid} = $id; 
  $self->{result} = $result; 
  $self->{trans_syntax} = [ ]; 
  $self->{abs_syntax} = undef; 

  while ($length > 0) {
    read $source, $input, 1; 
    my ( $item ) = unpack("c", $input);
    if (($item != 64) && ($item != 48)) { 
    	return 0;	
	# die "Having trouble reading pres context!\n"; 
    }

    my $subpdu = new DICOM::PDUItem; 
    $subpdu->{itemtype} = $item; 
    $subpdu->read( $source ); 
    
    if ($item==64) { 
      push @{$self->{trans_syntax}}, $subpdu; 
    } else { 
      $self->{abs_syntax} = $subpdu;
    }
    $length -= $subpdu->get_length() + 4;
  }
  return 1; 
}

sub print_contents { 
  my $self = shift; 

  print " PresContext: ID         : " . $self->{pcid} . "\n"; 
  if (defined $self->{abs_syntax}) { 
    print "              AbsSyntax  : " . $self->{abs_syntax}->{uid} . "\n"; 
  }
  if (defined $self->{result}) { 
    print "              Result     : " . $self->{result} . "\n";  
  }
  foreach my $trans (@{$self->{trans_syntax}}) { 
  	print "              TransSyntax: " . $trans->{uid} . "\n"; 
  } 
}

1;
