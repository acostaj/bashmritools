# DICOM::PDataValue.pm
#
# Copyright (c) 2007 Guy B. Williams <gbw1000@wbic.cam.ac.uk>. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package DICOM::PDataValue; 
use strict; 

sub new { 
 my $class = shift;  
 
 my $pcid = shift; 
 my $messctrl = shift; 
 my $pdata = shift;

 my $pdu = {}; 
 bless $pdu, $class; 
 $pdu->{pcid} = 1; 
 
 $pdu->{pcid} = $pcid; 
 $pdu->{messcontrol} = $messctrl;
 $pdu->{data} = $pdata; 
 
 return $pdu; 
}

sub get_length { 
  my $self = shift;  
  return (length($self->{data}) + 2); 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 
  
  print $dest pack("N", $self->get_length());
  print $dest pack("c", $self->{pcid});
  print $dest pack("c", $self->{messcontrol}); 
  print $dest $self->{data}; 

}

sub read { 
  my $self = shift; 
  my $source = shift; 

  my $input;
  my $data; 
  read $source, $input, 6; 
  my ($length, $pcid, $messcontrol) = unpack("Ncc", $input); 

  if ($length > 2) { 
  	read $source, $data, ($length-2); 
  }
  $self->{data} = $data; 
  $self->{pcid} = $pcid; 
  $self->{messcontrol} = $messcontrol; 

}

sub parse { 
  my $self = shift; 
  my $data = shift; 
  my $messcontrol = shift; 

  $self->{data} = $data; 
  $self->{messcontrol} = $messcontrol; 

}


1;

