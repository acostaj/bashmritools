# DICOM::Transaction.pm
#
# Copyright (c) 2007 Guy B. Williams <gbw1000@wbic.cam.ac.uk>. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package DICOM::Transaction; 

require IO::Socket::INET;
#require IO::Socket::SSL;
require DICOM::PDU; 
require DICOM::Abort; 
require DICOM::AssociateAC; 
require DICOM::AssociateRQ; 
require DICOM::ReleaseRQ; 
require DICOM::ReleaseRP; 
require DICOM::PDataTF; 
require DICOM::DICOMObject; 

use strict; 
use vars qw( $VERSION ); 

$VERSION = "0.10"; 

sub new {
 my $class = shift;
 my $elements = {};
 
 $elements->{localae} = "localhost"; 
 $elements->{remoteae} = "localhost"; 
 $elements->{remoteip} = "localhost"; 
 $elements->{port} = 104; 
 $elements->{message} = 1; 
 $elements->{ssl} = 0; 
 $elements->{use_cert} = 0; 
 $elements->{key} = $ENV{HOME} . "/.dicom/client-key.pem"; 
 $elements->{cert} = $ENV{HOME} . "/.dicom/client-cert.pem"; 

 my $i; 
 for ($i=0; $i < scalar @_; $i++) {
   if ($_[$i] eq "LocalAE") { $elements->{localae} = $_[$i+1]; $i++; } 
   elsif ($_[$i] eq "RemoteAE") { $elements->{remoteae} = $_[$i+1]; $i++; } 
   elsif ($_[$i] eq "RemoteIP") { $elements->{remoteip} = $_[$i+1]; $i++; } 
   elsif ($_[$i] eq "Port") { $elements->{port} = $_[$i+1]; $i++; } 
   elsif ($_[$i] eq "Key") { $elements->{key} = $_[$i+1]; $i++; } 
   elsif ($_[$i] eq "Cert") { $elements->{cert} = $_[$i+1]; $i++; } 
   elsif ($_[$i] eq "SSL") { $elements->{ssl} = 1; } 
   elsif ($_[$i] eq "UseCert") { $elements->{use_cert} = 1; } 
 }

 if ($elements->{ssl}) { 
	if (! eval "require IO::Socket::SSL") {
		die "Cannot load IO::Socket::SSL\n";
	} else { 
 		require IO::Socket::SSL;
	}
 }

 $elements->{connect_assoc} = new DICOM::AssociateRQ( 'LocalAE' => $elements->{localae}, 
					       'RemoteAE' => $elements->{remoteae} ); 
 
 bless $elements, $class;
 return $elements;
}

sub connect { 
 my $self = shift; 
 
 my $sock;
 
 if ($self->{ssl}) { 
 	if ( ( ! -f $self->{key} ) || ( ! -f $self->{cert} ) ) { 
		 $self->{use_cert} = 0;
	}
	$sock = new IO::Socket::SSL (
			  PeerAddr => $self->{remoteip},
			  PeerPort => $self->{port},
			  Proto => 'tcp',
			  SSL_verify_mode => "SSL_VERIFY_NONE",
			  SSL_use_cert => $self->{use_cert},
			  SSL_key_file => $self->{key},
			  SSL_cert_file => $self->{cert},
			);
 } else { 
 	$sock = new IO::Socket::INET (
			  PeerAddr => $self->{remoteip},
			  PeerPort => $self->{port},
			  Proto => 'tcp',
			);
 }
 if ( ! $sock ) { 
 	$DICOM::Transaction::errstr = "Could not create socket: $!";
	return 0;
 }
 
 $self->{link} = $sock; 
 $self->{connect_assoc}->write( $self->{link} ); 
 
 $self->{response_assoc} = new DICOM::PDU( $self->{link} ); 
 if ( ! $self->{response_assoc} ) { 
	$DICOM::Transaction::errstr = "Problem reading response association"; 
	return 0;
 }
 $self->{connected} = 1; 
 
 $self->set_max_size(); 

 if ($self->{response_assoc}->{pdutype} != 2) { 
 	$DICOM::Transaction::errstr = "Association Rejected";
	return 0;
 }
 return 1;
}

sub close { 
  my $self = shift;
  my $assoc_release = new DICOM::ReleaseRQ; 
  $assoc_release->write( $self->{link} );
  my $acknowledgement = new DICOM::PDU( $self->{link} ); 
  $self->{connected} = 0; 
}

sub abort { 
  my $self = shift;
  my $assoc_release = new DICOM::Abort; 
  $assoc_release->write( $self->{link} );
  
  $self->{connected} = 0; 
}

sub reject { 
  my $self = shift; 
  my $result = shift || 1; 
  my $reason = shift || 1; 
  my $source = shift || 1; 

  if ( ! defined $self->{connect_assoc} ) { 
    $DICOM::Transaction::errstr = "AssociateRQ not defined!"; 
    return 0; 
  }

  my $assorj = new DICOM::AssociateRJ( Result => $result,
					Reason => $reason,
					Source => $source
				); 
  $assorj->write( $self->{link} );

  return 1; 
}

sub make_assoc_response { 
  my $self = shift; 
  
  if ( ! defined $self->{connect_assoc} ) { 
    $DICOM::Transaction::errstr = "AssociateRQ not defined!"; 
    return 0; 
  }

  my $assorsp = new DICOM::AssociateAC( RemoteAE => $self->{connect_assoc}->{remoteae},
					LocalAE => $self->{connect_assoc}->{localae}
				); 
  $assorsp->{prescontext} = [ ]; 
  foreach my $pc (@{$self->{connect_assoc}->{prescontext}} ) {
   	my $res = 3; 			# Default: Abs not supported 
 	
	if ( ! grep "1.2.840.10008.1.2", $pc->{trans_syntax} ) { 
		$res = 4;		# Transfer syntax not supported 
	} 
	elsif ( grep /^$pc->{abs_syntax}->{uid}$/, @_ ) { 
		$res = 0; 		# Success 
	}
	
	$assorsp->add_prescontext( ID => $pc->{pcid}, Result => $res ); 
  }
  
  $self->{response_assoc} = $assorsp;
  $self->{response_assoc}->write( $self->{link} ); 
  return 1;
}

sub add_prescontext { 
  my $self = shift; 

  return $self->{connect_assoc}->add_prescontext( @_ ); 
}

#----------------------------------------------------------------------------------
#- High level commands
#----------------------------------------------------------------------------------
#
# Syntax: dofind( <Callback => $routine>, <ID qualifiers> )
#
#----------------------------------------------------------------------------------

sub dofind { 
  my $self = shift; 
  
  my $pcid = $self->{connect_assoc}->get_prescontext_id( "1.2.840.10008.5.1.4.1.2.2.1" )
    or do {
    	$DICOM::Transaction::errstr = "C-FIND not negotiated with server"; 
  	return 0;
    };
  if ($self->{response_assoc}->check_prescontext_id( $pcid ) != 0) { 
    	$DICOM::Transaction::errstr = "C-FIND was rejected by server"; 
  	return 0;
  }

  my %tt = ( @_ );
  my $progress = $tt{Callback}; 

  my %params; 

  my $i; 
  for ($i=0; $i < scalar @_; $i++) {
    if ($_[$i] eq "Level") { $params{level} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "PatName") { $params{patname} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "PatID") { $params{patid} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "Date") { $params{date} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "AccNum") { $params{accnum} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "StudyUID") { $params{studyuid} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "RefPhyNam") { $params{refphynam} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "StudyDes") { $params{studydes} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "Mod") { $params{modality} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "SeriesUID") { $params{seriesuid} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "SeriesDes") { $params{seriesdes} = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "ImageUID") { $params{imageuid} = $_[$i+1]; $i++; } 
  }
  if ( ! defined ( $params{level} ) ) { $params{level} = "STUDY"; } 
  
  my $dcmq = new DICOM::DICOMObject;
  $dcmq->add_element( '0008', '0052', $params{level} ); # QueryRetrieveLevel
  if ( $params{level} eq 'STUDY' ) { 
    $dcmq->add_element( '0010', '0010', ( $params{patname} || '' )); # Patient Name
    $dcmq->add_element( '0010', '0020', ( $params{patid} || '' )); # Patient ID
    $dcmq->add_element( '0010', '0030', ( $params{birthd} || '' )); # Patient Birth Date 
    $dcmq->add_element( '0010', '0032', ( $params{birtht} || '' )); # Patient Birth Time 
    $dcmq->add_element( '0010', '0040', ( $params{sex} || '' )); # Patient Sex
    $dcmq->add_element( '0010', '1010', ( $params{age} || '' )); # Patient Age
    $dcmq->add_element( '0010', '1020', ( $params{size} || '' )); # Patient Size 
    $dcmq->add_element( '0010', '1030', ( $params{weight} || '' )); # Patient Weight 
    $dcmq->add_element( '0008', '0020', ( $params{date} || '' )); # Study Date
    $dcmq->add_element( '0008', '0030', ( $params{time} || '' )); # Study Time
    $dcmq->add_element( '0008', '0050', ( $params{accnum} || '' )); # Accession Number 
    $dcmq->add_element( '0008', '0090', ( $params{refphynam} || '' )); # Referring Physician 
    $dcmq->add_element( '0008', '1030', ( $params{studydes} || '' )); # Study Description 
    $dcmq->add_element( '0020', '000d', ( $params{studyuid} || '' )); # Study UID 
    $dcmq->add_element( '0020', '0010', ( $params{studyid} || '' )); # Study ID 
  } elsif ( $params{level} eq 'SERIES' ) { 
    $dcmq->add_element( '0020', '000d', ( $params{studyuid} || '' )); # Study UID 
    $dcmq->add_element( '0020', '000e', ( $params{seriesuid} || '' )); # Series UID 
    $dcmq->add_element( '0020', '0011', ( $params{seriesno} || '' )); # Series Number 
    $dcmq->add_element( '0018', '1030', ( $params{pronam} || '' )); # Protocol Name 
    $dcmq->add_element( '0008', '103e', ( $params{seriesdes} || '' )); # Series Description 
    $dcmq->add_element( '0008', '0060', ( $params{modality} || '' )); # Modality 
  } elsif ( $params{level} eq 'IMAGE' ) { 
    $dcmq->add_element( '0020', '0013', '' ); # Instance Number 
    $dcmq->add_element( '0020', '000d', ( $params{studyuid} || '' )); # Study UID 
    $dcmq->add_element( '0020', '000e', ( $params{seriesuid} || '' )); # Series UID 
    $dcmq->add_element( '0008', '0018', ( $params{imageuid} || '' )); # Image UID 
  } else { 
  	$DICOM::Transaction::errstr = "Bad search level"; 
	return 0;
  }
  
  $self->{message}++; 

  my $dcmc = $self->make_RQ_command( 	32, 	# Command Field. See 3.7 p38
					'1.2.840.10008.5.1.4.1.2.2.1'
				); 

  my @query_results; 
  
  my $collate = sub { 
    my $status = $_[0]; 
    my $dcmdata = $_[6]; 
    
    if (defined $dcmdata) { 
    	
    	my %values; 
	$values{patname} = $dcmdata->get_element('0010', '0010'); # Patient Name 
    	$values{patid} = $dcmdata->get_element('0010', '0020'); # Patient ID
    	$values{date} = $dcmdata->get_element('0008', '0020'); # Date 
    	$values{accnum} = $dcmdata->get_element('0008', '0050'); # Accession Number 
    	$values{refphynam} = $dcmdata->get_element('0008', '0090'); # Referring Physician 
    	$values{studydes} = $dcmdata->get_element('0008', '1030'); # Study Description 
    	$values{seriesdes} = $dcmdata->get_element('0008', '103e'); # Series Description 
    	$values{pronam} = $dcmdata->get_element('0018', '1030'); # Protocol Name 
    	$values{studyuid} = $dcmdata->get_element('0020', '000d'); # Study UID 
    	$values{seriesuid} = $dcmdata->get_element('0020', '000e'); # Series UID 
    	$values{seriesno} = $dcmdata->get_element('0020', '0011'); # Series Number 
    	$values{modality} = $dcmdata->get_element('0008', '0060'); # Modality 
    	$values{imageuid} = $dcmdata->get_element('0008', '0018'); # Image UID 
    
    	push @query_results, \%values; 
    } 
    my $ret_value = 1; 

    if (defined $progress) { $ret_value = &$progress( @_ ); } 
    if ($ret_value == 0) { return 0; } 

    if (($status != hex('FF00')) && ($status != hex('FF01'))) { return 0; }
    return 1; 
  };
  
#  $self->request_operation( $dcmc, $dcmq, $collate, $storage ); 
  $self->request_operation( $dcmc, $dcmq, $collate ); 

  return @query_results; 
}

#----------------------------------------------------------------------------------
#
# Syntax: doget( <Callback => $routine>, <ID qualifiers> )
#
#----------------------------------------------------------------------------------

sub doget { 
  my $self = shift; 
   
  if ( ! $self->{connected} ) { 
    $DICOM::Transaction::errstr = "Not connected"; 
    return 0; 
  }

  if ( $self->{response_assoc}->{pdutype} != 2) { 
    $DICOM::Transaction::errstr = "Remote device refused connection"; 
    return 0; 
  }

  $self->{message}++; 
  
  my %tt = ( @_ );
  
  my $progress = $tt{Callback}; 
  my $recimg = $tt{RecImage}; 

  my $dcmq = $self->make_identifier( %tt ); 
  my $dcmc = $self->make_RQ_command( 	16, 	# Command Field. See 3.7 p38
					'1.2.840.10008.5.1.4.1.2.2.3'
				); 
  my $prog_show = sub { 
    my $ret_value = 1; 

    if (defined $progress) { $ret_value = &$progress( @_ ); } 
    if ($ret_value == 0) { return 0; } 

    my $status = shift; 

    if (($status != hex('FF00')) && ($status != hex('FF01'))) { return 0; }
    return 1; 
  };
  
  my @data_array = (); 
  my $storage = sub { 
    my $pcid = shift; 
    my $command = shift; 
    my $data = shift; 

    if (! defined $recimg) { 
    	push @data_array, $data; 
    } else { 
   	&$recimg( $data ); 
    }
    $self->send_rsp( 0, $pcid, $command); 
  };
  

  $self->request_operation( $dcmc, $dcmq, $prog_show, $storage ); 

  return @data_array;

}

#----------------------------------------------------------------------------------
#
# Syntax: domove( <Callback => $routine>, <ID qualifiers> )
#
#----------------------------------------------------------------------------------

sub domove { 
  my $self = shift; 
   
  if ( ! $self->{connected} ) { 
    $DICOM::Transaction::errstr = "Not connected"; 
    return 0; 
  }

  if ( $self->{response_assoc}->{pdutype} != 2) { 
    $DICOM::Transaction::errstr = "Remote device refused connection"; 
    return 0; 
  }

  my %tt = ( @_ );
  my $dest = $tt{Destination} || ""; 
  my $progress = $tt{Callback}; 

  my $dcmq = $self->make_identifier( %tt ); 
  my $dcmc = $self->make_RQ_command( 	33, 	# Command Field. 
					'1.2.840.10008.5.1.4.1.2.2.2' , 
				 	$dest	
				); 
  my $prog_show = sub { 
    my $ret_value = 1; 

    if (defined $progress) { $ret_value = &$progress( @_ ); } 
    if ($ret_value == 0) { return 0; } 

    my $status = shift; 

    if (($status != hex('FF00')) && ($status != hex('FF01'))) { return 0; }
    return 1; 
  };
 
  return $self->request_operation( $dcmc, $dcmq, $prog_show ); 

}

#----------------------------------------------------------------------------------
#
# Syntax: dostore( $dcm_data_object, $move_originator, $move_message_id )
#
#----------------------------------------------------------------------------------

sub dostore { 
  my $self = shift; 
  my $dcmd = shift; 
  my $move_originator = shift || ""; 
  my $move_message_id = shift || 1; 

  my $store_uid = $dcmd->get_element('0008', '0016');
 
  my $pcid = $self->{connect_assoc}->get_prescontext_id( $store_uid );
  if (! $pcid ) { 
	$DICOM::Transaction::errstr = "Have not negotiated a transfer syntax for this storage class ($store_uid)"; 
 	return 0; 
  }
  if ($self->{response_assoc}->check_prescontext_id( $pcid ) != 0) { 
  	$DICOM::Transaction::errstr =  "$store_uid was rejected by server"; 
 	return 0; 
  }
  
  $self->{message}++; 

  my $dcmc = new DICOM::DICOMObject;

  $dcmc->add_element( '0000', '0002', $store_uid ); # Affected SOP Class UID
  $dcmc->add_element( '0000', '0100', 1 ); # Command field. 
  $dcmc->add_element( '0000', '0110', $self->{message} ); # Message ID
  $dcmc->add_element( '0000', '0700', 0 ); # Priority
  $dcmc->add_element( '0000', '0800', 258 ); # Data set type
  $dcmc->add_element( '0000', '1000', $dcmd->get_element('0008', '0018')  ); # SOP Instance UID 
  $dcmc->add_element( '0000', '1030', $move_originator  ); # Move originator
  $dcmc->add_element( '0000', '1031', $move_message_id  ); # Move originator message id 

  my $status; 
  my $progress = sub { $status = $_[0]; return 0; };
  $self->request_operation( $dcmc, $dcmd, $progress );

  return $status; 
}

#----------------------------------------------------------------------------------
#
# Syntax: doecho( )
#
#----------------------------------------------------------------------------------

sub doecho { 
  my $self = shift;

  $self->{message}++;
  my $dcmc = new DICOM::DICOMObject;
  $dcmc->add_element( '0000', '0002', "1.2.840.10008.1.1" ); # Affected SOP Class UID
  $dcmc->add_element( '0000', '0100', 48 ); # Command field.
  $dcmc->add_element( '0000', '0110', $self->{message} ); # Message ID
  $dcmc->add_element( '0000', '0800', 257 ); # Data set type
  
  my $progress = sub { return 0; };
  $self->request_operation( $dcmc, undef, $progress );
  
  return 1; 
}

#----------------------------------------------------------------------------------
#
# Syntax: listen
#
#----------------------------------------------------------------------------------

sub listen  { 
  my $self = shift;
	
  if (! defined $self->{socket} ) { 

  	my $sock = new IO::Socket::INET (
				LocalPort => $self->{port}, 
				Proto => 'tcp',
				Listen => 1,
				Reuse => 1,
			);
	if (! $sock ) { 
		$DICOM::Transaction::errstr = "Could not create socket: $!";
		return 0;
	}
  	$self->{socket} = $sock;
  }

  $self->{link} = $self->{socket}->accept();

  my $req = new DICOM::PDU( $self->{link} );
  $self->{remoteip} = $self->{link}->peerhost;
  $self->{remoteae} = $req->{remoteae};
  $self->{localae} = $req->{localae};
  $self->{connect_assoc} = $req; 
 
  $self->set_max_size();
  if ($req->{pdutype} != 1) {
  	$DICOM::Transaction::errstr = "Unexpected PDU type " . $req->{pdutype};
	return 0;
  }
  return 1; 
}

#----------------------------------------------------------------------------------
#-Internal Commands
#----------------------------------------------------------------------------------
#
# Syntax: request_operation 
#
#----------------------------------------------------------------------------------

sub request_operation { 
  my $self = shift; 
  
  my $rq_command = shift; 
  my $rq_data = shift; 
  
  my $progress = shift; 
  my $storage = shift; 
  
  my $abs_uid = $rq_command->get_element( '0000', '0002' ); # Affected SOP Class UID
  $abs_uid =~ s/ //g;

  my $pcid = $self->{connect_assoc}->get_prescontext_id( $abs_uid )
    or do { 
    	$DICOM::Transaction::errstr = "Abstract Syntax $abs_uid not negotiated with server"; 
  	return 0;
    };
  if ($self->{response_assoc}->check_prescontext_id( $pcid ) != 0) { 
  	$DICOM::Transaction::errstr = "$abs_uid was rejected by server"; 
	return 0;
  }
 
  $rq_command->set_group_length( '0000' );	# Needed for some servers to work
  $self->send_data( $self->{link}, $pcid, $rq_command, $rq_data );

  my $type; 

  while(1) {
  
  	my ($typel, $dcmresp, $dcmdata, $rpcid) = $self->receive_data( ); 
  	$type = $typel; 
	
	if ($type != 4) { 
		$type = 0; 
		last; 
	}
	
	#print "INCOMING COMMAND: \n"; 
	#$dcmresp->print_contents(); 
	if (defined $dcmdata) { 
		#print "INCOMING DATA: \n";
		#$dcmdata->print_contents();
	}

	if ($rpcid != $pcid) { 
		if (! defined $storage) { 
			$DICOM::Transaction::errstr = "Incoming data: No processing routine defined";  
			return 0;
		}
		&$storage( $rpcid, $dcmresp, $dcmdata); 
	} else { 
		my $status = $dcmresp->get_element( '0000', '0900' ); 	# Status
		my $no_remain = $dcmresp->get_element( '0000', '1020');	# No remaing sub-ops
		my $no_compl = $dcmresp->get_element( '0000', '1021');	# No complete sub-ops
		my $no_failed = $dcmresp->get_element( '0000', '1022');	# No failed sub-ops
		my $no_warn = $dcmresp->get_element( '0000', '1023');	# No warn sub-ops
		
		if (defined $progress) { 
			if (&$progress( $status, $no_remain, $no_compl, $no_failed, $no_warn, 
				$dcmresp, $dcmdata) == 0) { last; } 
		}	
	}
   } 

   return ($type==4); 
}

#----------------------------------------------------------------------

sub make_identifier { 
  my $self = shift; 

  my ( $studyuid, $seriesuid, $imageuid ); 
  
  my $i; 
  for ($i=0; $i < scalar @_; $i++) {
    if ($_[$i] eq "StudyUID") { $studyuid = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "SeriesUID") { $seriesuid = $_[$i+1]; $i++; } 
    elsif ($_[$i] eq "ImageUID") { $imageuid = $_[$i+1]; $i++; } 
  } 
  
  my $dcmq = new DICOM::DICOMObject;
  $dcmq->add_element( '0020', '000d', $studyuid); # Study UID
  if (defined $seriesuid) { $dcmq->add_element( '0020', '000e', $seriesuid); } # Series UID
  if (defined $imageuid) { $dcmq->add_element( '0008', '0018', $imageuid); } # Image UID

  if (defined $imageuid) { $dcmq->add_element( '0008', '0052', "IMAGE"); } # QueryRetrieveLevel
  elsif (defined $seriesuid) { $dcmq->add_element( '0008', '0052', "SERIES"); } # QueryRetrieveLevel
  else { $dcmq->add_element( '0008', '0052', "STUDY"); } 
  
  return $dcmq; 
}

#----------------------------------------------------------------------

sub set_max_size { 
 my $self = shift; 

 my $connect_maxsize = 0;
 my $userinfo = $self->{connect_assoc}->{userinfo}; 
 foreach (@{$userinfo->{contents}}) { 
	if ($_->{itemtype} != 81) { next; }
	$connect_maxsize = unpack("N", $_->{uid}); 
 }
 
 my $response_maxsize = 0;
 $userinfo = $self->{response_assoc}->{userinfo}; 
 foreach (@{$userinfo->{contents}}) { 
	if ($_->{itemtype} != 81) { next; }
	$response_maxsize = unpack("N", $_->{uid}); 
 }
 
 if ($connect_maxsize == 0) { $self->{maxsize} = $response_maxsize; } 
 elsif ($response_maxsize == 0) { $self->{maxsize} = $connect_maxsize; } 
 else { $self->{maxsize} = ($connect_maxsize < $response_maxsize) ? $connect_maxsize : $response_maxsize; } 


}

#----------------------------------------------------------------------

sub make_RQ_command { 
  my $self = shift; 
  my $command = shift; 
  my $uid = shift; 
  my $movedest = shift; 

  my $dcmc = new DICOM::DICOMObject;

  $dcmc->add_element( '0000', '0100', $command ); # Command field. 
  $dcmc->add_element( '0000', '0110', $self->{message} ); # Message ID
  $dcmc->add_element( '0000', '0700', 0 ); # Priority
  $dcmc->add_element( '0000', '0800', 258 ); # Data set type
  $dcmc->add_element( '0000', '0002', $uid ); # Affected SOP Class UID
  
  if (defined $movedest) { 
	$dcmc->add_element( '0000', '0600', pack ("A16", $movedest) ); # Move Destination
  }

  return $dcmc; 
}

#----------------------------------------------------------------------

sub send_rsp { 
   my $self = shift; 
   my $status = shift; 
   my $storepcid = shift; 
   my $dcmresp = shift; 
   my $dcmdata = shift; 
  
   my $num_remain = shift || 0;
   my $num_complete = shift || 0;
   my $num_failed = shift || 0;
   my $num_warn = shift || 0;

   my $dstype =  (defined $dcmdata) ? 258 : 257; 
   my $rsp_command; 

   my $rq_command =  $dcmresp->get_element('0000', '0100'); 
   if ($rq_command == 1) { 
	$rsp_command = hex('8001');	# CStoreRSP 
   } elsif ($rq_command == hex('10')) { 
	$rsp_command = hex('8010');	# CGetRSP 
   } elsif ($rq_command == hex('20')) { 
	$rsp_command = hex('8020');	# CFindRSP 
   } elsif ($rq_command == hex('21')) { 
	$rsp_command = hex('8021');	# CMoveRSP 
   } elsif ($rq_command == hex('30')) { 
	$rsp_command = hex('8030');	# CEchoRSP 
   } else { 
	return; 
   }

   my $dcms = new DICOM::DICOMObject;
   $dcms->add_element( '0000', '0002', $dcmresp->get_element('0000', '0002') ); # Affected SOP Class UID
   $dcms->add_element( '0000', '0100', $rsp_command ); # Command field.
   $dcms->add_element( '0000', '0120', $dcmresp->get_element('0000', '0110') ); # RSP Message ID
   $dcms->add_element( '0000', '0700', 0 ); # Priority
   $dcms->add_element( '0000', '0800', $dstype ); # Data set type
   $dcms->add_element( '0000', '0900', $status ); # Status 
   if ($rsp_command == hex('8001')) {
   	$dcms->add_element( '0000', '1000', $dcmresp->get_element('0000', '1000') ); # Affected SOP Class UID
   }
   if (($rsp_command == hex('8010')) || ($rsp_command == hex('8021'))) { 
	$dcms->add_element( '0000', '1020', $num_remain );
	$dcms->add_element( '0000', '1021', $num_complete );
	$dcms->add_element( '0000', '1022', $num_failed );
	$dcms->add_element( '0000', '1023', $num_warn );
   }
   $dcms->set_group_length( '0000' );

   $self->send_data( $self->{link}, $storepcid, $dcms, $dcmdata );
   
}

#---------------------------------------------------------------------

sub receive_data { 
  my $self = shift; 
  my $source = $self->{link}; 

  my $com_flag = 0; 
  my $data_flag = 0; 
  
  my $dcm_com; 
  my $dcm_obj; 

  my $dcm_com_data = ""; 
  my $dcm_obj_data = ""; 
  
  my $pcid; 
  my $type; 

  while (($com_flag == 0) || ($data_flag == 0)){ 
	my $data_frag = new DICOM::PDU( $source ); 
	if (! $data_frag) { 
		$DICOM::Transaction::errstr = $DICOM::PDU::errstr;  	
		return 0;	
	}
	$type = $data_frag->{pdutype}; 	
	if ($type == 7) { 
		$DICOM::Transaction::errstr = "Aborted!"; 
 	}
	if ($type != 4) { 
		last; 
		#die ("Corrupted! " . $data_frag->{pdutype}); 
	}
	$pcid = $data_frag->get_prescontext_id(); 
	while (my $pdv = shift (@{$data_frag->{pdata_values}})) { 
		if ($pdv->{messcontrol} & 1) { 
			$dcm_com_data .= $pdv->{data}; 
			if ($pdv->{messcontrol} & 2) { 
				$com_flag = 1; 
				$dcm_com = new DICOM::DICOMObject( $dcm_com_data ); 
				$dcm_com_data = undef; 
			}
		} else { 
			$dcm_obj_data .= $pdv->{data};
			if ($pdv->{messcontrol} & 2) { 
				$data_flag = 1; 
				$dcm_obj = new DICOM::DICOMObject( $dcm_obj_data ); 
				$dcm_obj_data = undef; 
			}
		}
	}
 	
	if (defined $dcm_com ) { 
		if ($dcm_com->get_element( '0000', '0800' ) == 257) { # Data set type = no data present
			$data_flag = 1;
		}
	}
  }
  
  if ($type == 5) { 
	my $assoc_release = new DICOM::ReleaseRP; 
	$assoc_release->write( $source );
  	$self->{connected} = 0; 
  } elsif ($type == 7) { 
  	#print "Aborting...\n"; 
  	$self->{connected} = 0; 
  }

  return ( $type, $dcm_com, $dcm_obj, $pcid ); 
}

sub send_data { 
  my $self = shift;
  my $dest = shift; 
  my $pcid = shift; 

  my $dcm_com = shift; 
  my $dcm_obj = shift; 
  
  my $maxpdu = shift || $self->{maxsize} || 0; 
  
  if ( defined $dcm_com) { 
	my $offset = 0; 
	my $flag = 0; 
	my $dcm_data = $dcm_com->get_binary(); 

  	while ( $flag == 0) { 
		my $pdv_data = ($maxpdu != 0) ? 
			substr $dcm_data, $offset, ($maxpdu - 6) : $dcm_data; 		
		my $messctrl = 1;	
		
		if ( ($maxpdu==0) || ( length($pdv_data) + $offset == length( $dcm_data ) ) ) { 
			$flag = 1; 
			$messctrl = 3; 
		} else { 
			$offset += length($pdv_data);	
		}
		my $data_frag = new DICOM::PDataTF( $pcid, $messctrl, $pdv_data ); 
		$data_frag->write( $dest ); 
	}
  }
  
  if ( defined $dcm_obj) { 
	my $offset = 0; 
	my $flag = 0; 
	my $dcm_data = $dcm_obj->get_binary(); 

  	while ( $flag == 0) { 
		my $pdv_data = ($maxpdu != 0) ? 
			substr $dcm_data, $offset, ($maxpdu - 6) : $dcm_data; 		
		my $messctrl = 0;	
		if ( ($maxpdu==0) || ( length($pdv_data) + $offset == length( $dcm_data ) ) ) { 
			$flag = 1; 
			$messctrl = 2; 
		} else { 
			$offset += length($pdv_data);	
		}
		my $data_frag = new DICOM::PDataTF( $pcid, $messctrl, $pdv_data ); 
		$data_frag->write( $dest ); 
	}
  }
}

1;

__END__

=head1 NAME

DICOM::Transaction - Perl extension for handling DICOM connections

=head1 SYNOPSIS

  use DICOM::Transaction
  

  my $link = new DICOM::Transaction ( 'LocalAE' => 'localDICOM', 
                     'RemoteAE' => 'remoteDICOM', 
                     'RemoteIP' => 'remoteDICOM', 
                     'Port' => 2100 ); 

  $link->add_prescontext( AbsSyntax => "1.2.840.10008.5.1.4.1.2.2.1" ); 	# Study Root Query FIND SOP 
  $link->add_prescontext( AbsSyntax => "1.2.840.10008.5.1.4.1.2.2.3" ); 	# Study Root Query GET SOP 
  $link->add_prescontext( AbsSyntax => "1.2.840.10008.5.1.4.1.1.2" ); 	# CT Image Storage SOP 
  if (! $link->connect() ) { die "Association rejected!"; }

  # C-Find operation
  my @results = $link->dofind( Level => STUDY, PatID => '' );
  
  # C-Get operation
  my @imagearray = $link->doget( StudyUID => '1.3.12.2.1107.5.1.4.28001.4.0.20274299511492455' );

  # C-Store operation
  $link->dostore( $dcmd );
  
  # C-Echo operation
  $link->doecho( );

  $link->close; 

  # Operating as a server
  my $server = new DICOM::Transaction ( Port => 2100 );
  
  # Wait for a valid AssociateRQ 
  while ($server->listen() ! = 1) { } 
  
  # Send an AssociateAC to accept the connection for given SOPs
  $server->make_assoc_response("1.2.840.10008.5.1.4.1.1.2" ); 	# CT Image Storage SOP
  # or
  # $server->reject(); 
 
  my ($type, $dcmresp, $dcmdata, $storepcid) = $server->receive_data();
  while ($type==4) { 
 	# Process the command and data 
    $server->send_rsp( 0, $storepcid, $dcmresp); 	# Send an appropriate response 
    ($type, $dcmresp, $dcmdata, $storepcid) = $server->receive_data();
  }

  

=head1 DESCRIPTION

The module DICOM::Transaction is a convenience module for handling DICOM traffic.
It handles all the low level DICOM communications involved in a C-FIND, C-GET, C_MOVE
and C-STORE commands. In itself it does not fully comply with any DICOM conformance
statement as Presentation Contexts need to be added to implement DICOM data exchange. 
DICOM transactions are only possible using a Transfer Syntax of 1.2.840.10008.1.2.

=head2 new

Configures a new DICOM link (but do not yet attempt to connect). An association request can
be defined at the same time. The input parameters are in the form of a hash table with valid keys: 

=over 14 

=item LocalAE

The local Application Entity Title 

=item RemoteAE

The Application Entity of the remote server

=item RemoteIP

The IP address of the remote server

=item Port

The port associated with this link

=item SSL

If this flag is non-zero, a DICOM SSL connection will be attempted (client mode only).
No authentication checks are made on the server.

=item UseCert

If this flag is non-zero, client certificates will be loaded for any SSL links

=item Key

The location of the private key (in PEM format) used if SSL is used with client certificates
enabled.

=item Cert

The location of the certificate (in PEM format) used if SSL is used with client certificates
enabled.

=back

The association request associated with these parameters is stored in $self->{connect_assoc}

=head2 add_prescontext

This routine adds a Presentation Context to the list of those which will be negotiated with
the remote dicom server. The input parameter is a hash table, which must contain a key
"AbsSyntax" which defines the type of operation to be negotiated. Optionally a key "ID" 
may be present - if so this will be used as an identifier for the presentation context within
the association request (if absent, one is calculated automatically). Note that there is no 
facility for setting the Transfer Syntax as only one is permissible with this module.

=head2 connect

Connect to the remote server defined within the Associate Request and wait for a reply. If 
the association has been accepted, a value of one is returned, otherwise zero. 
If successful, the socket is stored in the variable $self->{link}, and the reply association is
stored in $self->{response_assoc}.

=head2 close

Close the DICOM link with a ReleaseRQ. 

=head2 abort

Close the DICOM link with an Abort.

=head2 dofind

This performs a C-FIND on a transaction (which has previously been successfully connected to). 
The input parameters are in the form of a hash table, recognised keys for which are: 

=over 14

=item Level 

One of "STUDY", "SERIES" or "IMAGE"

=item PatName  

The Patient Name to be searched for

=item PatID 

The Patient ID to be searched for

=item Date 

The scan date

=item AccNum 

Accession Number

=item StudyUID 

The unique ID of the study

=item RefPhyNam 

Referring Physician Name

=item StudyDes 

Study Description

=item Mod 

The modality of the image series (SERIES level only)

=item SeriesUID 

The unique ID of the image series (IMAGE & SERIES levels only)

=item ImageUID 

The unique ID of the image (IMAGE level only)

=back

The data returned is an array of pointers to hashes containing the following keys which may
or may not have valid values associated with them:

=over 14

=item patname 

Patient Name

=item patid 

Patient ID

=item date 

Study Date

=item accnum 

Accession Number

=item refphynam 

Referring Physician Name

=item studydes 

Study Description

=item seriesdes 

Image Series Description

=item studyuid 

The unique ID of the study

=item seriesuid 

The unique ID of the image series

=item modality 

The modality of the image series

=item imageuid 

The unique ID of the image

=back

=head2 doget

This performs a C-GET on the remote dicom server. This is not supported by many DICOM implementations
(but is by the CTN software). The input parameter is a hash table. Valid keys are shown below:

=over 14

=item Callback 

A pointer to a user defined progress monitoring routine

=item StudyUID 

The study to be retrieved

=item SeriesUID 

The series to be retrieved

=item ImageUID 

The image to be retrieved

=back

Only one UID should be specified per call. If specified, the callback routine will return an array containing values for:

=over 14

=item 

Status. 
See DICOM standard for details. 0 = Success, FFxx = Pending plus many other SOP dependant values 

=item 

Number of remaining sub-operations

=item 

Number of completed sub-operations

=item 

Number of failed sub-operations

=item 

Number of warned sub-operations

=back

Returning a value of zero from the callback routine will abort the transaction.
On exit, doget will return an array of pointers to DICOM::DataObject objects which represent the successfully returned images.

=head2 domove

This behaves in exactly the same way as doget, except that an additional hash key for "Destination" is recognised as an input parameter. No value is returned on exit. 

=head2 dostore

This command takes a DICOM:DataObject as a parameter and attempts to store this on the remote server using C-STORE. The correct storage UID must have been negotiated previously. 

=head2 doecho

Perform a C-ECHO. A dicom "ping"

=head2 listen

This procedure will listen for connections on the port previously defined when the DICOM::Transaction object was created. On receiving data it will return with a value of 1 if the packet received is an AssociateRQ. The variables $self->{remoteip}, $self->{remoteae}, $self->{localae} are set to the values contained within the DICOM packet received. The received object is stored in $self->{connect_assoc} if further inspection is needed. 

=head2 make_assoc_response

This procedure automates the reply to an Associate-RQ. It takes an array of Abstract Syntaxes as a parameter, all of which will be accepted in a reply Associate-AC to the remote dicom server.

=head2 reject

This procedure sens an Associate-RJ in response to an Associate-RQ. It takes a hash with valid keys Reason, Source and Result as parameters (see DICOM standard 3.8 for meanings). 

=head2 receive_data

This low level command receives dicom command/data packets and turns them into dicom objects. The return value is an array containing the data: 

=over 14

=item Type

The PDU type: 4 for normal data traffic, 5 for a release request (the link is closed cleanly), 7 for an abort. 
1,2 & 3 should never happen if this routine is used appropriately.

=item DICOM Command Object

DICOM::DICOMObject containing the commands describing the transaction

=item DICOM Data Object

DICOM::DICOMObject containing the data sent in the transaction (may be null)

=item Presentation Context ID

The Presentation Context ID agreed in the Associate-RQ for this transaction

=back

=head2 send_rsp

This routine sends an appropriate response to a previously received DICOM command from a remote server. It takes 4 arguments: 

=over 14

=item Status

The success or otherwise of the initiating DICOM command

=item Presentation Context ID

The presentation context under which the command was received

=item DICOM Command Object

The original command object being responded to

=item DICOM Data Object

And DICOM data that needs to be returned (optional depending on context)

=back

=head2 send_data

This low level routine sends a dicom command/data object pair to the remote server. It takes up to 3 input arguments:

=over 14

=item Presentation Context ID

The Presentation Context ID agreed in the Associate-RQ for this transaction

=item DICOM Command Object

DICOM::DICOMObject containing the commands describing the transaction

=item DICOM Data Object

DICOM::DICOMObject containing the data sent in the transaction (may be null)

=item Max PDU size

The maximum size of packet that can be sent to the remote server (may be null for no limit)

=back

=head1 AUTHOR

Guy Williams, gbw1000@wbic.cam.ac.uk

=head1 SEE ALSO

DICOM::DICOMDataObject

=cut

