# DICOM::Abort.pm
#
# Copyright (c) 2007 Guy B. Williams <gbw1000@wbic.cam.ac.uk>. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package DICOM::Abort; 

use DICOM::Release; 
use strict; 
use vars qw( @ISA );

@ISA =  qw/DICOM::Release/; 

sub new {
 my $class = shift;
 
 my $elements = $class->SUPER::new( @_ );
 bless $elements, $class;
 
 $elements->{pdutype} = 7; 
 $elements->{reason} = 0; 

 return $elements;
}

1;
