# DICOM::PDUItem.pm
#
# Copyright (c) 2007 Guy B. Williams <gbw1000@wbic.cam.ac.uk>. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package DICOM::PDUItem; 
use strict; 

my $uid = ""; 
my $itemtype = 0; 

sub new { 
 my $class = shift;  
 my $pdu = {}; 
 
 $pdu->{itemtype} = 0; 

 bless $pdu, $class; 
 return $pdu; 
}

sub get_length { 
  my $self = shift;  
  return length($self->{uid}); 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 
  
  print $dest pack("c", $self->{itemtype});
  print $dest pack("c", 0);
  print $dest pack("n", $self->get_length()); 
  print $dest $self->{uid}; 

}

sub read { 
  my $self = shift; 
  my $source = shift; 

  # Have read the first byte (pdu type) already... 
  
  my $input;
  read $source, $input, 3; 
  my $length = unpack("xn", $input); 
  read $source, $uid, $length; 
  $self->{uid} = $uid; 
}

sub print_contents { 
  my $self = shift; 

  print " Type: " . $self->{itemtype} . "\t"; 
  if ((length $self->{uid}) == 4) { print unpack("N", $self->{uid}) . "\n"; } 
    else { print $self->{uid} . " (" . (length $self->{uid}) . ")\n"; } 
}

1;

