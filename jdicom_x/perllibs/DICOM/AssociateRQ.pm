# DICOM::AssociateRQ.pm
#
# Copyright (c) 2007 Guy B. Williams <gbw1000@wbic.cam.ac.uk>. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package DICOM::AssociateRQ; 

use DICOM::Associate; 
use strict;
use vars qw( @ISA );

@ISA =  qw/DICOM::Associate/; 

sub new {
 my $class = shift;
 my $elements = {};
 
 $elements = $class->SUPER::new( @_ );
 $elements->{pdutype} = 1; 
 bless $elements, $class;
 
 #$elements->{userinfo}->print_contents(); 

 return $elements;
}

1;
