# DICOM::DICOMObject.pm
#
# Copyright (c) 2007 Guy B. Williams <gbw1000@wbic.cam.ac.uk>. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package DICOM::DICOMObject; 

use DICOM::DICOMElement;
use DICOM::Dictionary;
use strict; 

sub new { 
  my $class = shift;
  my $rawdata = shift; 

  my $elements = {};
  bless $elements, $class;
  
  if (defined $rawdata) { 
	if ( -f $rawdata ) { 
		if ( ! $elements->load_file( $rawdata ) ) { 
			return 0;	
		}	
	} else { 
		if ( $elements->parse( $rawdata ) != length($rawdata) ) { 
			return 0; 
		}
  	}
  }

  return $elements; 
}

sub get_length { 
  my $self = shift; 
  my $isexplicit = shift || 0; 
  my $length = 0; 
  
  foreach my $gref (sort { hex($a) <=> hex($b) } keys (%{$self}) ) { 
    my %group = %{$self->{$gref}}; 
    foreach my $eref (sort { hex($a) <=> hex($b) } keys (%group) ) { 
      $length += $group{$eref}->get_length( $isexplicit ) + 8; 
      if ($isexplicit) { 
        my $vr = DICOM::Dictionary::get_type( $gref, $eref );
        if ( ($vr eq "OB") || ($vr eq "OW") || ($vr eq "OW/OB") || ($vr eq "OF") ||
	        ($vr eq "SQ") || ($vr eq "UT") || ($vr eq "UN") ) {$length += 4; }  
      }
    }
  }
  return $length; 
}

sub add_element { 
  my $self = shift; 
  my $group = shift; 
  my $element = shift; 
  my $contents = shift; 

  if ( ! defined $self->{$group} ) { 
    $self->{$group} = (); 
  }
  my $dcmelement = new DICOM::DICOMElement; 
  if (ref $contents eq "ARRAY") {
  	for (my $i=0; $i<scalar @{$contents}; $i++) { 
		$dcmelement->set_value( $group, $element, $contents->[$i], $i );
	}
  } else { 
  	$dcmelement->set_value( $group, $element, $contents ); 
  }
  $self->{$group}{$element} = $dcmelement; 

}

sub get_element { 
  my $self = shift; 
  my $group = shift; 
  my $element = shift; 
  my $vm = shift; 
  
  if (! defined $vm) { $vm = -1; }

  if ( ! defined $self->{$group} ) {
    return ""; 
  }
  if ( ! defined $self->{$group}{$element} ) { 
    return ""; 
  } 
  
  return $self->{$group}{$element}->get_data($vm); 
}

sub write { 
  my $self = shift; 
  my $dest = shift || "STDOUT"; 
  my $isexplicit = shift || 0; 

  foreach my $gref (sort { hex($a) <=> hex($b) } keys (%{$self}) ) { 
    # If group length exists, make sure it is correct 
    if (exists $self->{$gref}->{'0000'}) { 
      $self->set_group_length( $gref, $isexplicit ); 
    }
    my %group = %{$self->{$gref}}; 
    foreach my $eref (sort { hex($a) <=> hex($b) } keys (%group) ) { 
      $group{$eref}->write( $dest, $isexplicit ); 
    }
  }

}

sub write_mediafile { 
  my $self = shift; 
  my $dest = shift || "STDOUT"; 
  my $isexplicit = shift || 0; 

  print $dest pack("x128A4", "DICM"); 
  my $meta_info = new DICOM::DICOMObject; 
  $meta_info->add_element( "0002", "0001", pack("C2", 0, 1) );	# Version code 
  $meta_info->add_element( "0002", "0002", $self->get_element( "0008", "0016" ) ); # SOP Class UID
  $meta_info->add_element( "0002", "0003", $self->get_element( "0008", "0018" ) ); # SOP Instance UID
  $meta_info->add_element( "0002", "0010", 
  	( ($isexplicit) ? "1.2.840.10008.1.2.1" : "1.2.840.10008.1.2") ); # Transfer Syntax UID
  $meta_info->add_element( "0002", "0012", "1.0" ); # Implementation Class UID
  $meta_info->set_group_length( "0002", 1 );

  $meta_info->write( $dest, 1 ); 
  $self->write( $dest, $isexplicit );
}

sub get_binary { 
  my $self = shift; 
  
  my $data = ""; 
  foreach my $gref (sort { hex($a) <=> hex($b) } keys (%{$self}) ) { 
    # If group length exists, make sure it is correct for implicit VR 
    if (exists $self->{$gref}{'0000'}) { 
      $self->set_group_length( $gref ); 
    }
    my %group = %{$self->{$gref}}; 
    foreach my $eref (sort { hex($a) <=> hex($b) } keys (%group) ) { 
      $data .= $group{$eref}->get_binary( ); 
    }
  }
  return $data;  
}

sub load_file { 
  my $self = shift; 
  my $file = shift; 
  open (DCM, "<$file") or return 0; 
  my $tmp = $/;
  $/ = undef;
  my $data = <DCM>;
  $/ = $tmp;
  close DCM;
  if ( $self->parse( $data ) != length($data) ) { 
 	return 0; 
  } 
  return 1;
}
  
sub parse { 
  my $self = shift; 
  my $data = shift; 
  my $offset = shift || 0; 
  my $max = shift || length( $data ); 
  
  my $isexplicit = shift || 0; 

  my $done = 0;
  my $ismedia = 0;
  # Look to see if this is a media file if this is the first call 
  if ((length($data) > 132) && ($offset==0)) { 
  	my ($code) = unpack("x128A4", $data);
	if ($code eq "DICM") { 
		$ismedia = 1; 
		$isexplicit = 1;	
		$done = 132;	
	}
  }

  while (($offset + $done < $max) || ($max == -1) ) { 
    if (($ismedia) && ($done==$self->get_element('0002', '0000')+144)) { 
   	my ($tuid) = ($self->get_element('0002', '0010') =~ m/([\d\.]*)/);
	$ismedia = 0; 
	if ($tuid eq "1.2.840.10008.1.2") { 
		$isexplicit = 0;
	} elsif ($tuid eq "1.2.840.10008.1.2.1") {
		$isexplicit = 1; 
	} else { die ("Can't do transfer syntax $tuid\n"); }
    }

    my $dcmelement = new DICOM::DICOMElement;
    my $length = $dcmelement->parse( $data, $offset + $done, $isexplicit ); 
    $done += $length; # + 8; 

    my $group = $dcmelement->{group}; 
    my $elem = $dcmelement->{elem}; 
   
    if (($max == -1) && ($group == "fffe") && ($elem == "e00d") ) { last; }

    $self->{$group}{$elem} = $dcmelement; 
    
    #$offset += $dcmelement->get_length() + 8; 
    #$offset += $length + 8; 
    
  
  }
  return $done;
}

sub set_group_length { 
  my $self = shift; 
  my $group = shift; 
  my $isexplicit = shift || 0; 
  my $length = 0; 

  my %group = %{$self->{$group}}; 
  foreach my $eref (keys (%group)) { 
    if ($eref eq '0000') {next;}
    $length += $self->{$group}{$eref}->get_length( $isexplicit ) + 8; 
    if ($isexplicit) { 
      my $vr = DICOM::Dictionary::get_type( $group, $eref );
      if ( ($vr eq "OB") || ($vr eq "OW") || ($vr eq "OW/OB") || ($vr eq "OF") ||
	        ($vr eq "SQ") || ($vr eq "UT") || ($vr eq "UN") ) {$length += 4; }  
    }
  }
  $self->add_element( $group, '0000', $length ); 
}

sub print_contents { 
  my $self = shift; 
  my $indent = shift || 0;

  foreach my $gref (sort { hex($a) <=> hex($b) } keys (%{$self}) ) { 
    my %group = %{$self->{$gref}}; 
    foreach my $eref (sort { hex($a) <=> hex($b) } keys (%group) ) { 
      my $elem = $group{$eref}; 
      $elem->print_contents($indent); 
#      print "$gref $eref "; 
#      print pack("A35", DICOM::Dictionary::get_name( $gref, $eref )); 
#      if (DICOM::Dictionary::is_unprintable( $gref, $eref )) { 
#	print "<Data>\n"; 
#      } else { 
#	print $elem->{data}, "\n"; 
#	print "\n"; 
#      } 
    }
  }
}

1;

__END__

=head1 NAME

DICOM::DICOMObject - Perl extension for handling DICOM objects

=head1 SYNOPSIS

  use DICOM::DICOMObject; 
  
  # Loading up a DICOM Object
  my $image = new DICOM::DICOMObject( "filename.dcm" );  
  
  # Printing out a DICOM Object
  $image->print_contents(); 

  # Saving a DICOM Object
  open (DEST, ">outfile.dcm") or die "Can't open"; 
  $image->write(*DEST);
  close(DEST); 

=head1 DESCRIPTION 

This module contains the DICOM Object. For more info see the source code.

=head2 get_length

Returns the length in bytes of the DICOM object

=back

=head2 add_element

Adds the specified element to the DICOM Object, replacing any existing element
with the same group and element tags. It takes three arguments:

=over 14

=item Group ID

The group id tag as a 4 digit hexadecimal string (e.g. '0018').

=item Element ID

The element id tag as a 4 digit hexadecimal string (e.g. '0020').

=item Contents

The value of the tag which should be set. For a tag with a multiplicity of more
than one this should be a pointer to an array containing the data.

=back

=head2 get_element

Retrieves the contents of elements within the DICOM Object. It takes two mandatory
arguments and a third optional argument.

=over 14

=item Group ID

The group id tag as a 4 digit hexadecimal string (e.g. '0018').

=item Element ID

The element id tag as a 4 digit hexadecimal string (e.g. '0020').

=item Element number

For a tag with a multiplicity greater than one, this is used to specify the index into
the array of the value to be returned. If absent, the tag values are returned as an 
array.

=back

=head2 write 

Write the object to the data stream given as the first argument, or STDOUT if none is
specified.

=back

=head2 get_binary

Return a binary representation of this data object (little endian, implicit VR).

=back

=head2 load_file

Attempt to load the filename specified as an argument as a dicom object. 
Return 0 on failure to parse it.

=back

=head2 parse

Parse the specified binary data specified as the first argumentinto a DICOM Object. 
Used by the constructor if filename loading does not work.

=back

=head2 set_group_length

Set the group lengths (xxxx,0000) tags correctly for the group tag specified as the
1st argument.

=back

=head2 print_contents

Print a textual representation of this DICOM object to the standard output.

=back

=head1 AUTHOR

Guy Williams, gbw1000@wbic.cam.ac.uk

=cut

  
