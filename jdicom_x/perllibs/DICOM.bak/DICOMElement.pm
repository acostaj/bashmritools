
package DICOM::DICOMElement; 

use DICOM::Dictionary; 
use strict; 

sub new { 
  my $class = shift;
  my $elements = {};
  bless $elements, $class;

  return $elements; 
}

sub getlength { 
  my $self = shift; 
  my $length = 0; 
  
  if (! defined $self->{type}) { 
    return length($self->{data}); 
  } elsif ( $self->{type} == 1 ) { # String 
    $length = length($self->{data}); 
  } elsif ( $self->{type} == 2) { # Short int 
    $length = 2; 
  } elsif ( $self->{type} == 3) { # Long int 
    $length = 4; 
  } elsif ( $self->{type} == 4) { # Sequence 
    foreach (@{$self->{data}}) { 
	$length += $_->getlength() + 8; 
    } 
  }
  
  return $length; 
}

sub setValue { 
  my $self = shift; 
  my $group = shift; 
  my $element = shift; 
  my $contents = shift; 
  
  my $type; 
  if ( DICOM::Dictionary::is_short( $group, $element ) ) {
    $type = 2; 
  } elsif ( DICOM::Dictionary::is_int( $group, $element ) ) { 
    $type = 3;
  } else { 
    $type = 1; 
  } 
  
  if (($type == 1) && ((length($contents) & 1) == 1)) { 
    if (DICOM::Dictionary::is_string( $group, $element ) ) { 
      $contents .= " "; 
    } else { 
      $contents .= "\0"; 
    }
  }
  
  $self->{group} = $group; 
  $self->{elem} = $element; 
  $self->{type} = $type; 
  $self->{data} = $contents; 

}

sub getbinary { 
  my $self = shift; 
  
  my $group_num = hex( $self->{group} );
  my $elem_num = hex( $self->{elem} );
  my $tags = pack("v", $group_num) . pack("v", $elem_num) . pack("V", $self->getlength() ); 
  
  my $data = $tags; 
  my $type = $self->{type} || 1; 
  my $contents = $self->{data}; 

  if ($type == 1) { 
    if (defined $contents) { $data .= $contents } ; 
  } elsif ($type == 2) {
    $data .= pack("v", $contents);
  } elsif ($type == 3) {
    $data .= pack("V", $contents);
  } elsif ($type == 4) {
    foreach (@{$self->{data}}) {
	$data .= pack("v", hex('FFFE') );
	$data .= pack("v", hex('E000') );
	$data .= pack("V", $_->getlength() ); 
 	$data .= $_->getbinary();
    }
  } else { 
    $data .= $contents;
  } 

  return $data; 
}

sub write { 
  my $self = shift; 
  my $dest = shift; 
  
  my $group_num = hex( $self->{group} ); 
  my $elem_num = hex( $self->{elem} ); 

  print $dest pack("v", $group_num); 
  print $dest pack("v", $elem_num); 
  print $dest pack("V", $self->getlength() ); 

  if ($self->{type} == 1) { 
    print $dest $self->{data}; 
  } elsif ($self->{type} == 2) { 
    print $dest pack("v", $self->{data}); 
  } elsif ($self->{type} == 3) { 
    print $dest pack("V", $self->{data}); 
  } elsif ($self->{type} == 4) { 
    foreach (@{$self->{data}}) { 
	print $dest pack("v", hex('FFFE') ); 
	print $dest pack("v", hex('E000') ); 
	print $dest pack("V", $_->getlength());
 	$_->write( $dest ); 
    }
  } else {
    print $dest $self->{data};
  }

}

sub parse { 
  my $self = shift; 
  my $data = shift; 
  my $offset = shift; 
  
  my $tags = substr( $data, $offset, 8); 
  my ($group, $elem, $length) = unpack("vvV", $tags); 
  
  $self->{group} = sprintf("%04x", $group); 
  $self->{elem} = sprintf("%04x", $elem); 

  if ( DICOM::Dictionary::is_short( $self->{group},  $self->{elem} ) ) {
    $self->{data} = unpack("v", substr( $data, ($offset+8), $length) ); 
    $self->{type} = 2; 
  } elsif ( DICOM::Dictionary::is_int( $self->{group},  $self->{elem} ) ) {
    $self->{data} = unpack("V", substr( $data, ($offset+8), $length) );
    $self->{type} = 3; 
  } elsif ( DICOM::Dictionary::is_sq( $self->{group},  $self->{elem} ) ) {
    $self->{type} = 4; 
    $self->{data} = []; 
    my $sq_offset = 8; 
    while (($length > 0) || ($length ==  hex('FFFFFFFF'))) { 
	my $seq_item = substr($data, $offset+$sq_offset, 8); 
	my ($gp, $el, $sq_len) = unpack("vvV", $seq_item);
	$sq_offset += 8; 
	if (($length ==  hex('FFFFFFFF')) && ($gp == hex('FFFE')) && ($el == hex('E0DD')) ) { last; }
	my $sq_data = new DICOM::DICOMObject;
	if ($sq_len != hex('FFFFFFFF')) { 
		$sq_data->parse($data, $offset+$sq_offset, $offset+$sq_offset+$sq_len); 
	} else {
		$sq_len = $sq_data->parse($data, $offset+$sq_offset, -1);
	}
	push @{$self->{data}}, $sq_data; 
   	if ($length !=  hex('FFFFFFFF')) { $length -= ($sq_len+8); } 
	$sq_offset += $sq_len;
    }
    $length = $sq_offset-8; 

  } else { 
    $self->{data} = substr( $data, ($offset+8), $length); 
    $self->{type} = 1; 
  }

  return $length; 

}

sub printcontents {
  my $self = shift;
  my $indent = shift | 0; 

  my $gref = $self->{group}; 
  my $eref = $self->{elem}; 
  
  print pack("A$indent"); 
  print "$gref $eref ";
  print pack("A35", DICOM::Dictionary::getname( $gref, $eref ));
  
  if ($self->{type} == 4) { 
	print "Sequence\n"; 
	for (my $i=0; $i<(scalar @{$self->{data}}); $i++) { 
		print pack("A$indent");
		print " Item: $i\n"; 
		$self->{data}->[$i]->printcontents($indent + 1); 
		print "\n";	
	}
  } elsif (DICOM::Dictionary::is_unprintable( $gref, $eref )) {
	print "<Data>\n"; 
  } else { 
	print $self->{data}, "\n"; 
  }

}

1;
