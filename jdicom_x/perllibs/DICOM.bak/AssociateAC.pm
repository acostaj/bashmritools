
package DICOM::AssociateAC; 

use DICOM::Associate; 
use strict;
use vars qw( @ISA );

@ISA =  qw/DICOM::Associate/;

sub new {
 my $class = shift;

 my $elements = $class->SUPER::new( @_ ); 
 $elements->{pdutype} = 2; 
 bless $elements, $class;

 return $elements;
}

1;
