#!/bin/bash
# BASHMRITOOLS INSTALLATION FILE

echo Initialising bashmritools installation...


echo Register BMT root directory
chmod +x ${PWD}/jutils_x/j*
BMTLDIR=${HOME}/.bmt
./jutils_x/jmkdir ${BMTLDIR}
rm -f ${BMTLDIR}/dir
echo ${PWD} > ${BMTLDIR}/dir


echo Set BMT environment
if [ -f ${BMTLDIR}/profile ]; then
	echo ${BMTLDIR}/profile found - installer will not override this file.
else
	cp ${PWD}/src/profile ${BMTLDIR}/profile
fi
if [ -f ${BMTLDIR}/gsettings ]; then
        echo ${BMTLDIR}/gsettings found - installer will not override this file.
else
	cp ${PWD}/src/gsettings ${BMTLDIR}/gsettings
fi
source ${BMTLDIR}/profile


echo Autoset BMT environment on login/terminal init
if [ -f ${BASHPROF} ]; then
	FLAG1=`cat ${BASHPROF} | grep ${BMTLDIR}/profile`
else
	FLAG1=
fi
if [ -z "$FLAG1" ]; then
        echo Update ${BASHPROF}
	echo " " >> ${BASHPROF}
        echo "# BASHMRITOOLS" >> ${BASHPROF}
        echo "source ${BMTLDIR}/profile" >> ${BASHPROF}
else
        echo bashmritools entry exists - do nothing.
fi


echo Register BMT MATLAB directory
rm -f ${BMTLDIR}/matdir
echo ${BMTMAT}/ > ${BMTLDIR}/matdir


echo Autoset BMT environment on MATLAB startup
LMATDIR=${HOME}/matlab
if [ ! -d ${LMATDIR} ]; then ./jutils_x/jmkdir ${LMATDIR}; fi
if [ -f ${LMATDIR}/startup.m ]; then
	FLAG2=`cat ${LMATDIR}/startup.m | grep ${BMTMAT}/startup.m`
else
	FLAG2=
fi
if [ -z "$FLAG2" ]; then	
	echo Update MATLAB startup
        echo " " >> ${LMATDIR}/startup.m
	echo "%% BASHMRITOOLS" >> ${LMATDIR}/startup.m
	echo "run ${BMTMAT}/startup.m" >> ${LMATDIR}/startup.m
else
	echo bashmritools entry exists - do nothing.
fi


echo Enable SPM in MATLAB with jspmpath
./jutils_x/jmkdir ${BMTLDIR}/matlab
rm -f ${BMTLDIR}/matlab/jspmpath.m
echo "addpath ${BMTSPM}" > ${BMTLDIR}/matlab/jspmpath.m
if [ -z "$BMTSPM" ]; then
	echo WARNING: No SPM path selected in gsettings. Some features will not be available.
fi

echo ... done.

###############################################################################
echo
echo "NOTE: A successful installation should run without errors."
echo
echo "If errors were shown above, try editing:"
echo "+ ${PWD}/INSTALL.sh (this file), or"
echo "+ ${PWD}/profile"
echo "and run ./INSTALL.sh again."
echo
echo "If the installation file ran without errors, open a new terminal window" 
echo "(or run 'source ~/.bmt/profile'), and type 'echo \$BMTDIR' to verify you" 
echo "are all set up."
echo
echo "Created by Julio Acosta-Cabronero"
###############################################################################
