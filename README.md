BASHMRITOOLS v1.0.0
===================

Bash/MATLAB tools for MRI data manipulation

RECOMMENDED SOFTWARE
====================

General
-------

+ UNIX-based operating system, e.g. Linux/OSX
+ BASH as default shell interpreter
+ MATLAB
+ Python 3
+ Perl
+ pigz

Neuroimaging
------------

+ SPM 		http://www.fil.ion.ucl.ac.uk/spm/software
+ FSL 		https://fsl.fmrib.ox.ac.uk/fsl/fslwiki
+ FreeSurfer 	http://freesurfer.net
+ ANTs		http://stnava.github.io/ANTs

INSTALL
=======

+ Edit src/gsettings
+ Run $ ./INSTALL.sh
  NOTE: Succesful installation should return no errors

USAGE
=====

+ Directory names in pwd appending _x will be autoadded
to your PATH
+ All directories in the 'matlab' dir will be autoadded
to your MATLAB path

Created by Julio Acosta-Cabronero
