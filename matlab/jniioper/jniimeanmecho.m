function jniimeanmecho(mecho_froot,TE_fname)

[magn,st]=jniiload(mecho_froot);
TE=load(TE_fname);

[mS] = jmeanmecho(magn,TE);

jniisave(mS,st,['mean_' mecho_froot])
