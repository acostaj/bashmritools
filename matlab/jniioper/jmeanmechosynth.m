function [mS,S0,R2star] = jmeanmechosynth(magn,TE,TE_synth)

magn = log(magn);

sz = size(magn);
magn = reshape(magn,[sz(1)*sz(2)*sz(3) sz(4)]);

mTE = TE_synth;
O   = ones([length(TE) 1]);
TE  = [O TE'];

P = TE\magn';
clear magn

S0 = exp(P(1,:)');
R2star = -P(2,:)';
clear P

mS = S0.*exp(-mTE.*R2star);

S0 = reshape(S0,[sz(1) sz(2) sz(3)]);
R2star = reshape(R2star,[sz(1) sz(2) sz(3)]);
mS = reshape(mS,[sz(1) sz(2) sz(3)]);
