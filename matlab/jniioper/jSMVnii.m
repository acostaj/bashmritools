function jSMVnii(froot,res,radius)
% SMV (low and high pass) filtering
%
% jSMVnii(froot,res,radius)
% 
% froot     Fileroot
% res       Voxel size in mm
% radius    Spherical kernel radius in mm
% 
% Created by Julio Acosta-Cabronero

[ima,st] = jniiload(froot);

[ima_SMV,ima_edge] = jSMV(ima,res,radius);

jniisave(ima_SMV,st,['SMV' num2str(radius) '_lowpass_' froot])

jniisave(ima_edge,st,['SMV' num2str(radius) '_highpass_' froot])