function jniimeanmechosynth(mecho_froot,TE_fname,TE_synth)

[magn,st]=jniiload(mecho_froot);
TE=load(TE_fname);

[mS] = jmeanmechosynth(magn,TE,TE_synth);

jniisave(mS,st,['synthTE' num2str(TE_synth) '_' mecho_froot])
