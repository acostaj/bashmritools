function jsegmentmaxprob(baseimage_froot,tissue_class)
% Calculate tissue mask using the max probability criterion
% 
% N.b. This assumes a base (structural) image has been presegmented with SPM
% 
% SINTAX
%  jsegmentmaxprob(baseimage_froot,tissue_class)
% 
% INPUT
% 
%  baseimage_froot      Base image fileroot exc. filename extension
%                        i.e. This is the image that served as input to SPM.
% 
%  tissue_class         Tissue type
%                        [1] GM
%                        [2] WM
%                        [3] CSF
% 
% Created by Julio Acosta-Cabronero

[c1,st] = jniiload(['c1' baseimage_froot]);
ca(:,:,:,1) = c1;
clear c1
[c2]    = jniiload(['c2' baseimage_froot]);
ca(:,:,:,2) = c2;
clear c2
[c3]    = jniiload(['c3' baseimage_froot]);
ca(:,:,:,3) = c3;
clear c3

%[c4]    = jniiload(['c4' baseimage_froot]);
%ca(:,:,:,4) = c4;
%clear c4
%[c5]    = jniiload(['c5' baseimage_froot]);
%ca(:,:,:,5) = c5;
%clear c5
%[c6]    = jniiload(['c6' baseimage_froot]);
%ca(:,:,:,6) = c6;
%clear c6

%%
cmax = max(ca,[],4);

nca = ca(:,:,:,tissue_class)./cmax;

bc = uint8(zeros(size(nca)));

bc(nca==1) = 1;

%%
jniisave(bc,st,['maxc' num2str(tissue_class) baseimage_froot])
