function [ filt_ima ] = jfiltmedian3D( ima_froot, filt_width )
% DESCRIPTION
%  3D median filtering
%
% SINTAX
%  [ filt_ima ] = jfiltmedian3D( image_fileroot, filter_width )
% 
% Created by Julio Acosta-Cabronero

[ima, st] = jniiload( ima_froot );

filt_ima = medfilt3( ima, [filt_width filt_width filt_width] );

jniisave( filt_ima, st, ['filtmedian_3Dw' num2str(filt_width) '_' ima_froot] )

end
