function jrsos(magn_froot,phase_froot,scaling_factor)
% Calculate root sum of squares
[im,st]=jniiload(magn_froot);
im2 = sqrt(sum(im.^2,4));
jniisave(im2,st,['rsos_' magn_froot])
clear im2

if nargin>=2
    if nargin==2
        scaling_factor=pi;
    end
    
    [ip,st]=jniiload(phase_froot);
    
    ip2 = sum((im.^1).*exp(1i*ip*(pi/scaling_factor)),4);
    ip3 = angle(ip2);
    clear im ip ip2

    % NIFTI structure: int => float
    st(1).dt(1)=16;    
    jniisave(ip3,st,['rsos_' phase_froot])
end

end
