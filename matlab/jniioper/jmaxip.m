function jmaxip( image_fileroot, s )
%
% Maximum intensity projection
%
% jmaxip( image_fileroot, no_slices )
%
% Created by Julio Acosta-Cabronero (9/12/2015)

[ima st] = jniiload(image_fileroot);

S=size(ima,3)
t=S-s+1
for x=1:t
    P(:,:,x)=max(ima(:,:,x:x+s-1),[],3);
end

Spad=S-t
Spadup=round(Spad/2)

mIP=zeros(size(ima));
mIP(:,:,Spadup+1:Spadup+t)=P;

jniisave( mIP, st, ['maxIP' num2str(s) '_' image_fileroot] );
