function jflipap( fname )
% Flips image anterior/posterior

froot = jfnamesplit( fname, 'gz' );
[ima, st]=jniiload( froot );

for x=1:size(ima,3)
    ima_rot(:,:,x)=rot90(ima(:,:,x))'; 
end
jniisave(ima_rot,st,['flipAP_' froot])
