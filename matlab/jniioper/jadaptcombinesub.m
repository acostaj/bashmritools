function [ recon ] = jadaptcombinesub( im, bs, st, channel_idx )

[nc,ny,nx]=size(im);
maxcoil=channel_idx;    % maxcoil

rn=eye(nc);   % noise correlation matrix
wsmall=zeros(nc,round(ny./st),nx./st);
cmapsmall=zeros(nc,round(ny./st),nx./st);
inv_rn=inv(rn);

for x=st:st:nx
    for y=st:st:ny
        ymin1=max([y-bs./2 1]);
        xmin1=max([x-bs./2 1]);

        ymax1=min([y+bs./2 ny]);
        xmax1=min([x+bs./2 nx]);

        ly1=length(ymin1:ymax1);
        lx1=length(xmin1:xmax1);

        m1=reshape(im(:,ymin1:ymax1,xmin1:xmax1),nc,lx1*ly1);

        m=m1*m1';

        [e,v]=eig(inv_rn*m);
        v=diag(v);
        [ttt,ind]=max(v);
        mf=e(:,ind);

        mf=mf/(mf'*inv_rn*mf);
        normmf=e(:,ind);

        mf=mf.*exp(-1i*angle(mf(maxcoil)));
        normmf=normmf.*exp(-1i*angle(normmf(maxcoil)));

        wsmall(:,y./st,x./st)=mf;
        cmapsmall(:,y./st,x./st)=normmf;
    end
end

recon=zeros(ny,nx);

wfull=zeros([nc ny nx]);
cmap=zeros([nc ny nx]);

for i=1:nc
    wfull(i,:,:)=conj(imresize(squeeze(wsmall(i,:,:)),[ny nx],'bilinear'));
    cmap(i,:,:)=imresize(squeeze(cmapsmall(i,:,:)),[ny nx],'bilinear');
end

wfull=wfull(:,1:ny,1:nx);
cmap=cmap(:,1:ny,1:nx);

for i=1:nc
    recon=recon+reshape(squeeze(wfull(i,:,:).*im(i,:,:)),[ny nx]);
end

end % nested function