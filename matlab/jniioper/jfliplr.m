function jfliplr( fname )
% Flips image left/right

froot = jfnamesplit( fname, 'gz' );
[ima, st]=jniiload( froot );

for x=1:size(ima,1)
    ima_rot(x,:,:)=ima(end-x+1,:,:); 
end
jniisave(ima_rot,st,['flipLR_' froot])
