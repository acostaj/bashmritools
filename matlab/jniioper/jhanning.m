function [uout] = jhanning( magn_froot, phase_froot, window_width, save_LP, CT1, CT2, display, slice_offset, data_path, unwrap )
%
% Filter phase image applying Hanning window in k-space
%
% [uout] = jhanning( magn_froot, phase_froot, window_width, save_LP, CT1, CT2, display, slice_offset, data_path, unwrap )
%
% ARGUMENTS:
%
%  IN
%
%     + magnname: magnitude image name (prior brain extraction recommended)
%     + phasename: phase image name
%     + window_width: vector with length = 1 if isotropic 2D window is 
%                     required; length = 2 for iso- and anisotropic 2D 
%                     windows; and length = 3 for 3D filtering
%    [OPTIONAL}
%     + save_LP: 0 / 1 (save LP-filtered phase image, 0 = 'no', default /
%                       1 = 'yes')
%     + CT1: defines colorscale display range [-CT1 CT1] for phase image &
%            low pass-filtered image. Default: 'minmax'
%     + CT2: colorscale range for high-pass filtered image (default: CT2 =
%            CT1)
%     + display: 0 / 1 (display slices, 0 = 'off', default / 1 = 'on)
%     + slice_offset: instead of showing central slice, show for example
%                     two slices below ==> slice_offset = -2. Default: 0
%     + data_path: path to input data (magn & phase). If argument not 
%                  present it will assume datasets are in current directory
%     + unwrap = 1: if further unwrapping is required after high-pass 
%                   filtering; omit otherwise
% 
%  OUT
%
%     + uout: HP-filtered (output) image name 
%
% DEPENDENCE: Slices_18axial, prelude (if unwrap = 1)
%

%% Start timing
    
tic

%% Set defaults if argument not included

min_NARGIN = 3;
c = 0;

c = c+1;
if nargin < min_NARGIN+c
    save_LP = 0;
end

c = c+1;
if nargin < min_NARGIN+c
    CT1 = 'minmax';
end

c = c+1;
if nargin < min_NARGIN+c
    CT2 = CT1;
end

c = c+1;
if nargin < min_NARGIN+c
    display = 1;
end

c = c+1;
if nargin < min_NARGIN+c
    slice_offset = 0;
end

c = c+1;
if nargin < min_NARGIN+c
    data_path = './';
end

c = c+1;
if nargin < min_NARGIN+c
    unwrap = 0;
end

%% Define filter dimensionality

if length(window_width) == 1
    filter_dim='2D';
    window_width(2) = window_width(1);
elseif length(window_width) == 2
    filter_dim='2D';
elseif length(window_width) == 3
    filter_dim='3D';
else
    disp('Error: wrong hanning window width')
    
end

%% Load images

[magn, magn_struct] = jniiload( magn_froot );
[phase, phase_struct] = jniiload( phase_froot );

magn = single( magn );

% matrix dimensions
N = size( magn );

%% Compute brain mask

M = zeros( N );
M( abs(magn)>0 ) = 1;

%% Zero-pad to double FOV
pad_magn = padarray( magn, round(N/2) );
pad_phase = padarray( phase, round(N/2) );

% save padded images as NIFTI
% res = [1 1 2];
% datatype = 16;
% jac_nii_save_new_struct( pad_magn, res, datatype, ['magn_pad.nii'] );
% jac_nii_save_new_struct( pad_phase, res, datatype, ['phase_pad.nii'] );
% return

clear magn phase

% padded matrix dimensions
N2 = size( pad_magn );

%% Re-adjust Hanning window width if larger than padded FOV

if window_width(1) > N2(1)
    window_width(1) = N2(1);
end
if window_width(2) > N2(2)
    window_width(2) = N2(2);
end
if filter_dim == '3D' & window_width(3) > N2(3)
    window_width(3) = N2(3);
end

%% Complex MR signal

S = pad_magn.*( cos( pad_phase )+1i*sin( pad_phase ) );

clear pad_magn pad_phase

%% Transform signal to frequency domain

pad_kspace = jfft3c( S );

% save padded k-space as NIFTI
% res = [1 1 2];
% datatype = 16;
% jac_nii_save_new_struct( real(pad_kspace), res, datatype, ['kspace_real_pad.nii'] );
% jac_nii_save_new_struct( imag(pad_kspace), res, datatype, ['kspace_imag_pad.nii'] );
% return

%% Display padded k-space (top row)

% number of rows for subplot & initialise subplot counter
spr = 5;
c2 = 0;

CTkl = 0;
CTku = 750;

% display
figure

c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( :, :, N(3) ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
if filter_dim == '3D'
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( :, N(2), : ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1), :, : ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
else
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( :, N(2), : ) ) )' ); colormap('gray'); axis image; axis off; caxis([CTkl CTku])
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1), :, : ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
end

% min(abs(pad_kspace(:)));
% max(abs(pad_kspace(:)));

%% Display k-space (native matrix size)

% c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1)-(N(1)/2)+1 : N(1)+N(1)/2, N(2)-(N(2)/2)+1 : N(2)+N(2)/2, N(3) ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
% if filter_dim == '3D'
%     c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1)-(N(1)/2)+1 : N(1)+N(1)/2, N(2), N(3)-(N(3)/2)+1 : N(3)+N(3)/2 ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
%     c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1), N(2)-(N(2)/2)+1 : N(2)+N(2)/2, N(3)-(N(3)/2)+1 : N(3)+N(3)/2 ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
% else
%     c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1)-(N(1)/2)+1 : N(1)+N(1)/2, N(2), : ) ) )' ); colormap('gray'); axis image; axis off; caxis([CTkl CTku])
%     c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1), N(2)-(N(2)/2)+1 : N(2)+N(2)/2, : ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
% end

%% Display k-space within filter window

c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1)-(window_width(1)/2)+1 : N(1)+window_width(1)/2, N(2)-(window_width(2)/2)+1 : N(2)+window_width(2)/2, N(3) ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
if filter_dim == '3D'
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1)-(window_width(1)/2)+1 : N(1)+window_width(1)/2, N(2), N(3)-(window_width(3)/2)+1 : N(3)+window_width(3)/2 ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1), N(2)-(window_width(2)/2)+1 : N(2)+window_width(2)/2, N(3)-(window_width(3)/2)+1 : N(3)+window_width(3)/2 ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
else
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1)-(window_width(1)/2)+1 : N(1)+window_width(1)/2, N(2), : ) ) )' ); colormap('gray'); axis image; axis off; caxis([CTkl CTku])
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad_kspace( N(1), N(2)-(window_width(2)/2)+1 : N(2)+window_width(2)/2, : ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
end

%% Compute Hanning window in 2D

hannw_xy = hanning( window_width(1) ) * hanning( window_width(2) )';
pad2D_hannw_xy = padarray( hannw_xy, ( 2*N(1:2) - [ window_width(1) window_width(2) ] ) / 2 );
pad3D_hannw = repmat( pad2D_hannw_xy, [ 1 1 N2(3) ] );

clear *hannw_xy

%% Extend Hanning window to 3D

if filter_dim == '3D'
    hannw_z(1,1,:) = hanning( window_width(3) );
    pad1D_hannw_z = padarray( hannw_z, [ 0 0 round((N2(3) - window_width(3)) / 2) ] );
    pad3D_hannw_z = repmat( pad1D_hannw_z,[ 2*N(1:2) 1 ] );
    pad3D_hannw = pad3D_hannw .* pad3D_hannw_z;
    
    clear *hannw_z
end

% save padded hanning filter as NIFTI
% res = [1 1 2];
% datatype = 16;
% jac_nii_save_new_struct( pad3D_hannw, res, datatype, ['hannw_pad.nii'] );
% return

%% Compute low-pass filtered k-space

LP_pad_kspace = pad_kspace .* pad3D_hannw;

% pad_kspace(end/2,end/2,end/2)
% LP_pad_kspace(end/2,end/2,end/2)
% return

% save padded high-pass filtered k-space as NIFTI
% res = [1 1 2];
% datatype = 16;
% jac_nii_save_new_struct( real(LP_pad_kspace), res, datatype, ['LP_kspace_real_pad.nii'] );
% jac_nii_save_new_struct( imag(LP_pad_kspace), res, datatype, ['LP_kspace_imag_pad.nii'] );
% return

clear pad_kspace

%% Display Hanning filter

c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad3D_hannw( N(1)-(window_width(1)/2)+1 : N(1)+window_width(1)/2, N(2)-(window_width(2)/2)+1 : N(2)+window_width(2)/2, N(3) ) ) )' ); colormap('gray'); axis equal; axis off
if filter_dim == '3D'
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad3D_hannw( N(1)-(window_width(1)/2)+1 : N(1)+window_width(1)/2, N(2), N(3)-(window_width(3)/2)+1 : N(3)+window_width(3)/2 ) ) )' ); colormap('gray'); axis equal; axis off
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad3D_hannw( N(1), N(2)-(window_width(2)/2)+1 : N(2)+window_width(2)/2, N(3)-(window_width(3)/2)+1 : N(3)+window_width(3)/2 ) ) )' ); colormap('gray'); axis equal; axis off
else
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad3D_hannw( N(1)-(window_width(1)/2)+1 : N(1)+window_width(1)/2, N(2), : ) ) )' ); colormap('gray'); axis image; axis off
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( pad3D_hannw( N(1), N(2)-(window_width(2)/2)+1 : N(2)+window_width(2)/2, : ) ) )' ); colormap('gray'); axis equal; axis off
end

clear pad3D_hannw

%% Display low-pass filtered k-space within filter window

c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( LP_pad_kspace( N(1)-(window_width(1)/2)+1 : N(1)+window_width(1)/2, N(2)-(window_width(2)/2)+1 : N(2)+window_width(2)/2, N(3) ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
if filter_dim == '3D'
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( LP_pad_kspace( N(1)-(window_width(1)/2)+1 : N(1)+window_width(1)/2, N(2), N(3)-(window_width(3)/2)+1 : N(3)+window_width(3)/2 ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( LP_pad_kspace( N(1), N(2)-(window_width(2)/2)+1 : N(2)+window_width(2)/2, N(3)-(window_width(3)/2)+1 : N(3)+window_width(3)/2 ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
else
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( LP_pad_kspace( N(1)-(window_width(1)/2)+1 : N(1)+window_width(1)/2, N(2), : ) ) )' ); colormap('gray'); axis image; axis off; caxis([CTkl CTku])
    c2 = c2+1; subplot(spr,3,c2); imagesc( abs( squeeze( LP_pad_kspace( N(1), N(2)-(window_width(2)/2)+1 : N(2)+window_width(2)/2, : ) ) )' ); colormap('gray'); axis equal; axis off; caxis([CTkl CTku])
end

%% Transform LP filtered k-space to image domain

pad_LP_S = jifft3c( LP_pad_kspace );

clear LP_pad_kspace

% save padded high-pass filtered image as NIFTI
% res = [1 1 2];
% datatype = 16;
% jac_nii_save_new_struct( abs(pad_LP_S( (N(1)/2)+1 : end-(N(1)/2), (N(2)/2)+1 : end-(N(2)/2), (N(3)/2)+1 : end-(N(3)/2) ) ), res, datatype, ['LP_magn.nii'] );
% jac_nii_save_new_struct( angle(pad_LP_S( (N(1)/2)+1 : end-(N(1)/2), (N(2)/2)+1 : end-(N(2)/2), (N(3)/2)+1 : end-(N(3)/2) ) ), res, datatype, ['LP_phase.nii'] );
% return


%% Display original phase image & LP filter (bottom row: left & middle)

% set colorscale range
if CT1 == 'minmax'
    CT1l = min( angle( S(:) ));
    CT1u = max( angle( S(:) ));
else
    CT1l = -CT1;
    CT1u = CT1;
end

% display
c2 = c2+1; subplot(spr,3,c2); imagesc( angle( S( (N(1)/2)+1 : end-(N(1)/2), (N(2)/2)+1 : end-(N(2)/2), N(3)+(2*slice_offset) ) )' ); colormap('gray'); axis equal; axis off; caxis([CT1l CT1u])
c2 = c2+1; subplot(spr,3,c2); imagesc( angle( pad_LP_S( (N(1)/2)+1 : end-(N(1)/2), (N(2)/2)+1 : end-(N(2)/2), N(3)+(2*slice_offset) ) )' ); colormap('gray'); axis equal; axis off; caxis([CT1l CT1u])

%% Complex divide MR signal by LP filter to produce a high-pass filtered image

pad_HP_S = S ./ pad_LP_S;

clear S

%% Compute filtered images

pad_HP_magn = abs( pad_HP_S );
pad_HP_phase = angle( pad_HP_S );

clear pad_HP_S

%% Crop filtered images

HP_magn = M .* pad_HP_magn( (N(1)/2)+1 : end-(N(1)/2), (N(2)/2)+1 : end-(N(2)/2), round(N(3)/2)+1 : end-round(N(3)/2) );
HP_phase = M .* pad_HP_phase( (N(1)/2)+1 : end-(N(1)/2), (N(2)/2)+1 : end-(N(2)/2), round(N(3)/2)+1 : end-round(N(3)/2) );

clear pad_HP_*

%% Display HP-filtered phase image (bottom row: right)

% set colorscale range
if CT2 == 'minmax'
    CT2l = min( HP_phase(:) );
    CT2u = max( HP_phase(:) );
else
    CT2l = -CT2;
    CT2u = CT2;
end

% display
c2 = c2+1; subplot(spr,3,c2); imagesc( HP_phase( :, :, round(N(3)/2)+slice_offset)' ); colormap('gray'); axis equal; axis off; caxis([CT2l CT2u])

%% Define output filename

if filter_dim == '2D'
    outname = [ 'hann' filter_dim '_' num2str( window_width(1) ) 'x' num2str( window_width(2) ) '_' phase_froot ];
elseif filter_dim == '3D'
    outname = [ 'hann' filter_dim '_' num2str( window_width(1) ) 'x' num2str( window_width(2) ) 'x' num2str( window_width(3) ) '_' phase_froot ];
end
uout = outname;

%% Save HP-filtered phase image

cd( data_path )
jniisave(HP_phase,phase_struct,outname)

% jac_nii_save( magn_struct, M .* HP_magn, [ 'magn_' outname ]);
% return

%% Save LP-filtered phase image

if save_LP == 1
    LP_phase = M .* squeeze( angle( pad_LP_S( (N(1)/2)+1 : end-(N(1)/2), (N(2)/2)+1 : end-(N(2)/2), round(N(3)/2)+1 : end-round(N(3)/2) )));
    LP_magn = M .* squeeze( abs( pad_LP_S( (N(1)/2)+1 : end-(N(1)/2), (N(2)/2)+1 : end-(N(2)/2), round(N(3)/2)+1 : end-round(N(3)/2) )));
    jniisave(LP_phase,phase_struct,['LP_phase_' outname])

%     jniisave( magn_struct, LP_magn, [ 'LP_magn_' outname ]);
end
    
clear pad_LP_S LP_phase

%% Display slices and save as .gif

if display == 1
    ttt='High-pass filtered phase';
    disp(ttt)
    figure(135)
    jfig( HP_phase, {'gmin','gmax'} )
    title(ttt)    
end
    
%% Display running time

disp( [ 'Hanning filtering (time elapsed): ' num2str( round(toc) ) ' seconds' ] )

%% If further unwrapping is required

if unwrap == 1
    uout = [ 'u' outname ];
    eval( [ '!time prelude -a magn.nii -p ' outname ' -u ' uout ' -m brainmask.nii -f -v' ] )
    if display == 1
        eval( [ '!Slices_18axial ' uout ' ' num2str(-CT) ' ' num2str(CT) ] )
    end
end

end
