function jsegmentextend(roi1_froot,roi2_froot)
% Propagate atlas labels
%
% SINTAX
%  jsegmentextend(roi1_froot,roi2_froot)
% 
% DESCRIPTION 
% 
% ROI#1 represents e.g. a tissue probability map or a binary tissue mask
% (n.b. the loaded image will be binarised using a Pr=0.5 cutoff).
% 
% ROI#2 represent atlas ROI labels
% 
% The algorithm will:
% 
% 1. Find nonzero voxels in ROI#1 with undefined labels in ROI#2
% 
% 2. Assign labels to those voxels based on proximity
% 
% Created by Julio Acosta-Cabronero

%%
tic

programDir          = fileparts(which(mfilename));
ParforProgMonDir    = [programDir '/../j3rdparty/DylanMuir-ParforProgMon'];
addpath(ParforProgMonDir)

% addpath /home/jacosta/jtools/bashmritools/matlab/j3rdparty/DylanMuir-ParforProgMon

[roi1,st1]   = jniiload(roi1_froot);
[roi2,st2]   = jniiload(roi2_froot);

broi1           = uint8(zeros(size(roi1)));
ttt             = broi1;
broi1(roi1>0.5) = 1;

broi2 = uint8(zeros(size(roi2)));
broi2(roi2>0) = 1;

roi3 = (broi1-broi2).*broi1;
roi3(roi3>0) = 1;

%%
idx     = find(roi3==1);
N       = length(idx);
tttidx  = idx(1);
s1      = strel('sphere',1); % struct elem obj
label   = uint16(zeros(size(idx)));
vxdil   = uint8(zeros(size(roi1)));

p = gcp('nocreate');
if isempty(p)
    parpool
end

ppm = ParforProgMon('Propagating labels ',N);

for x = 1:N
%     disp(num2str(x))
    tttidx          = idx(x);
    vxdil(tttidx)   = 1;    
    flag            = true;
    while flag
        vxdil       = imdilate(vxdil,s1);
        ivxdil      = roi2(vxdil>0);
        if max(ivxdil)>0
            label(x) = mode(ivxdil(ivxdil>0));
            flag     = false;
        end
    end
    vxdil(:) = 0;
    ppm.increment(); % progress
end

%%
roi3 = uint16(roi3);
roi3(idx) = label;

roi4 = roi3 + uint16(broi1).*uint16(roi2);
jniisave(roi4,st2,[roi1_froot '_' roi2_froot])

%% Processing time
toc;
time_elapsed_in_hours = toc/3600
