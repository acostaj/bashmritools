function jfliphf( fname )
% Flips image front/back

froot = jfnamesplit( fname, 'gz' );
[ima, st]=jniiload( froot );

for x=1:size(ima,3)
    ima_rot(:,:,x)=ima(:,:,end-x+1);    % HF    
%     ima_rot(:,:,x)=rot90(ima(:,:,x))';  % FB
%     ima_rot(x,:,:)=ima(end-x+1,:,:);    % LR
end
jniisave(ima_rot,st,['flipHF_' froot])
