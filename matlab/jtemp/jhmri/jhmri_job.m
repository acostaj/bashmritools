matlabbatch{1}.spm.tools.hmri.hmri_config.hmri_setdef.customised = {'/home/jacosta/matlab/hMRI/Toolbox/config/local/hmri_local_defaults_trio_v3h_800mum.m'};
matlabbatch{2}.spm.tools.hmri.create_mpm.subj.output.outdir = {'/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/MTprot_MP01751'};
matlabbatch{2}.spm.tools.hmri.create_mpm.subj.sensitivity.RF_us = '-';
matlabbatch{2}.spm.tools.hmri.create_mpm.subj.b1_type.pre_processed_B1.b1input = {
                                                                                  '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wn4_uSumOfSq_MP01751.nii,1'
                                                                                  '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wsmuB1map_MP01751.nii,1'
                                                                                  };
matlabbatch{2}.spm.tools.hmri.create_mpm.subj.b1_type.pre_processed_B1.scafac = 1;
matlabbatch{2}.spm.tools.hmri.create_mpm.subj.raw_mpm.MT = {
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_mt_echo1.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_mt_echo2.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_mt_echo3.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_mt_echo4.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_mt_echo5.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_mt_echo6.nii,1'
                                                            };
matlabbatch{2}.spm.tools.hmri.create_mpm.subj.raw_mpm.PD = {
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_pd_echo1.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_pd_echo2.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_pd_echo3.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_pd_echo4.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_pd_echo5.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_pd_echo6.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_pd_echo7.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_pd_echo8.nii,1'
                                                            };
matlabbatch{2}.spm.tools.hmri.create_mpm.subj.raw_mpm.T1 = {
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_t1_echo1.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_t1_echo2.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_t1_echo3.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_t1_echo4.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_t1_echo5.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_t1_echo6.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_t1_echo7.nii,1'
                                                            '/media/sf_datavb_local/190125_NewPrecision/05_warp/subj02/wMP01751_t1_echo8.nii,1'
                                                            };
matlabbatch{2}.spm.tools.hmri.create_mpm.subj.popup = false;
