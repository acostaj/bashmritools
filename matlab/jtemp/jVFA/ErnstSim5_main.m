function ErnstSim5_main

%% DEFAULT SIMULATION PARAMETERS ==========================================

param.M0      = 3000;                           % initial magnetisation
param.nTE     = 8;                              % # of echoes
param.TE      = (2.3:2.3:2.3*param.nTE)*1e-3;   % in sec
param.T2s     = 35e-3;                          % in sec
param.alpha   = linspace(3,27,6);               % flip angle in deg
param.TR      = 25e-3;                          % in sec
param.T1      = 1.0;                            % typical whole brain avg: 1.27
param.B1      = .9;                            % relative B1 bias
param.SDnoise = 0;                              % noise sigma
param.Niter   = 1;                              % # of simulation repeats

%% DEFAULT FITTING OPTIONS ================================================

param.fitmode = 1;     % [1] Solve for A, R1, B1, R2*   fminsearch
                       % [2] Solve for A, R1, B1, R2*   fmincon with analytical gradient
                       % [3] Solve for A, R1, B1, R2*   fminunc with/without (default) analytical gradient
                       % [4] Solve for A, R1, B1, R2*   fmincon with analytical Hessian. N.B. VERY SLOW! 
                        
param.B1_init = 1;   % B1 initialisation

% fmincon {2,4} options
%==========================================================================
%            M0   R1      B1    R2star            fmincon
%==========================================================================
param.lb = [ 0,   0.10,   0.8,  2             ];  % lower bound constraints
param.ub = [ 1e4, 1.25,   1.2,  1/param.TE(1) ];  % upper bound constraints
%==========================================================================

%% MAIN BODY ==============================================================

SingleRun = false;

if SingleRun
    param.fitmode = 3;
    param.SDnoise = 5;
    param.Niter   = 500;
    ErnstSim5(param)
else
    disp(' ')
    disp('Note the default parameters simulate noiseless GRE data for 5 different flip angles,')
    disp('linspace(3,27,6)')
    disp(' ')
    disp('1. In this first run we will use fitmode1 (i.e. the Nelder-Mead simplex method) to minimise')
    disp('the objective cost function (i.e. SSE) which captures departures of the simulated data')
    disp('w.r.t. the signal equation')
    disp(' ')
    ttt = input('Press RETURN to continue');

    ErnstSim5(param)

    disp(' ')
    disp('We got perfect convergence to the correct solution!')

    disp(' ')
    disp('2. We will now run the same simulation but we will use a different solver, i.e. the sequential')
    disp('quadratic programming method with explicit constraints and analytical cost derivatives (fitmode2)')
    disp(' ')
    ttt = input('Press RETURN to continue');

    param.fitmode = 2;
    ErnstSim5(param)

    disp(' ')
    disp('Good solution again!')

    disp(' ')
    disp('3. Next, we will solve the noiseless problem with the interior-point method - constrained and')
    disp('providing analytical Jacobian and Hessian matrices (fitmode4)')
    disp(' ')
    ttt = input('Press RETURN to continue');

    param.fitmode = 4;
    ErnstSim5(param)

    disp(' ')
    disp('Much slower (due to use of symbolic math toolbox) but accurate convergence!')

    disp(' ')
    disp('4. fitmode3 consists of Matlab fminunc''s quasi-Newton implementation')
    disp(' ')
    ttt = input('Press RETURN to continue');

    param.fitmode = 3;
    ErnstSim5(param)

    disp(' ')
    disp('In the presence of unknown B1 bias, the R1 estimate (using the quasi Newton method) although it improved')
    disp('over the linear approximation, it returned a slightly (negatively) biased value (w.r.t. ground truth)')

    disp(' ')
    disp('... A FEW EXAMPLES MISSING HERE ...')

    disp(' ')
    disp('I will jump straight to the end:')
    disp(' ')
    disp('fitmode3 with noisy data (summary stats based on 500 simulations)')
    disp(' ')
    ttt = input('Press RETURN to continue');

    param.fitmode = 3;
    param.SDnoise = 5;
    param.Niter   = 500;
    ErnstSim5(param)

    disp(' ')
    disp('Interestingly, fitmode3 operating in this noise regime returned R1/B1 estimates with the same bias as that')
    disp('observed in the noiseless case and within 4-5% precision')
    disp(' ')
    ttt = input('N.b. other methods are more unstable in this regime. See e.g. (press RETURN to continue):');

    param.fitmode = 2;
    ErnstSim5(param)

    disp(' ')
    disp('fitmode2 returned much more variable B1/R1 estimates')

    disp(' ')
    ttt = input('To see fitmode1 results in this same regime, press RETURN');

    param.fitmode = 1;
    ErnstSim5(param)

    disp(' ')
    disp('fitmode1 returned even more variable B1/R1 estimates!')
    
    disp(' ')
    disp('The conclusion from these experiments is that fitmode3 is that best candidate method to try out in a real use case')
end