function ErnstSim5(param)

tic

%% Simulation parameters

M0      = param.M0;
nTE     = param.nTE;
TE      = param.TE;
T2s     = param.T2s;
alpha   = param.alpha;
TR      = param.TR;
T1      = param.T1;
B1      = param.B1;
SDnoise = param.SDnoise;
Niter   = param.Niter;

R2s     = 1/T2s;

%% Fitting options

fitmode = param.fitmode;                
B1_init = param.B1_init;

switch fitmode
    case 1
        % fminsearch (Nelder-Mead simplex method)
        options  = optimset(            'MaxIter',1e3,...
                                        'MaxFunEval',2e3,...
                                        'TolX',1e-3,...
                                        'TolFun',1e-3,...
                                        'Display','off');
        
    case 2
        % fmincon (sequential quadratic programming method with analytical cost derivative)
        options  = optimoptions(        'fmincon',...
                                        'Algorithm','sqp',...
                                        'SpecifyObjectiveGradient',true,...
                                        'Display','off',...
                                        'DerivativeCheck','off');
        % N.b. Other algorith options e.g. 'interior-point', 'trust-region-reflective'
          
    case 3
        % fminunc (quasi-Newton algorithm with analytical cost derivative)
        options  = optimoptions(        'fminunc',...
                                        'Algorithm','quasi-newton',...
                                        'SpecifyObjectiveGradient',true,...
                                        'Display','off',...
                                        'DerivativeCheck','off');
        % N.b. Other algorith options: 'trust-region' (slower!)

    case 4
        % fmincon (interior-point method with analytical cost Hessian)
        options  = optimoptions(        'fmincon',...
                                        'Algorithm','interior-point',...
                                        'SpecifyObjectiveGradient',true,...
                                        'Display','off',...
                                        'HessianFcn',@ErnstHessian);
end
     
switch fitmode
    case {2,4}
        lb = param.lb;
        ub = param.ub;
end
        
%% Simulation

disp(' ')
disp('Simulation')
disp('==========')
disp(' ')

for iter=1:Niter

    disp(['Iter #' num2str(iter)])
    
    % TE inside flip angle (rad)
    a       = deg2rad(alpha);

    arep    = [];
    for x = 1:length(a)
        arep    = [arep repmat(a(x),[1 length(TE)])];
    end

    TErep   = repmat(TE,[1 length(a)]);

    R1      = 1/T1;
    R2s     = 1/T2s;

    S1  = M0                 .* exp(-TErep.*R2s);
    S2  = sin(B1.*arep)      .* (1-exp(-TR.*R1));
    S3  = 1 - (cos(B1.*arep) .* exp(-TR.*R1));

    S   = S1 .* S2 ./ S3;

    % Add noise
    N           = SDnoise*randn([1 length(S)]);
    S           = abs(S + N);
    SNR(iter,:) = [min(S/SDnoise) max(S/SDnoise)];

    %% Initial estimates

    T1_init = ((((S(1) / a(1))                    - (S(end-nTE+1) / a(end)) + eps) ./...
             max((S(end-nTE+1) * a(end) / 2 / TR) - (S(1) * a(1) / 2 / TR),eps)) ./ B1_init.^2);

    R1_init = 1./T1_init;

    A_init  = (1./R1_init) .* (S(end-nTE+1) .* (a(end)*B1_init) / 2 / TR) +...
                              (S(end-nTE+1)  ./ (a(end)*B1_init));

    [~,R2star_init] = loglsqfit(S,TErep);

    %% Ernst fitting 

    S_single = S;

    switch fitmode
        case 1        
            % fminsearch
            [best_fit,fval] = fminsearch(@(best_fit) sum(((...
                                    best_fit(1).*exp(-TErep(:).*best_fit(4)).*...
                                    sin(best_fit(3).*arep(:)).*((1-exp(-best_fit(2).*TR(:)))./...
                                    (1-cos(best_fit(3).*arep(:)).*exp(-best_fit(2).*TR(:)))))-...
                                    S_single(:)).^2),[A_init,R1_init,B1_init,R2star_init],options);

        case {2,4}
            % fmincon
            obj             = @(best_fit) ErnstCost(S_single,arep,TErep,TR,best_fit);
            x0              = [A_init,R1_init,B1_init,R2star_init];
            A               = [];
            b               = [];
            Aeq             = [];
            beq             = [];
            nonlcon         = [];
            [best_fit,fval] = fmincon(obj,x0,A,b,Aeq,beq,lb,ub,nonlcon,options);

        case 3
            % fminunc
            obj             = @(best_fit) ErnstCost(S_single,arep,TErep,TR,best_fit);        
            x0              = [A_init,R1_init,B1_init,R2star_init];
            [best_fit,fval] = fminunc(obj,x0,options);
    end

    %% Append results for new iter

    M0_nerror(iter)       = (100*(best_fit(1)-M0)/M0);
    R1_nerror(iter)       = (100*(best_fit(2)-R1)/R1);
    B1_nerror(iter)       = (100*(best_fit(3)-B1)/B1);
    R2star_nerror(iter)   = (100*(best_fit(4)-R2s)/R2s);
    residual(iter)        = fval/(nTE*length(a));

    M0_init_nerror(iter)     = (100*(A_init-M0)/M0);
    R1_init_nerror(iter)     = (100*(R1_init-R1)/R1);
    R2star_init_nerror(iter) = (100*(R2star_init-R2s)/R2s);

    M0_fit(iter)          = best_fit(1);
    R1_fit(iter)          = best_fit(2);
    B1_fit(iter)          = best_fit(3);
    R2star_fit(iter)      = best_fit(4);

    if iter==Niter
        disp(' ')
        disp('Objective values')
        disp('================')
        M0
        R1
        B1
        R2s

        disp(' ')
        disp(['Nonlinear fitting approximation (' num2str(Niter) ' simulation repetitions)'])
        disp( '=============================================================')
        M0_fit
        R1_fit
        B1_fit
        R2star_fit
    end

    M0_init_all(iter)     = A_init;
    R1_init_all(iter)     = R1_init;
    R2star_init_all(iter) = R2star_init;
end

%% Print results

disp(' ')
disp('SNR (min/max)')
disp('==============')

SNR_minmax = round(median(SNR))

disp(' ')
disp('Helms approximation using low/high flip angle data only (% error, median/sigma)')
disp('===============================================================================')

M0_init_nerror      = round([median(M0_init_nerror) std(M0_init_nerror)])
R1_init_nerror      = round([median(R1_init_nerror) std(R1_init_nerror)])

disp(' ')
disp('R2star_init: loglinear fit using all data (% error, median/sigma)')
disp('=================================================================')

R2star_init_nerror  = round([median(R2star_init_nerror) std(R2star_init_nerror)])

disp(' ')
disp('Nonlinear fitting results using all data (% error, median/sigma)')
disp('================================================================')

M0_nerror       = round([median(M0_nerror)      std(M0_nerror)])
R1_nerror       = round([median(R1_nerror)      std(R1_nerror)])
B1_nerror       = round([median(B1_nerror)      std(B1_nerror)])
R2star_nerror   = round([median(R2star_nerror)  std(R2star_nerror)])

disp(' ')
disp('Normalised residual (median/sigma)')
disp('==================================')

residual        =       [median(residual)       std(residual)]

disp(' ')
disp('R1/B1 summary')
disp('=============')

fprintf('R1: Objective=%.2f, Nonlinear fit median=%.2f, Helms median=%.2f, Nonlinear fit sigma=%.2f, Helms sigma=%.2f\n',...
            R1,median(R1_fit),median(R1_init_all),std(R1_fit),std(R1_init_all));

disp(' ')        

fprintf('B1: Objective=%.2f, Nonlinear fit median=%.2f, Nonlinear fit min=%.2f, Nonlinear fit max=%.2f, Nonlinear fit sigma=%.2f\n',...
            B1,median(B1_fit),min(B1_fit),max(B1_fit),std(B1_fit));

disp(' ')        
        
toc

%% Nested functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [S0,R2star] = loglsqfit(S,TE)

    logS    = log(S);
    O       = ones([length(TE) 1]);
    TE      = [O TE'];
    P       = TE\logS';
    R2star  = -P(2,:)';
    S0      = exp(P(1,:)');

end

function [E,k1,k2,k3,k4,k5,h1,h2,h3] = ErnstForward(arep,TErep,TR,u)

    % Precalculations
    k1 = exp(-u(4)*TErep);
    h1 = u(1)*k1;
    
    k2 = sin(u(3)*arep);
    k3 = exp(-u(2)*TR);
    k4 = 1-k3;
    h2 = k2.*k4;
    
    k5 = cos(u(3)*arep);
    h3 = 1-k5.*k3;
    
    % Ernst eq.
    E = h1.*h2./h3;   
    
end

function [f,df] = ErnstCost(S_single,arep,TErep,TR,u)

    % Forward calculations
    [E,k1,k2,k3,k4,k5,h1,h2,h3] = ErnstForward(arep,TErep,TR,u);    
    
    % Objective error function
    [f] = sum((E-S_single).^2);

    % Analytical derivatives
    if nargout>1
        [m]     = 2*(E-S_single);
        [df_u1] = sum(m.*k1.*h2./h3);
        [df_u2] = sum(m.*(((h1.*k3.*TR)./h3.^2) .* (k2.*h3     - h2.*k5)));
        [df_u3] = sum(m.*(((h1.*arep)  ./h3.^2) .* (k4.*k5.*h3 - h2.*k3.*k2))); 
        [df_u4] = sum(m.*(u(1)*(h2./h3).*(-TErep).*k1));
        [df]    = [df_u1; df_u2; df_u3; df_u4];
    end
    
end

function [df] = ErnstDiffCost(S_single,arep,TErep,TR,u)

    % Forward calculations
    [E,k1,k2,k3,k4,k5,h1,h2,h3] = ErnstForward(arep,TErep,TR,u);    

    % Analytical derivatives
    [m]     = 2*(E-S_single);
    [df_u1] = sum(m.*k1.*h2./h3);
    [df_u2] = sum(m.*(((h1.*k3.*TR)./h3.^2) .* (k2.*h3     - h2.*k5)));
    [df_u3] = sum(m.*(((h1.*arep)  ./h3.^2) .* (k4.*k5.*h3 - h2.*k3.*k2))); 
    [df_u4] = sum(m.*(u(1)*(h2./h3).*(-TErep).*k1));
    [df]    = [df_u1; df_u2; df_u3; df_u4];
    
end

function [hessian] = ErnstHessian(u,~)

    [H,sA,sR1,sB1,sR2star] = SymErnstHessian(u);
    h = vpa(subs(H,{sA,sR1,sB1,sR2star},{u(1),u(2),u(3),u(4)}));
    hessian = double(h);
        
end

function [H,sA,sR1,sB1,sR2star] = SymErnstHessian(u)

    nTE     = 8;
    TR      = 25e-3;                    % in sec
    TE      = (2.3:2.3:2.3*nTE)*1e-3;   % in sec
    alpha   = linspace(3,27,6);         % flip angle in deg    
    a       = deg2rad(alpha);
    arep    = [];
    for x = 1:length(a)
        arep    = [arep repmat(a(x),[1 length(TE)])];
    end
    TErep   = repmat(TE,[1 length(a)]);

    syms sA sR1 sB1 sR2star

    % Precalculations
    k1 = exp(-sR2star.*TErep);
    h1 = sA.*k1;

    k2 = sin(sB1.*arep);
    k3 = exp(-sR1.*TR);
    k4 = 1-k3;
    h2 = k2.*k4;

    k5 = cos(sB1.*arep);
    h3 = 1-k5.*k3;

    % Ernst eq.
    E = h1.*h2./h3;

    % Calculate expected signals
    S = vpa(subs(E,{sA,sR1,sB1,sR2star},{u(1),u(2),u(3),u(4)}));

    % First derivatives
    der_analytical = true;
    if der_analytical
        m           = 2*(E-S);
        df_dA       = sum(m.*k1.*h2./h3);
        df_dR1      = sum(m.*(((h1.*k3.*TR)./h3.^2) .* (k2.*h3     - h2.*k5)));
        df_dB1      = sum(m.*(((h1.*arep)  ./h3.^2) .* (k4.*k5.*h3 - h2.*k3.*k2))); 
        df_dR2star  = sum(m.*(sA.*(h2./h3).*(-TErep).*k1));
    else     
        % Cost function
        lambda = .1;
        f = sum((E-S).^2) + lambda*(log(sB1)^2);
        % Derivatives
        df_dA       = diff(f,sA);
        df_dR1      = diff(f,sR1);
        df_dB1      = diff(f,sB1);
        df_dR2star  = diff(f,sR2star);
    end
    
    % Second derivatives (Hessian matrix)
    H(1,1)      = diff(df_dA,sA);
    H(1,2)      = diff(df_dA,sR1);
    H(1,3)      = diff(df_dA,sB1);
    H(1,4)      = diff(df_dA,sR2star);

    H(2,1)      = diff(df_dR1,sA);
    H(2,2)      = diff(df_dR1,sR1);
    H(2,3)      = diff(df_dR1,sB1);
    H(2,4)      = diff(df_dR1,sR2star);

    H(3,1)      = diff(df_dB1,sA);
    H(3,2)      = diff(df_dB1,sR1);
    H(3,3)      = diff(df_dB1,sB1);
    H(3,4)      = diff(df_dB1,sR2star);

    H(4,1)      = diff(df_dR2star,sA);
    H(4,2)      = diff(df_dR2star,sR1);
    H(4,3)      = diff(df_dR2star,sB1);
    H(4,4)      = diff(df_dR2star,sR2star);
    
end

end