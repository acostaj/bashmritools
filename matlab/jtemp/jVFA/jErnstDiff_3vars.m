clear all

syms A R1 R2star ALPHA TE TR S

% Precalculations
k1 = exp(-R2star*TE);
h1 = A*k1;

k2 = sin(ALPHA);
k3 = exp(-R1*TR);
k4 = 1-k3;
h2 = k2*k4;

k5 = cos(ALPHA);
h3 = 1-k5*k3;

% Ernst eq.
E = h1*h2/h3;

% Error function
f = sum((E-S).^2);

% First derivatives
df_dA       = diff(f,A);
df_dR1      = diff(f,R1);
df_dR2star  = diff(f,R2star);

% Second derivatives (Hessian matrix)
H(1,1)      = diff(df_dA,A);
H(1,2)      = diff(df_dA,R1);
H(1,3)      = diff(df_dA,R2star);

H(2,1)      = diff(df_dR1,A);
H(2,2)      = diff(df_dR1,R1);
H(2,3)      = diff(df_dR1,R2star);

H(3,1)      = diff(df_dR2star,A);
H(3,2)      = diff(df_dR2star,R1);
H(3,3)      = diff(df_dR2star,R2star);

%%
% a       = 3000;                     % initial magnetisation
% te      = 2.3e-3;                   % in sec
% r2s     = 1/35e-3;                  % in sec
% alpha   = deg2rad(3);               % flip angle in deg
% tr      = 25e-3;                    % in sec
% r1      = 1/1.5;                    % typical whole brain avg: 1/1.27

tr      = 25e-3;
N       = 5;
c       = 0;
SDnoise = .1;
SNR     = zeros([1 N^5]);
eig_h   = zeros([3 N^5]);

for a       = linspace(100,4000,N)                  
for te      = linspace(2.3e-3,25e-3,N)                
for r2s     = linspace(10,100,N)
for alpha   = linspace(1,30,N)
for r1      = linspace(0.2,1.2,N)   
% for SDnoise = linspace(1,10,N)

    c=c+1;
    
    s           = vpa(subs(E,{A,R1,R2star,ALPHA,TE,TR},{a,r1,r2s,alpha,te,tr}));
    s           = abs(s + SDnoise*randn(1));
    SNR(c)      = s/SDnoise;
    h           = vpa(subs(H,{A,R1,R2star,ALPHA,TE,TR,S},{a,r1,r2s,alpha,te,tr,s}));
    eig_h(:,c)  = eig(h);

% end
end; end; end; end; end

mean(SNR)
for x=1:3, median(eig_h(x,:)), end