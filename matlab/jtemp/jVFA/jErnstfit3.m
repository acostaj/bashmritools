clear all
close all

fitmode = 6;                % [1] plain Ernst
                            % [2] solve for B1 (B1_init=1)
                            % [3] single TE, solve for PD* & R1 only
                            % [4] solve for PD, R1, R2* & B1 providing B1_init
                            % [5] solve for PD, R1, R2*      providing B1
                            % [6] solve for PD, R1, R2* & B1 with constraints (fmincon)

if fitmode==6
    global arep TErep TR S_single
end
                            
dataset = 2;
switch dataset
    case 1
        alpha   = [12 21 30 6];
        init    = [4 3];
        TR      = 25e-3;
        TE      = 0;
        
    case 2
        alpha   = [15 21 27 3 9];       % nominal flip angle in degrees
        init    = [4 3];                % index pair (low alpha, high alpha) for R1/A initialisation
        TR      = 25e-3;                % repetition time in seconds
        TE      = (2.4:2.3:18.5)*1e-3;  % echo time(s) in seconds

    case 3
        alpha   = [21 3];
        init    = [2 1];
        TR      = 25e-3;
        TE      = (2.4:2.3:18.5)*1e-3;
end
   
w = 20;   % 3D cropping window size in voxels [if w=0 => do not crop]

list    = dir('*.nii');     % fname snippet

addpath DylanMuir-ParforProgMon-fd6bc94

%% Transform flip angles to radians and replicate vector
arep = [];
for x = 1:length(alpha)
    a0(x)   = double(deg2rad(alpha(x)));
    arep    = [arep repmat(a0(x),[1 length(TE)])];
end

%% Replicate TE vector
nTE     = length(TE);
TErep   = repmat(TE,[1 length(alpha)]);

%% Load data
for x = 1:length(list)
    fname    = list(x).name;
    froot    = jfnamesplit(fname,'gz');
    [ttt,st] = jniiload(froot);
    if w>0
        ttt      = ttt(round(end/2-w/2+1):round(end/2+w/2),round(end/2-w/2+1):round(end/2+w/2),round(end/2-w/2+1):round(end/2+w/2));
    end
    if x==1
        S = zeros([size(ttt) length(list)]);
    end
    S(:,:,:,x) = ttt;
end

matrix = size(ttt);
 clear ttt
 
st(1).dt(1)=16; % floating point

d0 = pwd;

if fitmode==4 || fitmode==5
    cd([d0 '/B1'])
    list2 = dir('*.nii');     % fname snippet
    for x = 1:length(list2)
        fname    = list2(x).name;
        froot    = jfnamesplit(fname,'gz');
        [ttt,st] = jniiload(froot);
        if w>0
            ttt  = ttt(round(end/2-w/2+1):round(end/2+w/2),round(end/2-w/2+1):round(end/2+w/2),round(end/2-w/2+1):round(end/2+w/2));
        end
        if x==1
            B1_init = zeros([size(ttt) length(list2)]);
        end
        B1_init(:,:,:,x) = ttt/100; 
    end
     clear ttt
    cd(d0)
%     B1_init = B1_init.*0+1;
end
     
%% Results dir
date1 = datestr(now,'yymmdd');
date2 = datestr(now,'HHMMSS');
wdir  = [d0 '/jErnstFit2_' date1 '_' date2 '_fitmode' num2str(fitmode)];
disp(['Results dir: ' wdir])
mkdir(wdir)
cd(wdir)

%% Starting matrices

if nTE==1
    for x = 1:2
        S0(:,:,:,x) = S(:,:,:,init(x));
    end
    
else
    disp('Calculate R2star_init')

    [~,R2star_init] = loglsqfit(S,TErep);

     jniisave(R2star_init,st,'R2star_init')

    disp('Calculate S0_init')

    for x = 1:2
        S0(:,:,:,x) = loglsqfit(S(:,:,:,(init(x)-1)*nTE+1:init(x)*nTE),TErep((init(x)-1)*nTE+1:init(x)*nTE));
    end
end

disp('Calculate R1_init')

if fitmode==4 || fitmode==5
    T1_init = ((((S0(:,:,:,1) / a0(init(1)))          - (S0(:,:,:,2) / a0(init(2))) + eps) ./...
             max((S0(:,:,:,2) * a0(init(2)) / 2 / TR) - (S0(:,:,:,1) * a0(init(1)) / 2 / TR),eps)) ./ B1_init.^2);
    R1_init = 1./T1_init;
    clear T1_init
    
     jniisave(B1_init,st,'B1_init')
    
else
    R1_init = (((S0(:,:,:,2) * (a0(init(2)) / 2 / TR)) - (S0(:,:,:,1) * (a0(init(1)) / 2 / TR))) ./ ...
           max(((S0(:,:,:,1) /  a0(init(1))          ) - (S0(:,:,:,2) /  a0(init(2)))          ),eps));
end
       
 jniisave(R1_init,st,'R1_init')
       
disp('Calculate PD_init')
 
if fitmode==4 || fitmode==5
    A_init = (1./R1_init) .* (S0(:,:,:,2) .* (a0(init(2))*B1_init) / 2 / TR) +...
                             (S0(:,:,:,2) ./ (a0(init(2))*B1_init));
else
    A_init  = (1./R1_init).*((S0(:,:,:,2) * a0(init(2))) / (2*TR)) +...
                             (S0(:,:,:,2) / a0(init(2)));
end                      

 jniisave(A_init,st,'A_init')
 
%% Initialise optimisation

mat1        = [matrix(2) matrix(3)];
mat2        = [1 matrix(2)];
mat2b       = matrix(2);
mat3        = matrix(3);
A           = zeros(matrix); 
R1          = zeros(matrix);
if fitmode~=3
    R2star  = zeros(matrix);
end
if fitmode==2 || fitmode==4 || fitmode==6
    B1      = zeros(matrix);
end
residual    = zeros(matrix);
N           = matrix(1);

if fitmode<=5
    options = optimset('MaxIter',1e3,'MaxFunEval',2e3,'TolX',1e-3,'TolFun',1e-3,'Display','off');
else
    options = optimoptions('fmincon','Algorithm','sqp','SpecifyObjectiveGradient',true,'Display','off','DerivativeCheck','off');
end

p = gcp('nocreate');
if isempty(p)
    parpool
end

ppm = ParforProgMon('Ernst Eq. fitting ',N);

%% Outer loop

tic
parfor x = 1:N
    disp(['x = ' num2str(x)]);
    
    a_      = zeros(mat1);
    r1_     = zeros(mat1);
    if fitmode~=3
        r2star_ = zeros(mat1);
    end
    if fitmode==2 || fitmode==4 || fitmode==6
        b1_ = zeros(mat1);
    end
    r_      = zeros(mat1);
    
    for z = 1:mat3
        
        a       = zeros(mat2);
        r1      = zeros(mat2);
        if fitmode~=3
            r2star  = zeros(mat2);
        end
        if fitmode==2 || fitmode==4 || fitmode==6
            b1  = zeros(mat2);
        end
        r       = zeros(mat2);
        
        for y = 1:mat2b
            
            S_single = double(squeeze(S(x,y,z,:)));
            
            switch fitmode
                case 1
                    [best_fit,fval] = fminsearch(@(best_fit) sum(...
                        ((best_fit(1).*exp(-TErep(:).*best_fit(3)).*sin(arep(:)).*...
                        ((1-exp(-best_fit(2).*TR(:)))./...
                        (1-cos(arep(:)).*exp(-best_fit(2).*TR(:)))))-...
                %% Processing time
toc
time_elapsed_in_mins    = toc/60
time_elapsed_in_hrs     = toc/3600        S_single(:)).^2),...
                        [A_init(x,y,z),R1_init(x,y,z),R2star_init(x,y,z)],options);
                    
                case 2
                    [best_fit,fval] = fminsearch(@(best_fit) sum(((...
                        best_fit(1).*exp(-TErep(:).*best_fit(3)).*...
                        sin(best_fit(4).*arep(:)).*(...
                        (1-exp(-best_fit(2).*TR(:)))./...
                        (1-cos(best_fit(4).*arep(:)).*exp(-best_fit(2).*TR(:)))...
                        ))-S_single(:)).^2),...
                        [A_init(x,y,z),R1_init(x,y,z),R2star_init(x,y,z),1],options);
                    
                case 3
                    [best_fit,fval] = fminsearch(@(best_fit) sum(((...
                        best_fit(1).*sin(arep(:)).*(...
                        (1-exp(-best_fit(2).*TR(:)))./...
                        (1-cos(arep(:)).*exp(-best_fit(2).*TR(:)))...
                        ))-S_single(:)).^2),...
                        [A_init(x,y,z),R1_init(x,y,z)],options);
                    
                case 4
                    [best_fit,fval] = fminsearch(@(best_fit) sum(((...
                        best_fit(1).*exp(-TErep(:).*best_fit(3)).*...
                        sin(best_fit(4).*arep(:)).*(...
                        (1-exp(-best_fit(2).*TR(:)))./...
                        (1-cos(best_fit(4).*arep(:)).*exp(-best_fit(2).*TR(:)))...
                        ))-S_single(:)).^2),...
                        [A_init(x,y,z),R1_init(x,y,z),R2star_init(x,y,z),B1_init(x,y,z)],options);
                    
                case 5
                    [best_fit,fval] = fminsearch(@(best_fit) sum(((...
                        best_fit(1).*exp(-TErep(:).*best_fit(3)).*...
                        sin(B1_init(x,y,z).*arep(:)).*(...
                        (1-exp(-best_fit(2).*TR(:)))./...
                        (1-cos(B1_init(x,y,z).*arep(:)).*exp(-best_fit(2).*TR(:)))...
                        ))-S_single(:)).^2),...
                        [A_init(x,y,z),R1_init(x,y,z),R2star_init(x,y,z)],options);
                    
                case 6
                    obj             = @ErnstCost;
                    x0              = [A_init(x,y,z),R1_init(x,y,z),1,R2star_init(x,y,z)];
                    A               = [];
                    b               = [];
                    Aeq             = [];
                    beq             = [];
                    nonlcon         = [];
                    [best_fit,fval] = fmincon(obj,x0,A,b,Aeq,beq,lb,ub,nonlcon,options);
               
            end
            
            a(y)        = best_fit(1);
            r1(y)       = best_fit(2);
            if fitmode~=3
                r2star(y)   = best_fit(3);
            end
            if fitmode==2 || fitmode==4 || fitmode==6
                b1(y)   = best_fit(4);
            end
            r(y)        = fval;
            
        end
        
        a_(:,z)         = a;
        r1_(:,z)        = r1;
        if fitmode~=3
            r2star_(:,z)    = r2star;
        end
        if fitmode==2 || fitmode==4 || fitmode==6
            b1_(:,z)    = b1;
        end
        r_(:,z)         = r;             
        
    end
    
    A(x,:,:)        = a_;
    R1(x,:,:)       = r1_;
    if fitmode~=3
        R2star(x,:,:)   = r2star_;
    end
    if fitmode==2 || fitmode==4 || fitmode==6
        B1(x,:,:)   = b1_;
    end
    residual(x,:,:) = r_;
    
    ppm.increment(); % progress
end

%% Processing time
toc
time_elapsed_in_mins    = toc/60
time_elapsed_in_hrs     = toc/3600

%% Save maps as NIFTI
 jniisave(A,st,'A')
 jniisave(R1,st,'R1')
if fitmode~=3
     jniisave(R2star,st,'R2star')
end
if fitmode==2 || fitmode==4 || fitmode==6
     jniisave(B1,st,'B1')
end
 jniisave(residual,st,'fval')

cd(d0)

%% SUBFUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [f,df] = ErnstCost(u)

    global arep TErep TR S_single
    
    % Precomputation
    k1 = exp(-u(4)*TErep(:));
    h1 = u(1)*k1;
    
    k2 = sin(u(3)*arep(:));
    k3 = exp(-u(2)*TR(:));
    k4 = 1-k3;
    h2 = k2.*k4;
    
    k5 = cos(u(3)*arep(:));
    h3 = 1-k5.*k3;
        
    g = h1.*h2./h3;
    
    m = 2*(g-S_single(:));
    
    % Objective
    f = sum((g-S_single(:)).^2);
        
    % Explicit derivatives
    if nargout>1
        df_u1 = sum(m.*k1.*h2./h3);
        df_u2 = sum(m.*(((h1.*k3.*TR(:))./h3.^2) .* (k2.*h3     - h2.*k5)));
        df_u3 = sum(m.*(((h1.*arep(:))  ./h3.^2) .* (k4.*k5.*h3 - h2.*k3.*k2))); 
        df_u4 = sum(m.*(u(1)*(h2./h3).*(-TErep(:)).*k1));
        df    = [df_u1; df_u2; df_u3; df_u4];
    end
end

function [S0,R2star] = loglsqfit(S,TE)
    logS    = log(S);
%      clear S
    sz      = size(logS);
    logS    = reshape(logS,[sz(1)*sz(2)*sz(3) sz(4)]);
    O       = ones([length(TE) 1]);
    TE      = [O TE'];
    P       = TE\logS';
%      clear logS
    R2star  = -P(2,:)';
    S0      = exp(P(1,:)');
%      clear P
    S0      = reshape(S0,       [sz(1) sz(2) sz(3)]);
    R2star  = reshape(R2star,   [sz(1) sz(2) sz(3)]);
end
