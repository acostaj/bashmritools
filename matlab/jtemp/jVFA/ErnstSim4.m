clear all

tic

%% Simulation parameters

%==========================================================================                
M0      = 3000;                     % initial magnetisation
nTE     = 8;                        % # of echoes
TE      = (2.3:2.3:2.3*nTE)*1e-3;   % in sec
T2s     = 35e-3;                    % in sec
alpha   = linspace(3,27,6);         % flip angle in deg
TR      = 25e-3;                    % in sec
T1      = 1.5;                      % typical whole brain avg: 1.27
B1      = 1.1;                      % relative B1 bias
SDnoise = 5;                        % noise sigma
Niter   = 5;                        % # of simulation repeats
%==========================================================================

R2s     = 1/T2s;

%% Fitting options

fitmode  = 5;   % [1] Solve for A, R1, B1, R2* (fminsearch)
                % [2] Solve for A, R1, B1, R2* (fmincon with analytical gradient) 
                % [3] Solve for A, R1, B1, R2* (fminunc) 
                % [4] Solve for A, R1, B1, R2* (CG)
                % [5] Solve for A, R1, B1, R2* (fmincon with analytical Hessian) 

B1_init = 1.0;                      % B1 initialisation

switch fitmode
    case 1
        % fminsearch (Nelder-Mead simplex method)
        options  = optimset('MaxIter',1e3,'MaxFunEval',2e3,'TolX',1e-3,'TolFun',1e-3,'Display','off');
        
    case 2
        % fmincon (sequential quadratic programming method)
        options  = optimoptions('fmincon','Algorithm','sqp','SpecifyObjectiveGradient',true,'Display','off','DerivativeCheck','off');
%         options  = optimoptions('fmincon','Algorithm','interior-point');
%         options  = optimoptions('fmincon','Algorithm','trust-region-reflective');
          
    case 3
        % fminunc (trust-region reflective algorithm)
        options  = optimoptions('fminunc','Algorithm','trust-region','SpecifyObjectiveGradient',true,'Display','off','DerivativeCheck','off');
        
    case 4
        cg_max_iter     = 2000;
        cg_tol          = 1e-3;
        verbose         = 1;
        
    case 5
        options  = optimoptions('fmincon','Algorithm','interior-point','SpecifyObjectiveGradient',true,'Display','off','HessianFcn',@ErnstHessian);
end
     
switch fitmode
    case {2,5}
        %==========================================================================
        %            M0   R1      B1     R2star          fmincon
        %==========================================================================
        lb      = [  0,   0.10,   0.8,   2       ];      % lower bound constraints
        ub      = [  1e4, 1.25,   1.2,   1/TE(1) ];      % upper bound constraints
        %==========================================================================
end
        
%% Simulation

disp(' ')
disp('Simulation')
disp('==========')
disp(' ')

for iter=1:Niter

    disp(['Iter #' num2str(iter)])
    
% TE inside flip angle (rad)
a       = deg2rad(alpha);

arep    = [];
for x = 1:length(a)
    arep    = [arep repmat(a(x),[1 length(TE)])];
end

TErep   = repmat(TE,[1 length(a)]);

R1      = 1/T1;
R2s     = 1/T2s;

S1  = M0                 .* exp(-TErep.*R2s);
S2  = sin(B1.*arep)      .* (1-exp(-TR.*R1));
S3  = 1 - (cos(B1.*arep) .* exp(-TR.*R1));

S   = S1 .* S2 ./ S3;

% Add noise
% N   = (prctile(S,R2star5)/SNR)*randn([1 length(S)]);
N           = SDnoise*randn([1 length(S)]);
S           = abs(S + N);
SNR(iter,:) = [min(S/SDnoise) max(S/SDnoise)];

%% Initial estimates

T1_init = ((((S(1) / a(1))                    - (S(end-nTE+1) / a(end)) + eps) ./...
         max((S(end-nTE+1) * a(end) / 2 / TR) - (S(1) * a(1) / 2 / TR),eps)) ./ B1_init.^2);

R1_init = 1./T1_init;

A_init  = (1./R1_init) .* (S(end-nTE+1) .* (a(end)*B1_init) / 2 / TR) +...
                          (S(end-nTE+1)  ./ (a(end)*B1_init));

[~,R2star_init] = loglsqfit(S,TErep);

%% Ernst fitting 
 
S_single = S;

switch fitmode
    case 1        
        % fminsearch (Nelder-Mead simplex method)
        [best_fit,fval] = fminsearch(@(best_fit) sum(((...
                                best_fit(1).*exp(-TErep(:).*best_fit(4)).*...
                                sin(best_fit(3).*arep(:)).*((1-exp(-best_fit(2).*TR(:)))./...
                                (1-cos(best_fit(3).*arep(:)).*exp(-best_fit(2).*TR(:)))))-...
                                S_single(:)).^2),[A_init,R1_init,B1_init,R2star_init],options);
                            
    case {2,5}
        % fmincon (sequential quadratic programming method)
        obj             = @(best_fit) ErnstCost(S_single,arep,TErep,TR,best_fit);
        x0              = [A_init,R1_init,B1_init,R2star_init];
        A               = [];
        b               = [];
        Aeq             = [];
        beq             = [];
        nonlcon         = [];
        [best_fit,fval] = fmincon(obj,x0,A,b,Aeq,beq,lb,ub,nonlcon,options);
        
    case 3
        % fminunc (trust-region reflective algorithm)
        obj             = @(best_fit) ErnstCost(S_single,arep,TErep,TR,best_fit);        
        x0              = [A_init,R1_init,B1_init,R2star_init];
        [best_fit,fval] = fminunc(obj,x0,options);
        
    case 4
        % conjugate gradient
        x0              = [A_init,R1_init,B1_init,R2star_init];
%         A               = @(best_fit) ErnstCost(S_single',arep,TErep,TR,best_fit);
        A               = @(best_fit) ErnstDiffCost(S_single,arep,TErep,TR,best_fit);
        b               = zeros(size(x0));
        [best_fit,rsd]  = jcgsolve2(A,b,cg_tol,cg_max_iter,verbose,x0);
        fval            = ErnstCost(S_single,arep,TErep,TR,best_fit); % SoS errors for best_fit
%         A               = @(best_fit) ErnstForward(arep,TErep,TR,best_fit);
%         b               = S_single;
%         [best_fit,rsd]  = jcgsolve(A,b,cg_tol,cg_max_iter,verbose,x0);
%         [best_fit,fval] = jcgsolve2(S_single,arep,TErep,TR,x0,cg_tol);
end

%% Store results

M0_nerror(iter)       = (100*(best_fit(1)-M0)/M0);
R1_nerror(iter)       = (100*(best_fit(2)-R1)/R1);
B1_nerror(iter)       = (100*(best_fit(3)-B1)/B1);
R2star_nerror(iter)   = (100*(best_fit(4)-R2s)/R2s);
residual(iter)        = fval/(nTE*length(a));

M0_init_nerror(iter)     = (100*(A_init-M0)/M0);
R1_init_nerror(iter)     = (100*(R1_init-R1)/R1);
R2star_init_nerror(iter) = (100*(R2star_init-R2s)/R2s);

M0_fit(iter)          = best_fit(1);
R1_fit(iter)          = best_fit(2);
B1_fit(iter)          = best_fit(3);
R2star_fit(iter)      = best_fit(4);

if iter==Niter
    disp(' ')
    disp('Objective values')
    disp('================')
    M0
    R1
    B1
    R2s
    
    disp(' ')
    disp(['Nonlinear fitting approximation (' num2str(Niter) ' simulation repetitions)'])
    disp( '=============================================================')
    M0_fit
    R1_fit
    B1_fit
    R2star_fit
end

M0_init_all(iter)     = A_init;
R1_init_all(iter)     = R1_init;
R2star_init_all(iter) = R2star_init;

end

%% Print results

disp(' ')
disp('SNR (min/max)')
disp('==============')

SNR_minmax = round(median(SNR))

disp(' ')
disp('Helms approximation using low/high flip angle data only (% error, median/sigma)')
disp('===============================================================================')

M0_init_nerror      = round([median(M0_init_nerror) std(M0_init_nerror)])
R1_init_nerror      = round([median(R1_init_nerror) std(R1_init_nerror)])

disp(' ')
disp('R2star_init: loglinear fit using all data (% error, median/sigma)')
disp('=================================================================')

R2star_init_nerror  = round([median(R2star_init_nerror) std(R2star_init_nerror)])

disp(' ')
disp('Nonlinear fitting results using all data (% error, median/sigma)')
disp('================================================================')

M0_nerror       = round([median(M0_nerror)      std(M0_nerror)])
R1_nerror       = round([median(R1_nerror)      std(R1_nerror)])
B1_nerror       = round([median(B1_nerror)      std(B1_nerror)])
R2star_nerror   = round([median(R2star_nerror)  std(R2star_nerror)])

disp(' ')
disp('Normalised residual (median/sigma)')
disp('==================================')

residual        =       [median(residual)       std(residual)]

disp(' ')
disp('R1/B1 summary')
disp('=============')

fprintf('R1: Objective=%.2f, Nonlinear fit median=%.2f, Helms median=%.2f, Nonlinear fit sigma=%.2f, Helms sigma=%.2f\n',...
            R1,median(R1_fit),median(R1_init_all),std(R1_fit),std(R1_init_all));

disp(' ')        

fprintf('B1: Objective=%.2f, Nonlinear fit median=%.2f, Nonlinear fit min=%.2f, Nonlinear fit max=%.2f, Nonlinear fit sigma=%.2f\n',...
            B1,median(B1_fit),min(B1_fit),max(B1_fit),std(B1_fit));

disp(' ')        
        
toc

%% Nested functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [S0,R2star] = loglsqfit(S,TE)

    logS    = log(S);
    O       = ones([length(TE) 1]);
    TE      = [O TE'];
    P       = TE\logS';
    R2star  = -P(2,:)';
    S0      = exp(P(1,:)');

end

function [E,k1,k2,k3,k4,k5,h1,h2,h3] = ErnstForward(arep,TErep,TR,u)

    % Precalculations
    k1 = exp(-u(4)*TErep);
    h1 = u(1)*k1;
    
    k2 = sin(u(3)*arep);
    k3 = exp(-u(2)*TR);
    k4 = 1-k3;
    h2 = k2.*k4;
    
    k5 = cos(u(3)*arep);
    h3 = 1-k5.*k3;
    
    % Ernst eq.
    E = h1.*h2./h3;   
    
end

function [f,df] = ErnstCost(S_single,arep,TErep,TR,u)

    % Forward calculations
    [E,k1,k2,k3,k4,k5,h1,h2,h3] = ErnstForward(arep,TErep,TR,u);    
    
    % Objective error function
    [f] = sum((E-S_single).^2);

    % Analytical derivatives
    if nargout>1
        [m]     = 2*(E-S_single);
        [df_u1] = sum(m.*k1.*h2./h3);
        [df_u2] = sum(m.*(((h1.*k3.*TR)./h3.^2) .* (k2.*h3     - h2.*k5)));
        [df_u3] = sum(m.*(((h1.*arep)  ./h3.^2) .* (k4.*k5.*h3 - h2.*k3.*k2))); 
        [df_u4] = sum(m.*(u(1)*(h2./h3).*(-TErep).*k1));
        [df]    = [df_u1; df_u2; df_u3; df_u4];
    end
    
end

function [df] = ErnstDiffCost(S_single,arep,TErep,TR,u)

    % Forward calculations
    [E,k1,k2,k3,k4,k5,h1,h2,h3] = ErnstForward(arep,TErep,TR,u);    

    % Analytical derivatives
    [m]     = 2*(E-S_single);
    [df_u1] = sum(m.*k1.*h2./h3);
    [df_u2] = sum(m.*(((h1.*k3.*TR)./h3.^2) .* (k2.*h3     - h2.*k5)));
    [df_u3] = sum(m.*(((h1.*arep)  ./h3.^2) .* (k4.*k5.*h3 - h2.*k3.*k2))); 
    [df_u4] = sum(m.*(u(1)*(h2./h3).*(-TErep).*k1));
    [df]    = [df_u1; df_u2; df_u3; df_u4];
    
end

function [hessian] = ErnstHessian(u,~)

    [H,sA,sR1,sB1,sR2star] = SymErnstHessian(u);
    h = vpa(subs(H,{sA,sR1,sB1,sR2star},{u(1),u(2),u(3),u(4)}));
    hessian = double(h);
        
end

function [H,sA,sR1,sB1,sR2star] = SymErnstHessian(u)

    nTE     = 8;
    TR      = 25e-3;                    % in sec
    TE      = (2.3:2.3:2.3*nTE)*1e-3;   % in sec
    alpha   = linspace(3,27,6);         % flip angle in deg    
    a       = deg2rad(alpha);
    arep    = [];
    for x = 1:length(a)
        arep    = [arep repmat(a(x),[1 length(TE)])];
    end
    TErep   = repmat(TE,[1 length(a)]);

    syms sA sR1 sB1 sR2star

    % Precalculations
    k1 = exp(-sR2star.*TErep);
    h1 = sA.*k1;

    k2 = sin(sB1.*arep);
    k3 = exp(-sR1.*TR);
    k4 = 1-k3;
    h2 = k2.*k4;

    k5 = cos(sB1.*arep);
    h3 = 1-k5.*k3;

    % Ernst eq.
    E = h1.*h2./h3;

    % Calculate expected signals
    S = vpa(subs(E,{sA,sR1,sB1,sR2star},{u(1),u(2),u(3),u(4)}));

    % First derivatives
    der_analytical = false;
    if der_analytical
        m           = 2*(E-S);
        df_dA       = sum(m.*k1.*h2./h3);
        df_dR1      = sum(m.*(((h1.*k3.*TR)./h3.^2) .* (k2.*h3     - h2.*k5)));
        df_dB1      = sum(m.*(((h1.*arep)  ./h3.^2) .* (k4.*k5.*h3 - h2.*k3.*k2))); 
        df_dR2star  = sum(m.*(sA.*(h2./h3).*(-TErep).*k1));
    else     
        % Cost function
        lambda = .1;
        f = sum((E-S).^2) + lambda*(log(sB1)^2);
        % Derivatives
        df_dA       = diff(f,sA);
        df_dR1      = diff(f,sR1);
        df_dB1      = diff(f,sB1);
        df_dR2star  = diff(f,sR2star);
    end
    
    % Second derivatives (Hessian matrix)
    H(1,1)      = diff(df_dA,sA);
    H(1,2)      = diff(df_dA,sR1);
    H(1,3)      = diff(df_dA,sB1);
    H(1,4)      = diff(df_dA,sR2star);

    H(2,1)      = diff(df_dR1,sA);
    H(2,2)      = diff(df_dR1,sR1);
    H(2,3)      = diff(df_dR1,sB1);
    H(2,4)      = diff(df_dR1,sR2star);

    H(3,1)      = diff(df_dB1,sA);
    H(3,2)      = diff(df_dB1,sR1);
    H(3,3)      = diff(df_dB1,sB1);
    H(3,4)      = diff(df_dB1,sR2star);

    H(4,1)      = diff(df_dR2star,sA);
    H(4,2)      = diff(df_dR2star,sR1);
    H(4,3)      = diff(df_dR2star,sB1);
    H(4,4)      = diff(df_dR2star,sR2star);
    
end

function [x,min_obj] = jcgsolve2(S_single,arep,TErep,TR,x0,tol)
    
    % Variable definition
    syms u_A u_R1 u_B1 u_R2star;
    
    % Forward calculations
    k1 = exp(-u_R2star.*TErep);
    h1 = u_A.*k1;
    
    k2 = sin(u_B1.*arep);
    k3 = exp(-u_R1.*TR);
    k4 = 1-k3;
    h2 = k2.*k4;
    
    k5 = cos(u_B1.*arep);
    h3 = 1-k5.*k3;
    
    % Ernst eq.
    E = h1.*h2./h3;   
        
    % Objective error function
    [f] = sum((E-S_single).^2);
    
    % Initial estimates
    u_a(1)      = x0(1);
    u_r1(1)     = x0(2);
    u_b1(1)     = x0(3);
    u_r2star(1) = x0(4);
        
    iter = 1; % Initialise counter
    
    % Derivatives
    der_analytical = true;
    if der_analytical
        [m]          = 2*(E-S_single);
        [df_da]      = sum(m.*k1.*h2./h3);
        [df_dr1]     = sum(m.*(((h1.*k3.*TR)./h3.^2) .* (k2.*h3     - h2.*k5)));
        [df_db1]     = sum(m.*(((h1.*arep)  ./h3.^2) .* (k4.*k5.*h3 - h2.*k3.*k2))); 
        [df_dr2star] = sum(m.*(u_A.*(h2./h3).*(-TErep).*k1));
    else        
        [df_da]      = diff(f,u_A);
        [df_dr1]     = diff(f,u_R1);
        [df_db1]     = diff(f,u_B1);
        [df_dr2star] = diff(f,u_R2star);
    end
        
    J = [subs(df_da,[u_A,u_R1,u_B1,u_R2star],[u_a(1),u_r1(1),u_b1(1),u_r2star(1)]) subs(df_dr1,[u_A,u_R1,u_B1,u_R2star],[u_a(1),u_r1(1),u_b1(1),u_r2star(1)]) subs(df_db1,[u_A,u_R1,u_B1,u_R2star],[u_a(1),u_r1(1),u_b1(1),u_r2star(1)]) subs(df_dr2star,[u_A,u_R1,u_B1,u_R2star],[u_a(1),u_r1(1),u_b1(1),u_r2star(1)])]; % Gradient
    S = -(J); % Search direction
    
    % Optimisation condition
    while norm(S)>tol 
        disp(num2str(iter))
%         keyboard
        I = [u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)]';
        syms h; % Step size
%         if iter==1, h0=h; end; h=h0;
        g = subs(f,[u_A,u_R1,u_B1,u_R2star],[u_a(iter)+S(1)*h, u_r1(iter)+S(2)*h, u_b1(iter)+S(3)*h, u_r2star(iter)+S(4)*h]);
        dg_dh = diff(g,h);
        h = solve(dg_dh,h); % Optimal step length
        u_a(iter+1)      = I(1)+h*S(1); % New value
        u_r1(iter+1)     = I(2)+h*S(2); % New value
        u_b1(iter+1)     = I(3)+h*S(3); % New value
        u_r2star(iter+1) = I(4)+h*S(4); % New value
        J_old = [subs(df_da,[u_A,u_R1,u_B1,u_R2star],[u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)]) subs(df_dr1,[u_A,u_R1,u_B1,u_R2star],[u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)]) subs(df_db1,[u_A,u_R1,u_B1,u_R2star],[u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)]) subs(df_dr2star,[u_A,u_R1,u_B1,u_R2star],[u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)])]; % Gradient
        iter = iter+1;
        J_new = [subs(df_da,[u_A,u_R1,u_B1,u_R2star],[u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)]) subs(df_dr1,[u_A,u_R1,u_B1,u_R2star],[u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)]) subs(df_db1,[u_A,u_R1,u_B1,u_R2star],[u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)]) subs(df_dr2star,[u_A,u_R1,u_B1,u_R2star],[u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)])]; % Updated gradient
        S = -(J_new)+((norm(J_new))^2/(norm(J_old))^2)*S; % New search direction
    end
    
    % Optimal solution
    x = [u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)];
    min_obj = subs(f,[u_A,u_R1,u_B1,u_R2star],[u_a(iter),u_r1(iter),u_b1(iter),u_r2star(iter)]);
    
    % Results table:`
    Iter                = 1:iter;
    u_A_coordinate      = u_a';
    u_R1_coordinate     = u_r1';
    u_B1_coordinate     = u_b1';
    u_R2star_coordinate = u_r2star';
    Iterations = Iter';
    T = table(Iterations,u_A_coordinate,u_R1_coordinate,u_B1_coordinate,u_R2star_coordinate);
    
    % Output:
    fprintf('Initial error: %d\n\n',subs(f,[u_A,u_R1,u_B1,u_R2star],[u_a(1),u_r1(1),u_b1(1),u_r2star(1)]));
    if (norm(S) < tol)
        fprintf('CG converged\n\n');
    end
    fprintf('Total # of iterations: %d\n\n',iter);
    fprintf('Optimal parameters: [%d,%d,%d,%d]\n\n',x(1),x(2),x(3),x(4));
    fprintf('Minimised error: %d\n\n',min_obj);
    disp(T)
end

function [x,residual,iter] = jcgsolve(A,b,tol,maxiter,verbose,x0)
    % jcgsolve.m
    %
    % Solve a symmetric positive definite system Ax = b via conjugate gradients.
    %
    % Usage: [x,res,iter] = jcgsolve(A,b,tol,maxiter,verbose,x0)
    %
    % A - Either an NxN matrix, or a function handle.
    %
    % b - N vector
    %
    % tol - Desired precision.  Algorithm terminates when 
    %    norm(Ax-b)/norm(b) < tol .
    %
    % maxiter - Maximum number of iterations.
    %
    % verbose - If 0, do not print out progress messages.
    %    If and integer greater than 0, print out progress every 'verbose' iters.
    %
    % x0 - Initial estimates
    %
    % Original code by Justin Romberg (Caltech)
    % Last edited by Julio Acosta-Cabronero

%     keyboard
    
    c           = 0;
    matrix_size = size(b);
    b           = b(:);

    if nargin < 6
        x = zeros(length(b),1);
    else 
        x = x0(:);
    end

    if nargin < 5
        verbose = 1;
    end

    implicit = isa(A,'function_handle');
        
    if nargin < 6
        r = b;
    else
        if implicit
            %r = b - A(reshape(x,matrix_size)); 
            a = A(x);
            r = b-a(:);
            r = r(:);
        else
            r = b-A*x; 
        end
    end

    % Initialise
    d       = r;
    delta   = r'*r;
    delta0  = b'*b;
    numiter = 0;
    bestx   = x;
    bestres = sqrt(delta/delta0); 

    while numiter<maxiter && delta>tol^2*delta0 
        
        if implicit
            %q = A(reshape(d,matrix_size)); 
            q = A(d);
            q = q(:);  
        else
            q = A*d;  
        end

        alpha = delta/(d'*q);
        
        x     = x+alpha*d;

        if (mod(numiter+1,50) == 0)
            if implicit
                %r = b-reshape(A(reshape(x,matrix_size)),size(b));  
                a = A(x);
                r = b-a(:);
                r = r(:);
            else
                r = b-A*x;  
            end
        else
            r = r-alpha*q;
        end

        deltaold = delta;
        delta    = r'*r;
        beta     = delta/deltaold;
        d        = r+beta*d;
        numiter  = numiter+1; 
        c        = c+1;

        if sqrt(delta/delta0)<bestres
            bestx   = x;
            bestres = sqrt(delta/delta0);
        end

        if verbose && mod(numiter,verbose)==0
            disp(sprintf('cg: Iter = %d, Best residual = %8.3e, Current residual = %8.3e', ...
              numiter, bestres, sqrt(delta/delta0)));
        end
      
    end

    if verbose
        disp(sprintf('CG stop iter: %d, residual: %8.3e', ...
                      numiter,       sqrt(delta/delta0)));
    end

%     x        = reshape(x,matrix_size);
    residual = bestres;
    iter     = numiter;

end