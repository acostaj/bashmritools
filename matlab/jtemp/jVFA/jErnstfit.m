clear all
close all

[S1,st] =jniiload('brainweb_alpha6_M');
[S2]    =jniiload('brainweb_alpha12_M');
[S3]    =jniiload('brainweb_alpha21_M');
[S4]    =jniiload('brainweb_alpha30_M');

a1=deg2rad(6);
a2=deg2rad(12);
a3=deg2rad(21);
a4=deg2rad(30);

TR=25e-3;

% R1_12 = ((1/2*TR)*(S2*a2-S1*a1))./max(S1/a1-S2/a2,eps);
% jniisave(R1_12,st,'R1_12')
 
R1_13 = ((1/2*TR)*(S3*a3-S1*a1))./max(S1/a1-S3/a3,eps);
% jniisave(R1_13,st,'R1_13')

% R1_14 = ((1/2*TR)*(S4*a4-S1*a1))./max(S1/a1-S4/a4,eps);
% jniisave(R1_14,st,'R1_14')

% A1_14 = (1./R1_14).*((S4*a4)/(2*TR))+(S4/a4);
% jniisave(A1_14,st,'A1_14')

% A2_14 = (S1.*S4*(a4/a1-a1/a4))./(S4*a4-S1*a1);
% jniisave(A2_14,st,'A2_14')

A1_13 = (1./R1_13).*((S3*a3)/(2*TR))+(S3/a3);
% jniisave(A1_13,st,'A1_13')

% A2_13 = (S1.*S3*(a3/a1-a1/a3))./(S3*a3-S1*a1);
% jniisave(A2_13,st,'A2_13')

% S3s_13 = A1_13*sin(a3).*((1-exp(-R1_13*TR))./(1-cos(a3)*exp(-R1_13*TR)));
% jniisave(S3s_13,st,'S3s_13')

% S3s_14 = A1_14*sin(a3).*((1-exp(-R1_14*TR))./(1-cos(a3)*exp(-R1_14*TR)));
% jniisave(S3s_14,st,'S3s_14')

% S3s_13b = A2_13*sin(a3).*((1-exp(-R1_13*TR))./(1-cos(a3)*exp(-R1_13*TR)));
% jniisave(S3s_13b,st,'S3s_13b')

% S3s_14b = A2_14*sin(a3).*((1-exp(-R1_14*TR))./(1-cos(a3)*exp(-R1_14*TR)));
% jniisave(S3s_14b,st,'S3s_14b')

% S4s_13 = A1_13*sin(a4).*((1-exp(-R1_13*TR))./(1-cos(a4)*exp(-R1_13*TR)));
% jniisave(S4s_13,st,'S4s_13')

% S4s_14 = A1_14*sin(a4).*((1-exp(-R1_14*TR))./(1-cos(a4)*exp(-R1_14*TR)));
% jniisave(S4s_14,st,'S4s_14')

% S4s_13b = A2_13*sin(a4).*((1-exp(-R1_13*TR))./(1-cos(a4)*exp(-R1_13*TR)));
% jniisave(S4s_13b,st,'S4s_13b')

% S4s_14b = A2_14*sin(a4).*((1-exp(-R1_14*TR))./(1-cos(a4)*exp(-R1_14*TR)));
% jniisave(S4s_14b,st,'S4s_14b')

%% Compute matrix size and initialise processing matrices
matrix = size(S1);
A        = zeros(matrix); 
R1       = zeros(matrix); 
residual = zeros(matrix);

% alpha = double([a1 a2 a3 a4]);
alpha = double([a1 a3]);

N = matrix(1);
options  = optimset('MaxIter',1e3,'MaxFunEval',2e3,'TolX',1e-3,'TolFun',1e-3,'Display','off');
addpath DylanMuir-ParforProgMon-fd6bc94
p = gcp('nocreate');
if isempty(p)
    parpool
end
ppm = ParforProgMon('Ernst Eq. fitting ',N);
tic
parfor x = 1:N
    disp(['x = ' num2str(x)]);
    a_  = zeros([matrix(2) matrix(3)]);
    r1_ = zeros([matrix(2) matrix(3)]);
    r_  = zeros([matrix(2) matrix(3)]);
    for z = 1:matrix(3)
        a  = zeros([1 matrix(2)]);
        r1 = zeros([1 matrix(2)]); 
        r  = zeros([1 matrix(2)]);
        for y = 1:matrix(2)    
%             S = double([S1(x,y,z) S2(x,y,z) S3(x,y,z) S4(x,y,z)]);
            S = double([S1(x,y,z) S3(x,y,z)]);
            [best_fit,fval] = fminsearch(@(best_fit)sum(...
                ((best_fit(1)*sin(alpha).*((1-exp(-best_fit(2)*TR))./(1-cos(alpha)*exp(-best_fit(2)*TR))))-S).^2),...
                [A1_13(x,y,z) R1_13(x,y,z)],options);
            a(y)    = best_fit(1);
            r1(y)   = best_fit(2);
            r(y)    = fval;
        end
        a_(:,z)     = a;
        r1_(:,z)    = r1;
        r_(:,z)     = r;             
    end
    A(x,:,:)        = a_;
    R1(x,:,:)       = r1_;
    residual(x,:,:) = r_;
    
    ppm.increment();
end

%% Display total processing time
toc; time_elapsed_in_minutes = toc/60

%% Save results to disk
save('results_fit02.mat')

%% Save maps as NIFTI
jniisave(A,st,'A_fit02')
jniisave(R1,st,'R1_fit02')
jniisave(residual,st,'fval_fit02')

% 01: 1234
% 02: 13