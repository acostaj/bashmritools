clear all

tic

global arep TErep TR S_single

%% Parameters

fitmode  = 2;   % [1] Solve for A, R1, B1, R2* (fminsearch)
                % [2] Solve for A, R1, B1, R2* (fmincon) 
                % [3] Solve for A, R1, B1, R2* (fminunc) 

%==========================================================================                
M0      = 3000;                     % initial magnetisation
nTE     = 8;                        % # of echoes
TE      = (2.3:2.3:2.3*nTE)*1e-3;   % in sec
T2s     = 35e-3;                    % in sec
alpha   = [3:6:27];                 % flip angle in deg
TR      = 25e-3;                    % in sec
T1      = 1.0;                      % typical whole brain avg: 1.27
B1      = .95;                      % relative B1 bias
SDnoise = 1;                        % noise sigma
%==========================================================================

B1_init = 1.0;                      % B1 initialisation

%==========================================================================
%            M0   R1      B1      R2star          fmincon
%==========================================================================
lb      = [  0,   0.15,   0.8,     2       ];      % lower bound constraints
ub      = [  1e4, 1.25,   1.2,     1/TE(1) ];      % upper bound constraints
%==========================================================================

switch fitmode
    case 1
        options  = optimset('MaxIter',1e3,'MaxFunEval',2e3,'TolX',1e-3,'TolFun',1e-3,'Display','off');
    case 2
        options  = optimoptions('fmincon','Algorithm','sqp','SpecifyObjectiveGradient',true,'Display','off','DerivativeCheck','off');
    case 3
        options  = optimoptions('fminunc','Algorithm','trust-region','SpecifyObjectiveGradient',true,'Display','off','DerivativeCheck','off');
end
        
for iter=1:500

%% Simulation

% TE inside flip angle (rad)
a = deg2rad(alpha);

arep = [];
for x = 1:length(a)
    arep = [arep repmat(a(x),[1 length(TE)])];
end

TErep = repmat(TE,[1 length(a)]);

R1  = 1/T1;
R2s = 1/T2s;

S1  = M0                 .* exp(-TErep.*R2s);
S2  = sin(B1.*arep)      .* (1-exp(-TR.*R1));
S3  = 1 - (cos(B1.*arep) .* exp(-TR.*R1));

S   = S1 .* S2 ./ S3;

% Add noise
% N   = (prctile(S,5)/SNR)*randn([1 length(S)]);
N           = SDnoise*randn([1 length(S)]);
S           = S + N;
SNR(iter,:) = [min(S/SDnoise) max(S/SDnoise)];

%% Initial estimates

T1_init = ((((S(1) / a(1))                    - (S(end-nTE+1) / a(end)) + eps) ./...
         max((S(end-nTE+1) * a(end) / 2 / TR) - (S(1) * a(1) / 2 / TR),eps)) ./ B1_init.^2);

R1_init = 1./T1_init;

A_init  = (1./R1_init) .* (S(end-nTE+1) .* (a(end)*B1_init) / 2 / TR) +...
                          (S(end-nTE+1)  ./ (a(end)*B1_init));

[~,R2star_init] = loglsqfit(S,TErep);

%% Ernst fitting 
 
S_single = S;

switch fitmode
    case 1
        [best_fit,fval] = fminsearch(@(best_fit) sum(((...
                                best_fit(1).*exp(-TErep(:).*best_fit(4)).*...
                                sin(best_fit(3).*arep(:)).*((1-exp(-best_fit(2).*TR(:)))./...
                                (1-cos(best_fit(3).*arep(:)).*exp(-best_fit(2).*TR(:)))))-...
                                S_single(:)).^2),[A_init,R1_init,B1_init,R2star_init],options);
                            
    case 2
        obj             = @ErnstCost;
        x0              = [A_init,R1_init,B1_init,R2star_init];
        A               = [];
        b               = [];
        Aeq             = [];
        beq             = [];
        nonlcon         = [];
        [best_fit,fval] = fmincon(obj,x0,A,b,Aeq,beq,lb,ub,nonlcon,options);
        
    case 3
        obj             = @ErnstCost;
        x0              = [A_init,R1_init,B1_init,R2star_init];
        [best_fit,fval] = fminunc(obj,x0,options);
end

%% Results

M0_nerror(iter)       = (100*(best_fit(1)-M0)/M0);
R1_nerror(iter)       = (100*(best_fit(2)-R1)/R1);
B1_nerror(iter)       = (100*(best_fit(3)-B1)/B1);
R2star_nerror(iter)   = (100*(best_fit(4)-R2s)/R2s);
residual(iter)        = fval/(nTE*length(a));

end

%% Print results

SNR_minmax      = round(mean(SNR))
M0_nerror       = round([mean(M0_nerror)      std(M0_nerror)])
R1_nerror       = round([mean(R1_nerror)      std(R1_nerror)])
B1_nerror       = round([mean(B1_nerror)      std(B1_nerror)])
R2star_nerror   = round([mean(R2star_nerror)  std(R2star_nerror)])
residual        =       [mean(residual)       std(residual)]
toc

%% Nested functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [S0,R2star] = loglsqfit(S,TE)
    logS    = log(S);
    O       = ones([length(TE) 1]);
    TE      = [O TE'];
    P       = TE\logS';
    R2star  = -P(2,:)';
    S0      = exp(P(1,:)');
end

function [f,df] = ErnstCost(u)

    global arep TErep TR S_single
    
    % Precomputation
    k1 = exp(-u(4)*TErep);
    h1 = u(1)*k1;
    
    k2 = sin(u(3)*arep);
    k3 = exp(-u(2)*TR);
    k4 = 1-k3;
    h2 = k2.*k4;
    
%     k5 = sqrt(1-k2.^2); % 
    k5 = cos(u(3)*arep);
    h3 = 1-k5.*k3;
        
    g = h1.*h2./h3;
    
    m = 2*(g-S_single);
    
    % Objective
    f = sum((g-S_single).^2);
        
    % Explicit derivatives
    if nargout>1
        df_u1 = sum(m.*k1.*h2./h3);
        df_u2 = sum(m.*(((h1.*k3.*TR)./h3.^2) .* (k2.*h3     - h2.*k5)));
        df_u3 = sum(m.*(((h1.*arep)  ./h3.^2) .* (k4.*k5.*h3 - h2.*k3.*k2))); 
        df_u4 = sum(m.*(u(1)*(h2./h3).*(-TErep).*k1));
        df    = [df_u1; df_u2; df_u3; df_u4];
    end
end
