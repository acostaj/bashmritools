clear all

tic

te      = [2.3e-3 10e-3];           % in sec
alpha   = deg2rad([6 21]);          % flip angle in deg
tr      = 25e-3;                    % in sec

syms A R1 B1 R2star ALPHA TE TR S

% Precalculations
k1 = exp(-R2star*TE);
h1 = A*k1;

k2 = sin(B1*ALPHA);
k3 = exp(-R1*TR);
k4 = 1-k3;
h2 = k2*k4;

k5 = cos(B1*ALPHA);
h3 = 1-k5*k3;

% Ernst eq.
E = h1*h2/h3;

% Error function
f = sum((E-S).^2); % + 1e9*log(B1)^2;

% First derivatives
df_dA       = diff(f,A);
df_dR1      = diff(f,R1);
df_dB1      = diff(f,B1);
df_dR2star  = diff(f,R2star);

% Second derivatives (Hessian matrix)
H(1,1)      = diff(df_dA,A);
H(1,2)      = diff(df_dA,R1);
H(1,3)      = diff(df_dA,B1);
H(1,4)      = diff(df_dA,R2star);

H(2,1)      = diff(df_dR1,A);
H(2,2)      = diff(df_dR1,R1);
H(2,3)      = diff(df_dR1,B1);
H(2,4)      = diff(df_dR1,R2star);

H(3,1)      = diff(df_dB1,A);
H(3,2)      = diff(df_dB1,R1);
H(3,3)      = diff(df_dB1,B1);
H(3,4)      = diff(df_dB1,R2star);

H(4,1)      = diff(df_dR2star,A);
H(4,2)      = diff(df_dR2star,R1);
H(4,3)      = diff(df_dR2star,B1);
H(4,4)      = diff(df_dR2star,R2star);

%%
a       = 3000;                     % initial magnetisation
r2s     = 1/35e-3;                  % in sec
r1      = 1/1.5;                    % typical whole brain avg: 1/1.27
b1      = 1.0;                      % relative B1 bias

s            = vpa(subs(E,{A,R1,B1,R2star,ALPHA,TE,TR},{a,r1,b1,r2s,alpha,te,tr}));
h            = vpa(subs(H,{A,R1,B1,R2star,ALPHA,TE,TR,S},{a,r1,b1,r2s,alpha,te,tr,s}));

return

%%
% a       = 3000;                     % initial magnetisation
% te      = 2.3e-3;                   % in sec
% r2s     = 1/35e-3;                  % in sec
% alpha   = deg2rad(3);               % flip angle in deg
% tr      = 25e-3;                    % in sec
% r1      = 1/1.5;                    % typical whole brain avg: 1/1.27
% b1      = 1.0;                      % relative B1 bias
% SDnoise = 0;

tr      = 25e-3;
N       = 2;
c       = 0;
SDnoise = 0;
SNR     = zeros([1 N^6]);
eig_h   = zeros([4 N^6]);

for a       = linspace(2000,4000,N)                  
for te      = linspace(2.3e-3,18.4e-3,N)                
for r2s     = linspace(20,100,N)
for alpha   = linspace(6,27,N)
for r1      = linspace(0.6,1.1,N)
for b1      = linspace(0.9,1.1,N)
% for SDnoise = linspace(1,10,N)

    c=c+1;

s            = vpa(subs(E,{A,R1,B1,R2star,ALPHA,TE,TR},{a,r1,b1,r2s,alpha,te,tr}));
s            = abs(s+SDnoise*randn(1));
SNR(c)       = s/SDnoise;
h            = vpa(subs(H,{A,R1,B1,R2star,ALPHA,TE,TR,S},{a,r1,b1,r2s,alpha,te,tr,s}));
diag_offset  = min(abs([h(1,1),h(2,2),h(3,3),h(4,4)]))*1e-7; %5e4;
for x=1:4
    h(x,x) = h(x,x)+diag_offset;
end
eig_h(:,c)   = eig(h);
% eig(h)

% end
end; end; end; end; end; end

y = [5,50,95];
prctile(SNR,y)
for x=1:4
    prctile(eig_h(x,:),y)
end

toc