clear all

syms A R2star TE S

% Precalculations
k1 = exp(-R2star.*TE);
h1 = A.*k1;

% Ernst eq.
E = h1;

% Error function
f = sum((E-S).^2);

% First derivatives
df_dA       = diff(f,A);
df_dR2star  = diff(f,R2star);

% Second derivatives (Hessian matrix)
H(1,1)      = diff(df_dA,A);
H(1,2)      = diff(df_dA,R2star);

H(2,1)      = diff(df_dR2star,A);
H(2,2)      = diff(df_dR2star,R2star);

%%
% a       = 3000;                     % initial magnetisation
% te      = 2.3e-3;                   % in sec
% r2s     = 1/35e-3;                  % in sec

% for a       = 100:10:1000;                     % initial magnetisation
% for te      = (2.3:2.3:25)*1e-3;                   % in sec
% for r2s     = (1/100:1/200:1/15)*1e-3;                  % in sec

a       = linspace(100,1000,10);                     % initial magnetisation
te      = linspace(2.3e-3,25e-3,10);                   % in sec
r2s     = linspace(10,100,10);                  % in sec

    
% SDnoise = 0;

s       = vpa(subs(E,{A,R2star,TE},{a,r2s,te}));
% s       = s + SDnoise*randn(1);
% SNR     = s/SDnoise
h       = vpa(subs(H,{A,R2star,TE,S},{a,r2s,te,s}));
eig_h   = eig(h)

% end; end; end