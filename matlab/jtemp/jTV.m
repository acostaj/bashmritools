function x = jTV(data,lambda)

% Data properties
res = [.8 .8 .8]; % in mm
matrix_size = size(data);

% Optimiser parameters
% lambda                     = 4;
                           % Lagrange multiplier            
e                          = 1e-6;
                           % L1-smoothing parameter
cg_max_iter                = 2000;
                           % Max iter # for internal conjugate gradient loop
cg_tol                     = 1e-1;
                           % Stopping tolerance for CG loop (default: .1)
max_iter                   = 1000;
                           % Max iter # for outer quasi-Newton loop
tol_norm_ratio             = 1e-1;
                           % Stopping tolerance for outer qN loop

% Operator definitions
grad = @fgrad;
div  = @bdiv;

% Initialise optimiser
disp('Initialise TV optimisation')
iter                    = 0;
x                       = zeros(matrix_size);
dx                      = zeros(matrix_size);
res_norm_ratio          = Inf;
res_norm_ratio_history  = zeros(1,max_iter);
cost_fid_history        = zeros(1,max_iter);
cost_reg_history        = zeros(1,max_iter);
tic

refsize=40;
HR=length(data);
cy=HR/2+1;
padlength=(HR-refsize)/2;

while res_norm_ratio>tol_norm_ratio && iter<max_iter
        
    iter=iter+1;
    
    % conjugate gradient computation
    Vr          = 1./sqrt(abs(grad(real(x),res)).^2+e);
    
    reg         = @(dx) div(Vr.*(grad(real(dx),res)),res);
    fidelity    = @(dx) 2*lambda*real(dx);
    A           = @(dx) reg(dx)+fidelity(dx);
    
    b           = reg(x) + 2*lambda*real(x-data);

%     t1          = fftshift(fft(dx));
%     t2          = t1(cy-refsize/2:cy+refsize/2-1);
%     t3          = padarray(t2,[padlength 0]);
%     dt4         = abs(ifft(t3));
% 
%     reg         = @(dt4) div(Vr.*(grad(real(dt4),res)),res);
%     fidelity    = @(dt4) 2*lambda*real(dt4);
%     A           = @(dt4) reg(dt4)+fidelity(dt4);
% 
%     t1          = fftshift(fft(x));
%     t2          = t1(cy-refsize/2:cy+refsize/2-1);
%     t3          = padarray(t2,[padlength 0]);
%     t4          = abs(ifft(t3));
%     b           = reg(t4) + 2*lambda*real(t4-data);
   
    dx          = real(jcgsolve(A,-b,cg_tol,cg_max_iter,0));
    
    % relative update size 
    res_norm_ratio = norm(dx(:))/norm(x(:));
        
    % update solution
    x           = x+dx;
    
    % save intermediate solution
    SaveIter = false;
    if SaveIter
        save('x.mat','x');
        figure(932)
        title(['Iter #' num2str(iter)])
        plot(1:length(data),data,'k-')
        hold on 
        plot(1:length(data),x,'b-')       
    end
        
    % residual matrix
    wres        = x - data;

    % calculate costs & archive history
    cost_fid                        = norm(wres(:),2);
    cost_fid_history(iter)          = cost_fid;
    cost_reg                        = abs(grad(x));
    cost_reg                        = sum(cost_reg(:));
    cost_reg_history(iter)          = cost_reg;
    res_norm_ratio_history(iter)    = res_norm_ratio;
    
    fprintf('TV qNiter #%d; update step: %8.4f; consistency cost: %8.4f; regularisation cost: %8.4f\n',...
             iter,res_norm_ratio,cost_fid,cost_reg);
    toc %
end

function Gx = fgrad(x,res)
    % Discrete Gradient Using Forward Differences
    % with the Neuman Boundary Condition
    % 
    % References:
    % [1] Chambolle, An Algorithm for Total Variation Minimization and
    % Applications, JMIV 2004
    % [2] Pock et al., Global Solutions of Variational Models with Convex
    % Regularization, SIIMS 2010

    if (nargin < 2)
        res = [1 1 1]; % in mm
    end

    Dx = [x(2:end,:,:); x(end,:,:)] - x;
    if size(x,2)>=2
        Dy = [x(:,2:end,:), x(:,end,:)] - x;
    end
    if size(x,3)>=2    
        Dz = cat(3, x(:,:,2:end), x(:,:,end)) - x;
    end
        
    Dx = Dx/res(1);
    if size(x,2)>=2
        Dy = Dy/res(2);
    end
    if size(x,3)>=2    
        Dz = Dz/res(3);
    end

    if size(x,2)>=2
        Gx = cat(4, Dx, Dy);
    elseif size(x,3)>=2
        Gx = cat(4, Dx, Dy, Dz);
    else
        Gx = Dx;
    end
end

function div = bdiv(Gx,res)
    % Discrete Divergence Using Backward Difference
    % with the Dirichlet Boundary Condition
    % 
    % References:
    % [1] Chambolle, An Algorithm for Total Variation Minimization and
    % Applications, JMIV 2004

    if (nargin < 2)
        res = [1 1 1]; % in mm
    end

    Gx_x = Gx(:,:,:,1);
    if size(Gx,4)>=2
        Gx_y = Gx(:,:,:,2);
    end
    if size(Gx,4)>=3
        Gx_z = Gx(:,:,:,3);
    end

    [Mx, My, Mz] = size(Gx_x);

    Dx = [Gx_x(1:end-1,:,:); zeros(1,My,Mz)]...
        - [zeros(1,My,Mz); Gx_x(1:end-1,:,:)];

    if size(Gx,4)>=2
        Dy = [Gx_y(:,1:end-1,:), zeros(Mx,1,Mz)]...
            - [zeros(Mx,1,Mz), Gx_y(:,1:end-1,:)];
    end
    if size(Gx,4)>=3    
        Dz = cat(3, Gx_z(:,:,1:end-1), zeros(Mx,My,1))...
            - cat(3, zeros(Mx,My,1), Gx_z(:,:,1:end-1));
    end
        
    Dx = Dx/res(1);
    if size(Gx,4)>=2
        Dy = Dy/res(2);
    else
        Dy = 0;
    end
    if size(Gx,4)>=3    
        Dz = Dz/res(3);
    else
        Dz = 0;
    end
        
    div = -( Dx + Dy + Dz );
end

function [x,residual,iter] = jcgsolve(A,b,tol,maxiter,verbose,x0)
    % jcgsolve.m
    %
    % Solve a symmetric positive definite system Ax = b via conjugate gradients.
    %
    % Usage: [x,res,iter] = jcgsolve(A,b,tol,maxiter,verbose,x0)
    %
    % A - Either an NxN matrix, or a function handle.
    %
    % b - N vector
    %
    % tol - Desired precision.  Algorithm terminates when 
    %    norm(Ax-b)/norm(b) < tol .
    %
    % maxiter - Maximum number of iterations.
    %
    % verbose - If 0, do not print out progress messages.
    %    If and integer greater than 0, print out progress every 'verbose' iters.
    %
    % x0 - initial solution
    %
    % Adapted from original code by Justin Romberg
    % Email: jrom@acm.caltech.edu
    % Created: October 2005

    c=0; % JAC
    matrix_size=size(b);
    b=b(:);

    if (nargin < 6)
        x = zeros(length(b),1);
    else 
        x=x0(:);
    end

    if (nargin < 5), verbose = 1; end

    implicit = isa(A,'function_handle');

    if (nargin < 6)
        r = b;
    else
        if (implicit), r = b - A(reshape(x,matrix_size)); r=r(:);  
        else r = b - A*x;  end
    end

    d = r;
    delta = r'*r;
    delta0 = b'*b;
    numiter = 0;
    bestx = x;
    bestres = sqrt(delta/delta0); 

    while numiter<maxiter && delta>tol^2*delta0 
      if implicit; q = A(reshape(d,matrix_size)); q=q(:);  
      else q = A*d;  end

      alpha = delta/(d'*q);
      x = x + alpha*d;

      if (mod(numiter+1,50) == 0)
        if (implicit), r = b - reshape(A(reshape(x,matrix_size)),size(b));  
        else r = b - A*x;  end
      else
        r = r - alpha*q;
      end

      deltaold = delta;
      delta = r'*r;
      beta = delta/deltaold;
      d = r + beta*d;
      numiter = numiter + 1; c=c+1; % JAC

      if sqrt(delta/delta0)<bestres
        bestx = x;
        bestres = sqrt(delta/delta0);
      end

      verbose=0; % JAC
      if verbose && mod(numiter,verbose)==0
        disp(sprintf('cg: Iter = %d, Best residual = %8.3e, Current residual = %8.3e', ...
          numiter, bestres, sqrt(delta/delta0)));
      end

    end

    verbose=1; % JAC
    if verbose
        disp(sprintf('CG stop iter: %d, residual: %8.3e', ...
                      numiter,       sqrt(delta/delta0)));
    end

    x = reshape(x,matrix_size);
    residual = bestres;
    iter = numiter;
end

end % jTV