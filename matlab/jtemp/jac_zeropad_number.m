function [ padded_number ] = jac_zeropad_number( positive_number, N_char )
% DESCRIPTION
%  Prepends zeroes to a given positive number so that the total number of
%  character is N_char
%
% SINTAX
%  [ padded_number ] = jac_zeropad_number( positive_number, N_char )
%
% > Created by Julio Acosta-Cabronero (03/12/14)

eval([ 'padded_number = sprintf(''%0'...
       num2str(N_char)...
       'd'', positive_number);' ])
