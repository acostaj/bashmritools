% clear all; close all
for x=1:10; clear all; close all; end

procmode    = 2;
%             [1] simulation: normal, permutation

tic

switch procmode

    case 2
        np      = 1e3; % # of data points
        nref    = 4;
%         offset  = 4*[-2 -2 -2 2];
%         SD      = [1.1 .8 1.2 .9];
        offset  = [0 0 0 0];
        SD      = [1 1 1 1];
        nrep    = 2e3;
        nrm     = 1;
        
        lpct    = linspace(1/np,50,50);
        lofs    = linspace(eps,1,50);
        
        dispfig = false;
        
        ksflag  = false;
        nperm   = 2;
        
        for x=1:nref
            ref(x,:) = offset(x) + SD(x)*randn(1,np);
        end

        P1_all = zeros([nrep,length(lpct),length(lofs)]);
        P2_all = zeros([nrep,length(lpct),length(lofs)]);
        if ksflag
            P3_all = zeros([nrep,length(lpct),length(lofs)]);
        end
        P1_cum = ones([length(lpct),length(lofs)]);
        P2_cum = ones([length(lpct),length(lofs)]);
        PR_P1  = zeros([length(lpct),length(lofs)]);
        PR_P2  = zeros([length(lpct),length(lofs)]);
        
        for y=1:nrep     
            rep = 1:y;   
            les = datasample(ref(:),nrm*np,'Replace',true);
            les_orig = les;
            
            counter_pct = 0;
            for pct = lpct
                counter_pct=counter_pct+1;

                counter_ofs = 0;                
                for ofs = lofs
                    counter_ofs=counter_ofs+1;

                        les_pct(y,counter_pct,counter_ofs) = pct;
                        les_ofs(y,counter_pct,counter_ofs) = ofs*std(ref(:));

                        nlesvx      = round((pct/100)*nrm*np);
                        idx         = randsample(nrm*np,nlesvx); 
                        les         = les_orig;
                        les(idx)    = les(idx)+les_ofs(y,counter_pct,counter_ofs); 

                        idx2     = randsample(nref*np,nrm*np);
                        refsamp  = ref(idx2);

                        if dispfig
                            figure(1)
                            subplot 121
                            hist(refsamp,50)
                            axis([-4 4 0 nrm*100])
                            subplot 122
                            hist(les,50)
                            axis([-4 4 0 nrm*100])
                        end

                        [~,P1,~] = ttest2(refsamp,les);
                        [P2,~,~] = ranksum(refsamp,les);

                        if ksflag
                            all = [refsamp les];
                            for x = 1:nperm
                                idx = randsample(2*nrm*np,2*nrm*np);
                                g1 = all(idx(1:nrm*np));
                                g2 = all(idx(nrm*np+1:end));
                                [~,~,kss] = kstest2(g1,g2);
                                KSstat(x) = kss;
                            end

                            [~,~,true_kss]                  = kstest2(refsamp,les);
                            ranking                         = 1 + length(find(KSstat>true_kss));
                            R(y,counter_pct,counter_ofs)    = ranking;
                            P3                              = ranking/(nperm+1);
                        end

                        P1_all(y,counter_pct,counter_ofs) = P1;
                        P2_all(y,counter_pct,counter_ofs) = P2;
                        if ksflag
                            P3_all(y,counter_pct,counter_ofs) = P3;
                        end

                        dispflag = false;

                        if ~mod(y,50)
                            P1_cum(counter_pct,counter_ofs) = exp(-median(-log(P1_all(rep,counter_pct,counter_ofs))));
                            P2_cum(counter_pct,counter_ofs) = exp(-median(-log(P2_all(rep,counter_pct,counter_ofs))));

                            P_rep = P1_all(rep,counter_pct,counter_ofs);
                            PR_P1(counter_pct,counter_ofs) = 100*(length(find(P_rep<0.05))/y);
                            
                            P_rep = P2_all(rep,counter_pct,counter_ofs);
                            PR_P2(counter_pct,counter_ofs) = 100*(length(find(P_rep<0.05))/y);

                            if ksflag
                                P3_cum_a(counter_pct,counter_ofs) = exp(-median(-log(P3_all(rep,counter_pct,counter_ofs))));
                                P3_cum_b(counter_pct,counter_ofs) = sum(R(:,counter_pct,counter_ofs))/(y*(nperm+1));
                                fprintf('%d.   %.4f   %.4f   %.4f   %.4f\n',...
                                    y,P1_cum(counter_pct,counter_ofs),P2_cum(counter_pct,counter_ofs),P3_cum_a(counter_pct,counter_ofs),P3_cum_b(counter_pct,counter_ofs))
                            else
                                fprintf('#%d.   P_parametric = %.4f   P_nonparametric = %.4f  |  PR_parametric = %.1f   PR_nonparametric = %.1f\n',...
                                    y,P1_cum(counter_pct,counter_ofs),P2_cum(counter_pct,counter_ofs),PR_P1(counter_pct,counter_ofs),PR_P2(counter_pct,counter_ofs))
                                dispflag = true;
                            end   

                            if pct==lpct(end) && ofs==lofs(end) %y==nrep
                                figure(2)
                                subplot 221
                                imagesc(1-P1_cum,[.95,.95+1e-6])
                                ylabel('Lesion %')
                                title(['Permutation #' num2str(y) ' /' num2str(nrep)])

                                subplot 222
                                imagesc(1-P2_cum,[.95,.95+1e-6])

                                subplot 223
                                imagesc(PR_P1,[5,5+1e-3])
                                xlabel('Lesion Offset')
                                ylabel('Lesion %')

                                subplot 224
                                imagesc(PR_P2,[5,5+1e-3])
                                xlabel('Lesion Offset')

                                drawnow
                            end                    
                        end
                    
                end %ofs
            end %pct
        end %y
               
        lpct
        lofs
        lofs_abs = lofs*std(ref(:))

%         figure(4)
%         subplot 121
%         hist(refsamp,round(np/10))
%         subplot 122
%         hist(les,round(np/10))

        return
        
        PR_P1 = 100*(length(find(P1_all<0.05))/nrep);
        PR_P2 = 100*(length(find(P2_all<0.05))/nrep);

        disp(' ')
        if ksflag
            PR_P3 = 100*(length(find(P3_all<0.05))/nrep);
            fprintf('Positive rate.   %.1f   %.1f   %.1f\n',PR_P1,PR_P2,PR_P3)
        else
            fprintf('Positive rate.   parametric = %.1f   nonparametric = %.1f\n',PR_P1,PR_P2)
        end
        disp(' ')
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 1

        np      = 1000; % # of data points
        offset  = 0;
        nouter  = 1000;
        nrep    = 4;
        nperm   = 1000;
        SD      = .01;

        P_param_cum_outer     = zeros([1,nouter]);
        P_nonparam_cum_outer  = zeros([1,nouter]);

        for z = 1:nouter

            P_param         = zeros([1,nrep]);
            P_nonparam      = zeros([1,nrep]);
            P_param_cum     = zeros([1,nrep]);
            P_nonparam_cum  = zeros([1,nrep]);
            ranking         = zeros([1,nrep]);

            for y = 1:nrep

                ref     = SD*randn(1,np);
                l1      = length(ref);

                rep     = 1:y;    
                les     = SD*randn(1,np) + offset;
                l2      = length(les);

                [~,pp,~] = ttest2(ref,les);
                P_param(y) = pp;
                P_param_cum(y) = exp(-mean(-log(P_param(rep))));

                all     = [ref les];
                KSstat  = zeros([1,nperm]);

                parfor x = 1:nperm
                    g1 = datasample(all,l1,'Replace',true);
                    g2 = datasample(all,l2,'Replace',true);
                    [~,~,kss] = kstest2(g1,g2);
                    KSstat(x) = kss;
                end

                [~,~,true_kss]      = kstest2(ref,les);
                ranking(y)          = 1 + length(find(KSstat>true_kss));
                P_nonparam(y)       = ranking(y)/(nperm+1);
                P_nonparam_cum(y)   = sum(ranking(rep))/(y*(nperm+1));

                dispfig = false;
                if dispfig
                    figure(12)
                    plot(rep,-log(P_nonparam_cum(rep)),'bo-')
                    hold on
                    plot(rep,-log(P_param_cum(rep)),'rs-')
                    if y==1
                        grid on
                        xlabel('Repetition')
                        ylabel('-log P')
                    end
                end

                fprintf('%d.   %.4f   %.4f   %.4f   %.4f\n',y,P_nonparam_cum(y),P_param_cum(y),P_nonparam(y),P_param(y))
            end

            std_logP_nonparam  = std(-log(P_nonparam));
            std_logP_param     = std(-log(P_param));

            PR_nonparam       = 100*(length(find(P_nonparam<0.05))/nrep);
            PR_param          = 100*(length(find(P_param<0.05))/nrep);

            disp(' ')
            fprintf('+++%d.   %.2f   %.2f   %.0f   %.0f\n',z,std_logP_nonparam,std_logP_param,PR_nonparam,PR_param)
            disp(' ')

            P_nonparam_cum_outer(z) = P_nonparam_cum(end);
            P_param_cum_outer(z)    = P_param_cum(end);
        end

        mean_outer_P_nonparam    = exp(-mean(-log(P_nonparam_cum_outer)));
        mean_outer_P_param       = exp(-mean(-log(P_param_cum_outer)));

        std_outer_logP_nonparam  = std(-log(P_nonparam_cum_outer));
        std_outer_logP_param     = std(-log(P_param_cum_outer));

        fprintf('FINAL.   %.4f   %.4f   %.4f   %.4f\n',mean_outer_P_nonparam,mean_outer_P_param,std_outer_logP_nonparam,std_outer_logP_param)
        disp(' ')
    
end
        
toc

