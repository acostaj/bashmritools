clear all
close all

image_fileroot='median_sub';
s=20;

[ima st] = jniiload(image_fileroot);

for ax=1:3
    switch ax
        case 1
            ima = permute(ima,[2 3 1]);
        case 2
            ima = permute(ima,[1 3 2]);
    end

    S=size(ima,3);

    t=S-s+1;

    clear P
    
    for x=1:t
        P(:,:,x)=max(ima(:,:,x:x+s-1),[],3);
    end

    Spad=S-t;

    Spadup=round(Spad/2);

    mIP=zeros(size(ima));

    mIP(:,:,Spadup+1:Spadup+t)=P;

    switch ax
        case 1
            ima = permute(ima,[3 1 2]);
            mIP = permute(mIP,[3 1 2]);
        case 2
            ima = permute(ima,[1 3 2]);
            mIP = permute(mIP,[1 3 2]);
    end

    jniisave( mIP, st, ['maxIP' num2str(ax) '_' num2str(s) 'slices_' image_fileroot] );
end