function m1_3Dgrad(froot)

[phase,st]=jniiload(froot);

[wGx,wGy,wGz]=imgradientxyz(phase,'central');

awG = sqrt(1/3*(wGx.^2 + wGy.^2 + wGz.^2));

jniisave(awG,st,['grad_' froot])
