function jac_TIV_adjustment( text_file, n_CN, n_PAT, output_fileroot )
% Normalise for differences in total intracranial volume using an analysis
% of covariance approach, where the measured areas are adjusted by an 
% amount proportional to the difference between each individual?s observed 
% total intracranial volume and the global total intracranial volume mean 
% for all control subjects*.
% 
% * Jack CR, Jr., Twomey CK, Zinsmeister AR, Sharbrough FW, Petersen RC, 
%   et al. (1989) Anterior temporal lobes and hippocampal formations: 
%   normative volumetric measurements from MR images in young adults. 
%   Radiology 172: 549?554.
%
% function jac_TIV_adjustment( text_file, n_CN, n_PAT, output_fileroot )
%
% ARGUMENTS
%  text_file: input text filename containing volumes. This single-column 
%             text file must be sorted as follows: [ROI#1 CN] [ROI#1 PAT]..
%             ..[ROI#N CN] [ROI#N PAT] [TIV CN] [TIV PAT]
%  n_CN:      Number of control subjects
%  n_PAT:     Number of patients
%  output_fileroot
%
% The plots illustrate the procedure: crosses denote raw data, whereas 
% circles denote adjusted values.
%
% Created by Julio Acosta-Cabronero, 03/12/14

close all

n_TOTAL = n_CN+n_PAT;
eval([ 'ALL=load(''' text_file ''');' ])
n_ROI = length(ALL)/n_TOTAL;

ro_STRUCT_VOL = reshape(ALL,[n_TOTAL n_ROI]);

for ROI = 1:n_ROI

    V_CN  = ro_STRUCT_VOL( 1:n_CN, [ ROI n_ROI] );
    V_PAT = ro_STRUCT_VOL( n_CN+1:end, [ ROI n_ROI ] );

    figure
    plot( V_CN( :, 2 ), V_CN( :, 1 ), 'bx' )
    hold on
    plot( V_PAT( :, 2 ), V_PAT( :, 1 ), 'rx' )

    P = polyfit( V_CN( :, 2 ), V_CN( :, 1 ), 1 );

    [ r, p_corr ] = corr( V_CN( :, 2 ), V_CN( :, 1 ) );
    p_CORR( ROI ) = p_corr;

    V_CN_synth = P( 1 ) .* V_CN( :, 2 ) + P( 2 );

    hold on
    plot( V_CN( :, 2 ), V_CN_synth, 'k-' )

    mean_TROIV_CN = mean( V_CN( :, 2 ) );
    diff_TROIV_CN = mean_TROIV_CN - V_CN( :, 2 );

    V_CN_adj = V_CN( :, 1 ) + ( diff_TROIV_CN .* P( 1 ) );
    V_CN_adj_2 = V_CN( :, 1 ) ./ V_CN( :, 2 );

    V_CN_ADJ( ROI, : ) = V_CN_adj;

    hold on
    plot( V_CN( :, 2 ), V_CN_adj, 'bo' )

    P_adj = polyfit( V_CN( :, 2 ), V_CN_adj, 1 );

    V_CN_adj_synth = P_adj( 1 ) .* V_CN( :, 2 ) + P_adj( 2 );

    hold on
    plot( V_CN( :, 2 ), V_CN_adj_synth, 'k-.' )

    mean_TROIV_PAT = mean( V_PAT( :, 2 ) );
    diff_TROIV_PAT = mean_TROIV_CN - V_PAT( :, 2 );

    V_PAT_adj = V_PAT( :, 1 ) + ( diff_TROIV_PAT .* P( 1 ) );
    V_PAT_adj_2 = V_PAT( :, 1 ) ./ V_PAT( :, 2 );
    V_PAT_ADJ( ROI, : ) = V_PAT_adj;

    hold on
    plot( V_PAT( :, 2 ), V_PAT_adj, 'ro' )

    [p h s] = ranksum( V_PAT_adj, V_CN_adj );
    p_RANK( 1, ROI ) = p;
    RSUM( 1, ROI ) = s.ranksum;

%     [p h s] = ranksum( V_CN_adj_2, V_PAT_adj_2 );
%     p_RANK( 2, ROI ) = p;
%     RSUM( 2, ROI ) = s.ranksum;
% 
%     [p h s] = ranksum( V_CN( :, 1 ), V_PAT( :, 1 ));
%     p_RANK( 3, ROI ) = p;
%     RSUM( 3, ROI ) = s.ranksum;
    V_all_adj = [V_CN_adj(:); V_PAT_adj(:)];
    zpROI = jac_zeropad_number(ROI,2);
    eval([ 'save(''V_all_adj_' zpROI '.txt'',''V_all_adj'',''-ascii'')' ])
end

disp(' '); disp([ 'CN correlation p-value - TROIV vs. TIV'])
p_CORR(1:end-1)

disp([ 'PAT vs. CN (adjusted) p-values' ])
p_RANK(1, 1:end-1)
% p_RANK(2, 1:end-1)
% p_RANK(3, 1:end-1)

ro_STRUCT_VOL_ADJ = [ V_CN_ADJ V_PAT_ADJ]';
csvwrite( [output_fileroot '.csv'], ro_STRUCT_VOL_ADJ)


%% SUBFUNCTION

function [ padded_number ] = jac_zeropad_number( positive_number, N_char )
% DESCRIPTION
%  Prepends zeroes to a given positive number so that the total number of
%  character is N_char
%
% SINTAX
%  [ padded_number ] = jac_zeropad_number( positive_number, N_char )

eval([ 'padded_number = sprintf(''%0'...
       num2str(N_char)...
       'd'', positive_number);' ])
end

end 
