function jROIextract(data_root,ROI_root)
% ROI EXTRACTION PROGRAM
% 
%  This program extracts data from a 4D NIFTI file (data_root)
% 
%  Regions of interest must be provided (ROI_root)
%
%  Created by Julio Acosta-Cabronero

tic

%%
% Output
csv_root  = [ROI_root '_FROM_' data_root];

%%
disp('>>> Load data')

addpath ~/jtools/QSMbox/master/ptb/_Utils
addpath ~/jtools/QSMbox/master/ptb/_spm12b_nifti

[data,st]       = jniiload(data_root);
[data_medROI]   = zeros(size(data));
[data_iqrROI]   = zeros(size(data));
[ROI]           = jniiload(ROI_root);

%%
disp('>>> ROI extraction')
minROI = min(ROI(ROI~=0));
maxROI = max(ROI(:));

c = 0;
% Loop across min-max ROI value range
for x = minROI:maxROI
    idx = find(ROI==x);
    % Check if ROI exits
    if (idx)
        disp(['ROI: ' num2str(x)])
        c = c+1;
        % Loop across subjects
        for y = 1:size(data,4)
            data0 = data(:,:,:,y);
            if y == 1
                mat(1,c,1:6) = x;
            end
            mat(y+1,c,1) = mean(data0(idx));
            mat(y+1,c,2) = median(data0(idx));
            mat(y+1,c,3) = std(data0(idx));
            mat(y+1,c,4) = iqr(data0(idx));
            mat(y+1,c,5) = prctile(data0(idx),25);
            mat(y+1,c,6) = prctile(data0(idx),75);
            
            ttt = data_medROI(:,:,:,y);
            ttt(idx) = median(data0(idx));
            data_medROI(:,:,:,y) = ttt;
            
            ttt = data_iqrROI(:,:,:,y);
            ttt(idx) = iqr(data0(idx));
            data_iqrROI(:,:,:,y) = ttt;
        end
    end
end

%%
disp('>>> Save spreadsheets')
csvwrite([csv_root '_MEAN.csv'],mat(:,:,1));
csvwrite([csv_root '_MEDIAN.csv'],mat(:,:,2));
csvwrite([csv_root '_STD.csv'],mat(:,:,3));
csvwrite([csv_root '_IQR.csv'],mat(:,:,4));
csvwrite([csv_root '_25PRCTILE.csv'],mat(:,:,5));
csvwrite([csv_root '_75PRCTILE.csv'],mat(:,:,6));

%%
disp('>>> Save images')
for y = 1:size(data,4)
    jniisave(data_medROI(:,:,:,y),st,['med_' csv_root '_' num2str(y)])
    jniisave(data_iqrROI(:,:,:,y),st,['iqr_' csv_root '_' num2str(y)])
end

%%
toc
