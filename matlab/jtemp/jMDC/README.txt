Software skeleton for minimum detectable change (MDC) calculation
=================================================================


To calculate MDC values you need:

1.- A 4D NIFTI data file, where the 4th dim represents repetitions of the same quantitative measurement (ideally using data acquired on different days and at different times of day)

2.- A 3D labelled atlas file

3.- Set up https://gitlab.com/acostaj/bashmritools


If you have those already, go ahead and edit/run jmcstatssimpar2_wrapper_MASTER.m in pwd. 

Please note there're some user params too in jmcstatssimpar2_SLAVE.m. Nb. It is recommended to turn FOC off if you are calculating MDCs from real-valued data (e.g. QSM).


Regarding (2), you may want to use generic atlases, e.g. those included in FSL (https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Atlases), mricron (https://people.cas.sc.edu/rorden/mricro/template.html), or those included in SPM-MarsBaR (http://marsbar.sourceforge.net/). Mindboggle (https://mindboggle.info/data.html) is also a great resource.


You will need to co-register the native template matching the atlas space to your study-wise space. 

In 02_MNI, find a couple sample scripts to carry out this registration step, apply the transform to some atlases and clean up the resulting labels. Find as well some examples of how to generate your own atlas (see 03_import.sh and 04_cleanup_ROIs.sh). 

Nb. Where possible/meaningful labels must be constraint to grey or white matter (aided by e.g. SPM probablity segments). As a result of registration errors and this intersection, however, some voxels won't be labelled. Please note in 04_cleanup_ROIs.sh you have a few calls to jsegmentextend.m. This Matlab program will fill in the voids for you doing a local (spherical) search.


If you wish to extract summary stats from ROIs and dump them to spreadsheets, please take a look at jROIextract.m.


For advice, please contact Julio Acosta-Cabronero at jac@cantab.net.
