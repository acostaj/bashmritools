function [MDC_neg_approx,MDC_pos_approx] = jmcstatssimpar2(ref,nref)
    
%% Set variables

nrep        = 100;  % # of repetitions. This builds robustness at the 
                    % expense of computing time. Default=100.
                    
FOC         = true; % Lesion fold-over control. FOC refers to the adjustment
                    % carried out to prevent (negative) lesion offsets from 
                    % yielding negative intensity values. If FOC=true, this 
                    % program will force negative values to be positive 
                    % (within a noise range estimate informed by ref's STD). 
                    % Default=true.
                    
P2_cum_thr  = .05;  % Threshold level used to estimate the critical lesion 
                    % interface. Default=0.05.
                    
prctile_thr = 5;    % Percentile used to calculate the robust minimum 
                    % detectable change (rMDC) at the lesion interface.
                    % Default=5.

%% Preamble

mean_ref = mean(ref(:));
std_ref = std(ref(:));
np      = length(ref(:))/nref;

disp(['MEAN(ref) = ' num2str(mean_ref)])
disp(['STD(ref)  = ' num2str(std_ref)])

%% Main body

lpct        = linspace(1/np,100,100);
lofs        = linspace(eps,1.5,100);
lofs        = [-lofs(end:-1:1) lofs]; 
                
% Initialisation
P2_all                      = zeros([length(lpct),length(lofs),nrep]);
ref_integral                = zeros([length(lpct),length(lofs),nrep]);
les_integral_theoretical    = zeros([length(lpct),length(lofs),nrep]);
length_lpct                 = length(lpct);
length_lofs                 = length(lofs);

p = gcp('nocreate');
if isempty(p)
    p = parcluster;
    p.NumWorkers = 24;
    p.NumThreads = 1;
    parpool(p,p.NumWorkers)
end

% Simulate lesions and infer impact on reference data
parfor y=1:nrep 
    % disp(num2str(y))

    for counter_ofs = 1:length_lofs

        % Lesion offset
        ofs         = lofs(counter_ofs);
        les_ofs     = ofs*std_ref;

        for counter_pct = 1:length_lpct

            % Bootstrap with replacement, i.e. new permutations of ref
            refsamp = datasample(ref(:),np,'Replace',false);
            les     = datasample(ref(:),np,'Replace',false);
            
            % Lesion indices 
            pct         = lpct(counter_pct);
            nlesvx      = round((pct/100)*np);
            idx         = randsample(np,nlesvx); 

            % Apply lesion with fold-over control (FOC)
            %
            % FOC refers to the adjustment carried out to prevent (negative) 
            % lesion offsets from yielding negative intensity values. This 
            % is achieved forcing negative values to be positive within 
            % a noise range estimate (informed by ref's STD).
            les(idx)            = les(idx)+les_ofs; % Lesion applied here
            
            if FOC
                idx_lesneg      = find(les<0);
                nlesneg         = length(idx_lesneg);
            else
                nlesneg         = [];
            end
            
            nlespos             = nlesvx-nlesneg;
            
            if FOC && nlesneg
                noise_lesneg    = abs(std_ref*randn(1,nlesneg));                        
                les(idx_lesneg) = noise_lesneg;  
            else
                noise_lesneg    = [];
            end
            
            % Absolute contributions
            les_integral_theoretical(counter_pct,counter_ofs,y) =...
                nlesvx*abs(les_ofs);
            ref_integral(counter_pct,counter_ofs,y) =...
                sum(abs(refsamp(:)));

            % Stats
            [P2,~,~] = ranksum(refsamp,les);
            P2_all(counter_pct,counter_ofs,y) = P2;

        end %pct
    end %ofs
end %rep

%% Build robustness across repetitions

P2_cum       = ones([length(lpct),length(lofs)]);
PR_P2        = zeros([length(lpct),length(lofs)]);
refI         = zeros([length(lpct),length(lofs)]);
lesI_theor   = zeros([length(lpct),length(lofs)]);
rep          = 1:nrep;
counter_pct  = 0;

for pct = lpct
    counter_pct=counter_pct+1;

    counter_ofs = 0;                
    for ofs = lofs
        counter_ofs=counter_ofs+1;

        % Median P. N.b. The following is an overkill for the median but
        % left it in for easy substitution by a mean calculation.
        P2_cum(counter_pct,counter_ofs) =...
            exp(-median(-log(P2_all(counter_pct,counter_ofs,rep))));
        
        % Median reference contribution
        refI(counter_pct,counter_ofs) =...
            median(ref_integral(counter_pct,counter_ofs,rep));
        
        % Median lesion contribution
        lesI_theor(counter_pct,counter_ofs) =...
            median(les_integral_theoretical(counter_pct,counter_ofs,rep));
    end
end

%% Robust minimum detectable change (rMDC) at the critical lesion interface 

% Detectable lesion mask
detectable_les = zeros(size(P2_cum));
detectable_les(P2_cum<P2_cum_thr) = 1;

% Critical lesion interface 
[wGx,wGy]=imgradientxy(detectable_les,'central');
grad_detectable_les = detectable_les.*sqrt(wGx.^2 + wGy.^2);
grad_detectable_les(grad_detectable_les>0) = 1;

% Split into negative and positive offset interfaces
detectable_les_neg = grad_detectable_les;
detectable_les_neg(:,round(length(lofs)/2)+1:end) = 0;

detectable_les_pos = grad_detectable_les;
detectable_les_pos(:,1:round(length(lofs)/2)) = 0;

% Robust MDC calculation for negative and positive offsets respectively.
% Relativce change is defined as the ratio between absolute lesion 
% (theoretical, assuming no FOC) and reference contributions.
% rMDC is the Nth percentile of change at the critical lesion interface.
les_ref_integral_ratio_neg      = detectable_les_neg.*(lesI_theor./refI);
MDC_neg_approx                  = prctile(...
    les_ref_integral_ratio_neg(les_ref_integral_ratio_neg>0),prctile_thr);

les_ref_integral_ratio_pos      = detectable_les_pos.*(lesI_theor./refI);
MDC_pos_approx                  = prctile(...
    les_ref_integral_ratio_pos(les_ref_integral_ratio_pos>0),prctile_thr);

disp(['Robust MDC (' num2str(prctile_thr) 'th prctile) = ' num2str(MDC_neg_approx) ' (negative offset) / ' num2str(MDC_pos_approx) ' (positive offset)'])

toc
