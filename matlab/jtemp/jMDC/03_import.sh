<<C
WARNING: Edit carefully
C

<<c
echo +++ Import QSM data
for i in all_abs_brain_wqsm25.nii.gz all_brain_wqsm25.nii.gz; do echo $i
	if [ ! -f $i ]; then
		mv ../03_warp/$i ./
	fi
done
c

echo +++ Import ROIs

cp -v /home/jacosta/sf_datavb_local/190125_NewPrecision/05_warp/subj02/05_atlas/02_MNI/wMNI-maxprob-thr50-1mm.nii.gz ./

exit
cp -v ../c2mean_MTprot_MT.nii* ./
cp -v ../c1c2_mean_MTprot_MT.nii* ./

exit
cp -v ../04_MD7T/wFINAL_ROIs.nii.gz ./
cp -v ../../mean_wqsm01_avgpdt1.nii.gz ./ 
cp -v ../c1mean_MTprot_MT.nii.gz ./

exit
cp -v /home/jacosta/sf_datavb_local/190125_NewPrecision/05_warp/subj02/05_atlas/02_MNI/wCerebellum-MNIfnirt-maxprob-thr50-1mm.nii.gz ./
cp -v /home/jacosta/sf_datavb_local/190125_NewPrecision/05_warp/subj02/05_atlas/03_OASIS-30/wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz ./
cp -v /home/jacosta/sf_datavb_local/190125_NewPrecision/05_warp/subj02/05_atlas/02_MNI/wTalairach-labels-1mm.nii.gz ./

exit
cp -v /home/jacosta/sf_datavb_local/190125_NewPrecision/05_warp/subj02/05_atlas/03_OASIS-30/final_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz ./
cp -v /home/jacosta/sf_datavb_local/190125_NewPrecision/05_warp/subj02/05_atlas/02_MNI/final_wCerebellum-MNIfnirt-maxprob-thr50-1mm.nii.gz ./
cp -v /home/jacosta/sf_datavb_local/190125_NewPrecision/05_warp/subj02/05_atlas/02_MNI/final_wTalairach-labels-1mm.nii.gz ./
cp -v 01_FIRST/ero1_brain_mean_MTprot_MT_all_fast_firstseg.nii.gz ./
