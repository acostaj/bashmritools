clear all
close all

tic

addpath spm12b_nifti

%%
% Input data
data_root = 'all_MTprot_R1';

% Input ROIs
ROI_root  = 'MPMmsk_JAC_atlas';

% Output
outroot  = [ROI_root '_FROM_' data_root];

%%
disp('>>> Load/initialise vols')

addpath ~/jtools/QSMbox/master/ptb/_Utils
addpath ~/jtools/QSMbox/master/ptb/_spm12b_nifti

[data,st]       = jniiload(data_root);
[ROI]           = jniiload(ROI_root);

[data_MDC_neg]  = zeros(size(data(:,:,:,1)));
[data_MDC_pos]  = zeros(size(data(:,:,:,1)));

%%
disp('>>> ROI extraction')
minROI = min(ROI(ROI~=0));
maxROI = max(ROI(:));

c = 0;
% Loop across min-max ROI value range
for x = minROI:maxROI
    
    % Find ROI indices
    idx = find(ROI==x);
    
    % If ROI exits, go ahead...
    if (idx)
        disp(['ROI: ' num2str(x) ' (' num2str(length(idx)) ' voxels)'])
        c = c+1;
        
        % Prep reference distribution - loop across subjects/timepoints
        ref = [];
        for y = 1:size(data,4)
            data0   = data(:,:,:,y);
            data1   = data0(idx);
            data1(isnan(data1)==1) = 0;
            data1(isinf(data1)==1) = 0;
            ref     = [ref; data1];
        end
        
        DEBUG = false;
        if DEBUG
            np      = 3e2;
            nref    = 4;
            offset  = [150 150 150 150];
            SD      = [1 1 1 1];
            for y=1:nref
                ref(y,:) = abs(offset(y) + SD(y)*randn(1,np));
            end
        end
            
        % Carry out simulation
        [MDC_neg,MDC_pos] = jmcstatssimpar2_SLAVE(ref,y);
        
        % Write out results to parcellated 3D vol
        data_MDC_neg(idx) = MDC_neg;
        data_MDC_pos(idx) = MDC_pos;
        
        toc
    end
end

%%
disp('>>> Save images')
jniisave(data_MDC_neg,st,['MDC_neg_' outroot])
jniisave(data_MDC_pos,st,['MDC_pos_' outroot])

%%
toc
