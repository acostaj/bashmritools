### brain_ero1_brain_mean_MTprot_MT_all_fast_firstseg (exc. ROI #16)

echo "brain_ero1_brain_mean_MTprot_MT_all_fast_firstseg (exc. ROI #16)"
roi=ero1_brain_mean_MTprot_MT_all_fast_firstseg.nii.gz
n=16
fslmaths $roi -thr $n -uthr $n -bin -sub 1 -abs ttt
fslmaths $roi -mas ttt $roi
rm -f ttt.nii*
fslmaths c1c2_mean_MTprot_MT -thr 0.5 -bin -mul ${roi} brain_${roi}


### ThalSeg

echo ThalSeg
roi=wFINAL_ROIs.nii.gz
n=100
fslmaths $roi -thr $n ThalSeg
roi=ero1_brain_mean_MTprot_MT_all_fast_firstseg.nii.gz
n=10
fslmaths $roi -thr $n -uthr $n -bin ttt_1
n=49
fslmaths $roi -thr $n -uthr $n -bin ttt_2
fslmaths ttt_1 -add ttt_2 -mul ThalSeg ThalSeg
rm -f ttt_*

roi=brain_ero1_brain_mean_MTprot_MT_all_fast_firstseg.nii.gz
for n in 10 49; do echo $n
	fslmaths $roi -thr $n -uthr $n -bin -sub 1 -abs ttt
	fslmaths $roi -mas ttt $roi
	rm -f ttt.nii*
done


### maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2

echo maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2
roi=wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz
tm=maxc1mean_MTprot_MT.nii.gz
jsegmentextend $tm $roi
jgzip
roi=$(jfr ${tm})_$(jfr ${roi})
n=1000
fslmaths $roi -uthr $n -bin -sub 1 -abs ttt
fslmaths $roi -mas ttt $roi
rm -f ttt.nii*


### DN: noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2

echo "Dentate nucleus: noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2"
roi=wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz
qsm=mean_wqsm01_avgpdt1.nii.gz
fslmaths $roi -mul 0 noncortex_${roi}
for n in 6 7 15 631 632; do echo $n
        fslmaths $roi -thr $n -uthr $n -bin -mul $n ttt_${n}
        fslmaths noncortex_${roi} -add ttt_${n} noncortex_${roi}
done
fslmaths noncortex_${roi} -bin -mul 7 noncortex_${roi}
for n in 46; do echo $n
        fslmaths $roi -thr $n -uthr $n -bin -mul $n ttt_${n}
        fslmaths noncortex_${roi} -add ttt_${n} noncortex_${roi}
done
rm -f ttt_*
fslmaths $qsm -thr 0.04 -bin bin_thr0.04_${qsm}
fslmaths ../c2mean_MTprot_MT.nii* -thr 0.5 -bin -mul noncortex_${roi} -mul bin_thr0.04_${qsm} noncortex_${roi}


### RSI LSI

echo Substantia innominata: RSI, LSI
qsm=mean_wqsm01_avgpdt1.nii.gz
fslmaths ero1_brain_mean_MTprot_MT_all_fast_firstseg.nii.gz -bin -sub 1 -abs nofirst_mask
for roi in RSI LSI; do echo $roi
        fslmaths $roi -bin orig_${roi}
        fslmaths $qsm -thr 0.04 -bin -mul orig_${roi} -mul nofirst_mask $roi
        jeromsk $roi 0.8
done


### dil0.8_ero0.8_dil0.8_RN_SN_STN

echo dil0.8_ero0.8_dil0.8_RN_SN_STN
qsm=mean_wqsm01_avgpdt1.nii.gz
roi=wFINAL_ROIs.nii.gz
n=6
fslmaths $roi -uthr $n RN_SN_STN
jdilmsk RN_SN_STN 0.8
fslmaths dil0.8_RN_SN_STN -uthr $n dil0.8_RN_SN_STN
jeromsk dil0.8_RN_SN_STN 0.8
fslmaths $qsm -thr 0.04 -bin -mul ero0.8_dil0.8_RN_SN_STN ero0.8_dil0.8_RN_SN_STN
jdilmsk ero0.8_dil0.8_RN_SN_STN.nii.gz 0.8
fslmaths $qsm -thr 0.04 -bin -mul dil0.8_ero0.8_dil0.8_RN_SN_STN dil0.8_ero0.8_dil0.8_RN_SN_STN


### finalGM_wCerebellum-MNIfnirt-maxprob-thr50-1mm

echo finalGM_wCerebellum-MNIfnirt-maxprob-thr50-1mm
roi=wCerebellum-MNIfnirt-maxprob-thr50-1mm.nii.gz
fslmaths maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2 -bin -sub 1 -abs -bin complement_maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2
fslmaths $roi -mas maxc1mean_MTprot_MT -mas complement_maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2 maxc1_${roi}
fslmaths maxc1mean_MTprot_MT -sub maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2 maxc1mean_MTprot_MT_NO_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels
fslmaths $roi -bin msk_${roi}
jdilmsk msk_${roi} 1
jdilmsk dil1_msk_${roi} 1
jdilmsk dil1_dil1_msk_${roi} 1
fslmaths maxc1mean_MTprot_MT_NO_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels -mas dil1_dil1_msk_${roi} dil1_dil1_msk_${roi}_intersect_maxc1mean_MTprot_MT_NO_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels
fslmaths maxc1_${roi} -bin -mul -1 -add dil1_dil1_msk_${roi}_intersect_maxc1mean_MTprot_MT_NO_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels remaining_GM_cerebellum
jsegmentextend remaining_GM_cerebellum.nii $roi
jgzip
fslmaths maxc1_${roi} -add remaining_GM_cerebellum_wCerebellum-MNIfnirt-maxprob-thr50-1mm finalGM_wCerebellum-MNIfnirt-maxprob-thr50-1mm


### msk_WM

echo msk_WM
jdilmsk ero1_brain_mean_MTprot_MT_all_fast_firstseg 1
jdilmsk dil1_ero1_brain_mean_MTprot_MT_all_fast_firstseg 1
jdilmsk noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2 1
jdilmsk dil1_noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2 1
jdilmsk RSI 1
jdilmsk LSI 1
jdilmsk dil0.8_ero0.8_dil0.8_RN_SN_STN 1
jdilmsk dil1_dil0.8_ero0.8_dil0.8_RN_SN_STN 1
fslmaths dil1_dil1_ero1_brain_mean_MTprot_MT_all_fast_firstseg -add dil1_dil1_noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2 -add dil1_RSI -add dil1_LSI -add dil1_dil1_dil0.8_ero0.8_dil0.8_RN_SN_STN -bin dil2_msk_deepGM 
fslmaths dil2_msk_deepGM -sub 1 -abs -bin -mul c2mean_MTprot_MT -thr 0.5 -bin msk_WM


### WM3_wTalairach-labels-1mm.nii.gz

echo WM3_wTalairach-labels-1mm.nii.gz
roi=wTalairach-labels-1mm.nii.gz
cat Talairach.xml |grep "White Matter" > Talairach_WM.xml
cat Talairach_WM.xml |cut -c15- > ttt_1
cut -c-4 ttt_1 > ttt_2
sed 's/\"//' ttt_2 > ttt_3
fslmaths $roi -mul 0 WM_${roi}
for n in $(cat ttt_3); do echo $n
        fslmaths $roi -thr $n -uthr $n -bin -mul $n ttt_${n}
        fslmaths WM_${roi} -add ttt_${n} WM_${roi}
done
rm -f ttt_*
fslmaths $roi -mul 0 Brainstem_${roi}
for n in 5 6 71 72 215 216 503 574 576; do echo $n
        fslmaths $roi -thr $n -uthr $n -bin -mul $n ttt_${n}
        fslmaths Brainstem_${roi} -add ttt_${n} Brainstem_${roi}
done
rm -f ttt_*
rm -f WM2_*
fslmaths WM_${roi} -add Brainstem_${roi} WM2_${roi}
fslmaths msk_wCerebellum-MNIfnirt-maxprob-thr50-1mm -bin -sub 1 -abs -mul WM2_${roi} -add msk_wCerebellum-MNIfnirt-maxprob-thr50-1mm WM2_${roi}
rm -f remaining_WM_Talairach.nii*
fslmaths WM2_${roi} -bin -sub 1 -abs -bin -mas msk_WM remaining_WM_Talairach
jsegmentextend remaining_WM_Talairach.nii WM2_${roi}
jgzip
fslmaths WM2_${roi} -mas msk_WM -add remaining_WM_Talairach_WM2_wTalairach-labels-1mm WM3_${roi}
fslmaths WM3_${roi} -thr 1 -uthr 1 -bin msk_WM3_Cerebellum


### WM9_wTalairach-labels-1mm

echo WM9_wTalairach-labels-1mm
roi=wMNI-maxprob-thr50-1mm.nii.gz
fslmaths $roi -thr 2 -uthr 2 -bin msk_Cerebellum_$roi
fslmaths msk_Cerebellum_$roi -sub 1 -abs -bin complement_msk_Cerebellum_${roi}
fslmaths msk_WM3_Cerebellum -mul complement_msk_Cerebellum_${roi} wrong_WM3_Cerebellum_labels
fslmaths wrong_WM3_Cerebellum_labels -sub 1 -abs -bin complement_wrong_WM3_Cerebellum_labels
fslmaths WM3_wTalairach-labels-1mm -mul complement_wrong_WM3_Cerebellum_labels WM4_wTalairach-labels-1mm
fslmaths msk_Cerebellum_$roi -mul msk_WM -bin msk_WM_Cerebellum
fslmaths msk_WM_Cerebellum -sub 1 -abs -bin complement_msk_WM_Cerebellum
fslmaths complement_msk_WM_Cerebellum -mul WM4_wTalairach-labels-1mm -add msk_WM_Cerebellum WM5_wTalairach-labels-1mm
fslmaths ero1_brain_mean_MTprot_MT_all_fast_firstseg_orig.nii.gz -thr 16 -uthr 16 -bin msk_brainstem
jdilmsk msk_brainstem 1
jdilmsk dil1_msk_brainstem 1
fslmaths WM5_wTalairach-labels-1mm -thr 1 -uthr 1 -bin -mul dil1_dil1_msk_brainstem wrong_WM5_Cerebellum_labels
fslmaths wrong_WM5_Cerebellum_labels -sub 1 -abs -bin complement_wrong_WM5_Cerebellum_labels
fslmaths WM5_wTalairach-labels-1mm -mul complement_wrong_WM5_Cerebellum_labels WM6_wTalairach-labels-1mm
fslmaths wrong_WM3_Cerebellum_labels -add wrong_WM5_Cerebellum_labels -bin wrong_WM6_Cerebellum_labels
jsegmentextend wrong_WM6_Cerebellum_labels.nii WM6_wTalairach-labels-1mm.nii
jgzip 
fslmaths WM6_wTalairach-labels-1mm -mas msk_WM -add wrong_WM6_Cerebellum_labels_WM6_wTalairach-labels-1mm WM7_wTalairach-labels-1mm
fslmaths WM7_wTalairach-labels-1mm -thr 1 -uthr 1 -bin -mul dil1_dil1_msk_brainstem wrong_WM7_Cerebellum_labels
fslmaths wrong_WM7_Cerebellum_labels -sub 1 -abs -bin complement_wrong_WM7_Cerebellum_labels
fslmaths WM7_wTalairach-labels-1mm -mul complement_wrong_WM7_Cerebellum_labels WM8_wTalairach-labels-1mm
jsegmentextend wrong_WM7_Cerebellum_labels.nii WM8_wTalairach-labels-1mm.nii
jgzip
fslmaths WM8_wTalairach-labels-1mm -mas msk_WM -add wrong_WM7_Cerebellum_labels_WM8_wTalairach-labels-1mm WM9_wTalairach-labels-1mm


### Refine

echo "Refine: final2GM_wCerebellum-MNIfnirt-maxprob-thr50-1mm"
fslmaths finalGM_wCerebellum-MNIfnirt-maxprob-thr50-1mm -bin msk_finalGM_wCerebellum
jeromsk msk_finalGM_wCerebellum 1
fslmaths finalGM_wCerebellum-MNIfnirt-maxprob-thr50-1mm -mas ero1_msk_finalGM_wCerebellum final2GM_wCerebellum-MNIfnirt-maxprob-thr50-1mm


### Sanity check

echo Sanity check
fslmaths final2GM_wCerebellum-MNIfnirt-maxprob-thr50-1mm.nii.gz -mul WM9_wTalairach-labels-1mm.nii.gz -mul brain_ero1_brain_mean_MTprot_MT_all_fast_firstseg.nii.gz -mul ThalSeg.nii.gz -mul maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz -mul noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz -mul RSI.nii.gz -mul LSI.nii.gz -mul dil0.8_ero0.8_dil0.8_RN_SN_STN.nii.gz -bin ROI_overlap
echo The following should return 0.000000 0.000000, otherwise some ROIs are overlapping:
jmm ROI_overlap.nii.gz


### Merge

echo Print ROI value range
for i in final2GM_wCerebellum-MNIfnirt-maxprob-thr50-1mm.nii.gz WM9_wTalairach-labels-1mm.nii.gz brain_ero1_brain_mean_MTprot_MT_all_fast_firstseg.nii.gz ThalSeg.nii.gz maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz RSI.nii.gz LSI.nii.gz dil0.8_ero0.8_dil0.8_RN_SN_STN.nii.gz; do echo $i
	jmm $i
done

#final2GM_wCerebellum-MNIfnirt-maxprob-thr50-1mm.nii.gz
#0.000000 28.000000 
#WM9_wTalairach-labels-1mm.nii.gz
#14.000000 1084.000000 
#brain_ero1_brain_mean_MTprot_MT_all_fast_firstseg.nii.gz
#0.000000 58.000000 
#ThalSeg.nii.gz
#0.000000 112.000000 
#maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz
#1000.000000 2035.000000 
#noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz
#0.000000 46.000000 
#RSI.nii.gz
#0.000000 1.000000 
#LSI.nii.gz
#0.000000 1.000000 
#dil0.8_ero0.8_dil0.8_RN_SN_STN.nii.gz
#0.000000 6.000000 


echo Preparing data for final merge
fslmaths final2GM_wCerebellum-MNIfnirt-maxprob-thr50-1mm -mul 1 cerebellum_GM
fslmaths brain_ero1_brain_mean_MTprot_MT_all_fast_firstseg -bin -mul 100 -add brain_ero1_brain_mean_MTprot_MT_all_fast_firstseg subcortex_1
fslmaths ThalSeg -bin -mul 100 -add ThalSeg thalamus
fslmaths dil0.8_ero0.8_dil0.8_RN_SN_STN -bin -mul 300 -add dil0.8_ero0.8_dil0.8_RN_SN_STN subcortex_2
fslmaths noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2 -bin -mul 400 -add noncortex_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2 DN
fslmaths RSI -bin -mul 500 -add RSI SI
fslmaths LSI -bin -mul 501 -add LSI -add SI SI
fslmaths maxc1mean_MTprot_MT_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2 -mul 1 cortex
fslmaths WM9_wTalairach-labels-1mm -bin -mul 3000 -add WM9_wTalairach-labels-1mm WM

echo Merge
fslmaths cerebellum_GM -add subcortex_1 -add thalamus -add subcortex_2 -add DN -add SI -add cortex -add WM JAC_atlas

echo Finished. See JAC_atlas.nii.gz.


exit

### Refine

roi=JAC_atlas.nii.gz
for n in 303 304; do echo $n
        fslmaths $roi -thr $n -uthr $n -bin ttt_1
	fslmaths ttt_1 -sub 1 -abs ttt_2
        fslmaths $roi -mas ttt_2 $roi
	jeromsk ttt_1 1
	fslmaths ero1_ttt_1 -mul $n -add $roi $roi
        rm -f *ttt_*.nii*
done

