echo "++++ Transform atlas labels to study-wise and MNI co-ordinate systems"

<<C
Custom settings
C

Troot=brain_mean_MTprot_MT
W=MNI_to_${Troot}
for R in MNI-maxprob-thr50-1mm.nii.gz Cerebellum-MNIfnirt-maxprob-thr50-1mm.nii.gz Talairach-labels-1mm.nii.gz; do echo $R


echo "+++ Apply transforms (atlas labels => study-wise space)"
antsApplyTransforms -d 3 -i $R -r ${Troot}.nii.gz -o w${R} -n NearestNeighbor -t ${W}_1Warp.nii.gz ${W}_0GenericAffine.mat


echo "+++ Import data"
cp ../c1mean_MTprot_MT.nii.gz ./
cp ../c2mean_MTprot_MT.nii.gz ./


echo "+++ Intersect warped atlas labels with group-average GM tissue"
if [ "$R" == "Cerebellum-MNIfnirt-maxprob-thr50-1mm.nii.gz" ]; then
	fslmaths c1mean_MTprot_MT.nii.gz -thr 0.5 -bin -mul w${R} final_w${R}
fi

if [ "$R" == "Talairach-labels-1mm.nii.gz" ]; then
        fslmaths c2mean_MTprot_MT.nii.gz -thr 0.5 -bin -mul w${R} final_w${R}
fi


done
