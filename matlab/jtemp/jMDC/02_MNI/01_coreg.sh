echo "++++ Study-wise T1 template => MNI co-registration"

<<C
Custom setting
C

Nthreads=1


Troot=brain_mean_MTprot_MT
Nthreads=4

echo +++ Co-registration operation
time antsRegistrationSyN.sh -d 3 -f ${Troot}.nii.gz -m MNI152_T1_1mm_brain.nii.gz -o MNI_to_${Troot}_ -t s -n $Nthreads

echo +++ Visualisation
fslview ${Troot}.nii.gz MNI_to_${Troot}_Warped.nii.gz &

