function jac_R2star_fit_2

% clear all
% close all

addpath ~/matlab/DylanMuir-ParforProgMon-fd6bc94

%% Input variables
TE_train = [6.15 17.33 28.55 39.77];  % echo train (timings in ms)
nff = 7; % noise floor factor
options  = optimset('MaxIter', 1e3,'MaxFunEval', 2e3,'TolX', 1e-3,'TolFun', 1e-3,'Display', 'off');

%% Load data

[M] = jniiload('magn_orig');

    M1=M(:,:,:,1);
    M2=M(:,:,:,2);
    M3=M(:,:,:,3);
    M4=M(:,:,:,4);
    M1_orig=M1;

%% Compute matrix size and initialise processing matrices
s = size( M1 ); M0=zeros( s ); T2star=zeros( s ); residual=zeros( s );

%% Compute noise
[noise_mask,stm]=jniiload('noisemask_manual');
noise_mask(noise_mask>1)=1;
noise = M1_orig .* noise_mask; noise = noise( find( noise ~= 0 )); noise_floor = std( noise(:)) * nff;

%% Display noise region relative to image
figure
subplot(121); imagesc(M1_orig(:,:,end/2)); colorbar; colormap(gray); caxis([0 20]); axis square; title('Signal Magnitude (1st echo)')
subplot(122); imagesc(noise_mask(:,:,end/2)); colorbar; colormap(gray); axis square; title('Noise Region')

%% FITTING :: parfor can be used in the outer loop - run e.g. matlabpool(12) in MATLAB's command window.
N = s(1);
p = gcp('nocreate');
if isempty(p)
    parpool
end
ppm = ParforProgMon('T2* fitting ',N);
tic
parfor x = 1:N
    disp([ 'x = ' num2str(x) ])
    a1 = squeeze( M1(x,:,:) );
    if max( a1(:) ) > noise_floor
        m0_ = zeros([ s(2) s(3) ]); t2star_ = zeros([ s(2) s(3) ]); r_ = zeros([ s(2) s(3) ]);
        for z = 1:s(3)
            a2 = M1(x,:,z);
            if max( a2(:) ) > noise_floor
                m0 = zeros([ 1 s(2) ]); t2star = zeros([ 1 s(2) ]); r = zeros([ 1 s(2) ]);
                for y = 1:s(2)    
                    decay = double( [ M1(x,y,z) M2(x,y,z) M3(x,y,z) M4(x,y,z)] );
                    if min( decay(:) ) < noise_floor
                        idx = find( decay > noise_floor );
                        decay = decay( idx ); TE = TE_train( idx );
                    else
                        TE = TE_train;
                    end
                    if length( decay ) >= 2
                            slope = ( decay(2)-decay(1) ) / ( TE(2)-TE(1) );
                            M0_init = decay(1)-( slope * TE(1) );
                            P = polyfit( TE, log( decay/M0_init ), 1 );
                            T2star_init = -1/P(1);     
                            if M0_init < 0; M0_init = 0; end
                            if T2star_init < 0; T2star_init = 0; end

                            [ best_fit, fval ] = fminsearch(...
                            @(best_fit)sum(((best_fit(1)*exp(-TE./best_fit(2)))-decay).^2),...
                            [M0_init T2star_init], options ); 
                            m0(y) = best_fit(1); t2star(y) = best_fit(2); r(y) = fval;
		    else
			    if max( decay(:) ) > 0
			    	t2star(y) = TE(1)/2;
			    end
                    end
		    	    
                end
                m0_(:,z) = m0; t2star_(:,z) = t2star; r_(:,z) = r;
            end
        end
        M0(x,:,:) = m0_; T2star(x,:,:) = t2star_; residual(x,:,:) = r_; 
    end
    ppm.increment();
end

%% Display total processing time
toc; time_elapsed_in_minutes = toc/60

%% Save data to disk
save('results01.mat')

%% Save resulting parameter maps as Nifti images
stm.dt=[16 0]; % <-- change file type to floating point
jniisave( M0, stm, 'M0' )
jniisave( T2star, stm, 'T2star' )
jniisave( 1./T2star, stm, 'R2star' )

return

%% Display maps
figure
subplot(121); imagesc(abs(squeeze(M0(:,:,end/2)))); caxis([0 350]); colorbar; colormap(gray); axis square; title('Initial Magnetisation')
subplot(122); imagesc(abs(squeeze(T2star(:,:,end/2)))); caxis([0 40]); colorbar; colormap(gray); axis square; title('T2star')

end
