function jqsmroi(thr)
% Generate brain mask for QSMbox using SPM tissue probability segments
% Run in SPM Segment directory
%
% INPUT
% thr	c1+c2+c3 probability cut-off threshold
%
% Created by Julio Acosta-Cabronero, 08/12/2018

%% Arg
if nargin<1
    thr = 1e-4;
end

%% Load SPM segments
% GM
fname = dir('c1*.nii*');
froot = jfnamesplit(fname.name,'gz');
[c1,st] = jniiload(froot);

% WM
fname = dir('c2*.nii*');
froot = jfnamesplit(fname.name,'gz');
[c2] = jniiload(froot);

% CSF
fname = dir('c3*.nii*');
froot = jfnamesplit(fname.name,'gz');
[c3] = jniiload(froot);

%% Processing
% Generate initial mask
roi = c1+c2+c3;
roi(roi>thr)=1;
roi(roi<=thr)=0;

% Fill holes (3D approximation)
roifillZ=roi*0;
for z=1:size(roi,3)
    roifillZ(:,:,z) = imfill(roi(:,:,z),'holes');
end
 
roifillY=roi*0;
for y=1:size(roi,2)
    roifillY(:,y,:) = imfill(squeeze(roi(:,y,:)),'holes');
end
roifill = roifillZ.*roifillY;
clear roifillZ roifillY

roifillX=roi*0;
for x=1:size(roi,1)
    roifillX(x,:,:) = imfill(squeeze(roi(x,:,:)),'holes');
end
roifill = roifill.*roifillX;
clear roifillX

%% Save brain mask
jniisave(roifill,st,'roi')
