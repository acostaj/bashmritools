function [ res ] = m1_ifft3c( x )
    A=ifftshift(x);
    B=ifftn(A); clear A
    C=fftshift(B); clear B
    res = sqrt(length(x(:)))*C;
% 	res = sqrt(length(x(:)))*fftshift(ifftn(ifftshift(x)));
end
