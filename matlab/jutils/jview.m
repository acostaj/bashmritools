function jview(froot,clipping_range,slice_range,panels)
% SINTAX
% jview(fname)
% jview(fname,{cmin,cmax})
% jview(fname,{cmin,cmax},[subplot_row,subplot_col])
%
% ARGUMENTS
% Display NIFTI(.gz) image = [ fname ]
% Clipping range = {cmin,cmax} - options:  global min ('gmin'), global max ('gmax'), global max/2 ('gma2'), or default: nonzero mean+-sigma ('mstd')
% Slice range = [.2 .75]
% Number of cuts = [subplot_row,subplot_col] - default: 3x6
%
% Created by Julio Acosta-Cabronero

if nargin < 2
    clipping_range = {'mstd','mstd'};
end

if nargin < 3
    slice_range = [.2,.75];
end

if nargin < 4
    panels = [3,6];
end

[mat] = jniiload(froot);
jfig(mat,clipping_range,slice_range,panels)
