clear all
close all

[EG st]  = m1_nifti_load('dMRI_ap_up_DRBUDDI_R1_EG');
alpha = acos(abs(EG(:,:,:,3)));
m1_nifti_save(alpha,st,'alpha');