function [ res ] = m1_fft3c( x )
    A=ifftshift(x);
    B=fftn(A); clear A
    C=fftshift(B); clear B
    res = 1/sqrt(length(x(:)))*C;
% 	res = 1/sqrt(length(x(:)))*fftshift(fftn(ifftshift(x)));
end

