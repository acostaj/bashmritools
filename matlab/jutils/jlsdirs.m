function [dirs] = jlsdirs(path_to_dir,snip)
% Print list of directories
%
% [dirs] = jlsdirs(path_to_dir,snip)
%
% If 'path_to_dir' not provided, will list directories in PWD
% 'snip' denotes a search string in the relevant directory
%
% Created by Julio Acosta-Cabronero

    if exist('path_to_dir')==0
        path_to_dir = pwd;
    end

    if exist('snip')==0
        d = dir(path_to_dir);
    else
        d = dir([path_to_dir '/*' snip '*']);
    end
    
    isub = [d(:).isdir]; %# returns logical vector
    dirs = {d(isub).name}';
    dirs( ismember(dirs,{ '.', '..' })) = [];
    clear d isub
end
