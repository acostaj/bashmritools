function jgunzip
% SINTAX
%  jgunzip
%
% Created by Julio Acosta-Cabronero

if isempty(dir('*.gz'))==0
    disp('Gunzip')
    gunzip('*.gz')
else
    disp('Did nothing. Exit')
end
