function jadaptcombine(phase_scaling)
% DESCRIPTION
%  Adaptive multi-channel data combination
%
% SINTAX
%  jadaptcombine(phase_scaling)
% 
% REQUIREMENTS
%  (M)agnitude & (P)hase multi-channel images [sM/P*.nii(.gz)] in pwd
%  Define phase_scaling (e.g. Native Siemens scaling = 4096)
% 
% OPTIONAL
%  Directory named '.jrsosbrainmask' in pwd containing 'brainmask.nii.gz' 
%   to select an optimal reference channel
% 
% OUTPUT (TO DISK)
%  Combined magnitude: ac_magn.nii.gz 
%  Combined phase: ac_phase.nii.gz 
%
% Created by Shan Yang (26/8/2013)
% Last edited by Julio Acosta-Cabronero (12/12/2013)


%% 1.- Load NIFTI data    
% gunzip files if there are any
jgunzip

% list images
idx = dir( '*.nii' );
tnf = length(idx);
idx1 = idx(1:round(tnf/2));
idx2 = idx(round(tnf/2)+1:end);

% quality check
l1 = length(idx1);
l2 = length(idx2);
if l1==l2
    disp([ '> # of channels: ' num2str(l1) ])
else
    error('ERROR: Inconsistent channel data')
end
 
% preload data
fname = idx1( end ).name;
M = spm_vol( fname );

fname = idx2( end ).name;
P = spm_vol( fname );
p = spm_read_vols( P );

% maximum phase 
maxP = phase_scaling;

if exist('.jrsosbrainmask')
    cd .jrsosbrainmask
    [mask] = jniiload('brainmask');
    cd ..
else
    [mask] = ones(size(M));
end

% loop initialisation
disp('> Loading data...')

[ A B C ] = size( p );
L = length( idx1 );
com = zeros( A, B, C, L );
a = zeros( L );

% load data loop
% ppm = ParforProgressStarter2( 'Loading data progress', L, .01 );
parfor t = 1:L                         % <============ [P]
    Mfname = idx1( t ).name;
    M = spm_vol( Mfname );
    tmp1 = spm_read_vols( M );
    
    %store 50th percentile value
    a(t)=prctile( tmp1(mask==1), 50 ); 
     
    Pfname = idx2( t ).name;
    P = spm_vol( Pfname );
    tmp2 = ( spm_read_vols( P )/maxP )*pi;
    tmp = double( complex( tmp1.*cos( tmp2 ), tmp1.*sin( tmp2 )));
    com( :, :, :, t ) = tmp;
% ppm.increment( t );
end
%delete( ppm )

if exist('.jrsosbrainmask')
    % select coil
    m=max(a(:));
    idx=find(a(:)==m(1));
    channel_idx=idx(1);
else
    disp(['> Warning: using default reference channel'])
    channel_idx=1;
end
disp(['> Reference channel: #' num2str(channel_idx)])
disp(['> median of brain signal magnitude: ' num2str(m(1))])

% delete NIFTIs if there are NIFTI-GZIP files in current dir
if isempty( dir( '*nii.gz' )) == 0
    delete( '*nii' )
end

%% 2.- Adaptive reconstruction
% loop initialisation
disp('> Adaptive reconstruction')

[ X Y Z C ] = size( com );
recon = zeros( X, Y, Z );

% sy_adapt loop
%ppm = ParforProgressStarter2( 'Adaptive reconstruction progress', Z, .01 );
parfor z = 1:Z                           % <============ [P]
% for z = 1:Z  								% <============ [P]
	disp([ '>> Adaptive combination: slice #' num2str(z)])
	tmp = squeeze( com( :, :, z, : ));
    tmp = permute( tmp, [ 3 1 2 ]);
    recon( :, :, z ) = jadaptcombinesub( tmp, 12, 2, channel_idx );
%ppm.increment( z );
end
%delete( ppm );

%% 3.- Save & display
% save magnitude data
m = abs( recon );
M.fname = 'ac_magn.nii';
M.private.dat.fname = M.fname;
spm_write_vol( M, m );

% save phase data
p = angle( recon );
P.fname = 'ac_phase.nii';
P.private.dat.fname = P.fname;
spm_write_vol( P, p );

end % main function
