cdir=pwd;

fid=fopen('~/.bmt/matdir');
jmat = fscanf(fid,'%s');
fclose(fid);

cd([jmat 'jutils']);
dirs = jlsdirs('../');
for x = 1:length(dirs)
    newpath = cell2mat([jmat dirs(x)]);
    addpath(newpath)
end
addpath('~/.bmt/matlab')
cd(cdir)
