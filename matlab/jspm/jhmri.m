function jhmri(jobfile)
% Created by Julio Acosta-Cabronero

tic

% Add SPM dir to MATLAB path
jspmpath;

% Run job file
spm('defaults', 'PET');
spm_jobman('initcfg');
spm_jobman('run',jobfile);

toc
