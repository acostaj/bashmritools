function jspmsegmentpar(settings)
% Parallel SPM Unified Segmentation
%
% Structural images must be located in separate directories
%
% jspmsegmentpar('settings_name')
%
% Presets available:
%  '3T_default' (global default, SPM Segment default)
%  '7T_default'
%
% Created by Julio Acosta-Cabronero

%if matlabpool('size')==0
%%    eval('matlabpool')
%    eval([ 'matlabpool open' ])
%end

% Default SPM Segment settings
if nargin < 1
   settings = '3T_default';
end

[dirs] = jlsdirs;
L = length(dirs);
%ppm = ParforProgressStarter2( 'Parallel segmentation progress...', L, .01 );
parfor i = 1:L
    j = cell2mat(dirs(i));
    disp(' ')
    disp('################################')
    disp(j)
    disp('################################')
    cd(j)
    %%%%%%%%%%%%%%%%%%%%%%
    a = dir('*nii*');
    fname = a(1).name;
    jspmsegment(fname,settings)
    %%%%%%%%%%%%%%%%%%%%%%
    cd ..
%ppm.increment( i );
end
%delete( ppm )
