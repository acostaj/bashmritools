function jspmsegment(fname,settings)
% SPM12 segmentation and bias correction
%
% jspmsegment(filename,settings)
%
% INPUTS
%  NIFTI(.gz) file in pwd
%  SETTINGS
%   + '3T_default'
%   + '7T_default'
%
% NOTE
%  Recommend to run each image in a dedicated directory
%
% Created by Julio Acosta-Cabronero

tic

% Default SPM Segment settings
if nargin < 2
    settings = '3T_default';
end

% Add SPM dir to MATLAB path
jspmpath;

% Gunzip if necessary
[froot,ext] = jfnamesplit(fname);
if strcmpi(ext,'gz')
    gunzip(fname);
    fname = froot;
end

% Locate job file
jspmdir = fileparts(which(mfilename));
jobfile = {[jspmdir '/jspmsegment_job.m']};

% Vars needed by job file
save('fname.mat','fname');
save('settings.mat','settings');

% Run job file
spm('defaults', 'PET');
spm_jobman('initcfg');
spm_jobman('run',jobfile);

% Gzip
jgzip;

toc
